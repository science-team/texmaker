/***************************************************************************
 *   copyright       : (C) 2003-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/texmaker/                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef XMLTAGSLISTWIDGET_H
#define XMLTAGSLISTWIDGET_H

#include <QListWidget>

class QDomElement;

struct xmlTag
    {
    QString txt;
    QString tag;
    QString dx;
    QString dy;
    int type;
    };

struct xmlTagList
    {
    QString title;
    QList<xmlTag> tags;
    QList<xmlTagList> children;
    };

class XmlTagsListWidget : public QListWidget  {
  Q_OBJECT
public:
	XmlTagsListWidget(QWidget *parent, QString file);
private:
xmlTagList getTags(const QDomElement &element);
xmlTagList xmlSections;
void addListWidgetItems(const xmlTagList &tagList);
};

#endif
