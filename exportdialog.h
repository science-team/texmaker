/***************************************************************************
 *   copyright       : (C) 2003-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/texmaker/                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include "ui_exportdialog.h"

#include <QProcess>
#include <QPointer>

class ExportDialog : public QDialog
{
    Q_OBJECT

public:
    ExportDialog(QWidget* parent = 0, QString f="", QString ep="");
	~ExportDialog();
Ui::ExportDialog ui;

private slots:
void browseHtlatex();
void runHtlatex();
void readFromStderr();
void readFromStdoutput();
void SlotEndProcess(int err);

private :
QString filename;
QString extra_path;
QPointer<QProcess> proc;
};

#endif
