/***************************************************************************
 *   copyright       : (C) 2003-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/texmaker/                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef LIGHTGOTOLINEWIDGET_H
#define LIGHTGOTOLINEWIDGET_H

#include "ui_gotolinewidget.h"
#include "lightlatexeditor.h"

class LightGotoLineWidget : public QWidget
{ 
    Q_OBJECT

public:
    LightGotoLineWidget(QWidget* parent = 0);
    ~LightGotoLineWidget();
    Ui::GotoLineWidget ui;

public slots:
    virtual void gotoLine();
    void SetEditor(LightLatexEditor *ed);
    void doHide();
protected:
    LightLatexEditor *editor;
signals:
void requestHide();
};

#endif 
