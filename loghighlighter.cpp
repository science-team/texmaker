/***************************************************************************
 *   copyright       : (C) 2003-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/texmaker/                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/


#include <QtGui>
#include <QRegularExpression>

#include "loghighlighter.h"

LogHighlighter::LogHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
	ColorStandard = QColor(0x00, 0x00, 0x00);
	ColorFile = QColor(0x00,0x80, 0x00);
	ColorError=QColor(0xFF, 0x00, 0x00);
	ColorWarning=QColor(0x00, 0x00, 0xCC);
}

void LogHighlighter::highlightBlock(const QString &text)
{
QRegularExpression rxLatexError("! (.*)");
QRegularExpression rxBadBox("(Over|Under)(full \\\\[hv]box .*)");
QRegularExpression rxWarning("(((! )?(La|pdf)TeX)|Package) .*Warning.*:(.*)");
if (rxLatexError.match(text).hasMatch())
	{
	setFormat(0, text.length(),ColorError);
	}
else if ((rxBadBox.match(text).hasMatch()) || (rxWarning.match(text).hasMatch()))
	{
	setFormat(0, text.length(),ColorWarning);
	}
else if (text.indexOf(".tex",0)!=-1) 
	{
	setFormat(0, text.length(),ColorFile);
	}
}
