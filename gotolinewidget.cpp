/***************************************************************************
 *   copyright       : (C) 2003-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/texmaker/                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "gotolinewidget.h"
#include <QAction>

#include "geticon.h"
#include "theme.h"

GotoLineWidget::GotoLineWidget(QWidget* parent)
    : QWidget( parent)
{
ui.setupUi(this);
connect( ui.closeButton, SIGNAL( clicked() ), this, SLOT( doHide() ) );
connect( ui.gotoButton, SIGNAL( clicked() ), this, SLOT( gotoLine() ) );
ui.gotoButton->setToolTip("Return");
ui.closeButton->setShortcut(Qt::Key_Escape);
ui.closeButton->setToolTip("Escape");
QPalette pal( palette() );
pal.setColor( QPalette::Active, QPalette::WindowText,Theme::grayColor );
pal.setColor( QPalette::Inactive, QPalette::WindowText, Theme::grayColor );
pal.setColor( QPalette::Disabled, QPalette::WindowText, Theme::grayColor );
pal.setColor( QPalette::Active, QPalette::HighlightedText, Theme::grayColor );
pal.setColor( QPalette::Inactive, QPalette::HighlightedText, Theme::grayColor );
pal.setColor( QPalette::Disabled, QPalette::HighlightedText,Theme::grayColor );
pal.setColor( QPalette::Active, QPalette::Base, Theme::grayColor );
pal.setColor( QPalette::Inactive, QPalette::Base, Theme::grayColor );
pal.setColor( QPalette::Disabled, QPalette::Base, Theme::grayColor);
pal.setColor(QPalette::Window, Theme::darkbackgroundColor);
setPalette( pal );
ui.closeButton->setStyleSheet(Theme::buttonSearchStyleSheet);
ui.gotoButton->setStyleSheet(Theme::buttonSearchStyleSheet);
ui.spinLine->setFrame(false);
ui.spinLine->setStyleSheet(Theme::spinboxSearchStyleSheet);
}

GotoLineWidget::~GotoLineWidget()
{

}
void GotoLineWidget::gotoLine()
{
if ( editor )
  {
  editor->gotoLine( ui.spinLine->value() - 1 );
  editor->viewport()->repaint();
  editor->setFocus();
  }

}
void GotoLineWidget::SetEditor(LatexEditor *ed)
{
editor=ed;
}

void GotoLineWidget::doHide()
{
emit requestHide();
if ( editor ) 
	{
	editor->viewport()->repaint();
	editor->setFocus();
	}
}

void GotoLineWidget::keyPressEvent ( QKeyEvent * e ) 
{
if ((e->key()==Qt::Key_Enter)||(e->key()==Qt::Key_Return)) gotoLine();
}
