<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.ui" line="14"/>
        <source>About Texmaker</source>
        <translation>حول Texmaker</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="76"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="86"/>
        <source>Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="96"/>
        <source>Thanks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="106"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddOptionDialog</name>
    <message>
        <location filename="../addoptiondialog.ui" line="14"/>
        <source>New</source>
        <translation>جديد</translation>
    </message>
</context>
<context>
    <name>AddTagDialog</name>
    <message>
        <location filename="../addtagdialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>حرِّر الكلمات الدلالية</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="22"/>
        <source>Item</source>
        <translation>نقطة</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="32"/>
        <source>LaTeX Content</source>
        <translation>محتوى LaTeX</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="55"/>
        <source>Keyboard Trigger</source>
        <translation>زناد لوحة المفاتيح</translation>
    </message>
</context>
<context>
    <name>ArrayDialog</name>
    <message>
        <location filename="../arraydialog.ui" line="45"/>
        <source>Num of Columns</source>
        <translation>عدد الأعمدة</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="59"/>
        <source>Columns Alignment</source>
        <translation>تراصف الأعمدة</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="76"/>
        <source>Environment</source>
        <translation>محيط</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="35"/>
        <source>Num of Rows</source>
        <translation>عدد الصفوف</translation>
    </message>
    <message>
        <location filename="../arraydialog.cpp" line="43"/>
        <source>Quick Array</source>
        <translation>مجموعة سريعة</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../browser.cpp" line="78"/>
        <source>&amp;File</source>
        <translation>&amp;ملف</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="79"/>
        <source>Print</source>
        <translation>طباعة</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="81"/>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="89"/>
        <source>Index</source>
        <translation>فهرس</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="107"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../configdialog.ui" line="14"/>
        <source>Configure Texmaker</source>
        <translation>هيّء Texmaker</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="130"/>
        <source>Commands (% : filename without extension - @ : line number)</source>
        <translation>أوامر (%: إسم الملف دون ملحق -@: رقم السّطر)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="141"/>
        <location filename="../configdialog.cpp" line="610"/>
        <location filename="../configdialog.cpp" line="673"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="376"/>
        <location filename="../configdialog.cpp" line="616"/>
        <location filename="../configdialog.cpp" line="679"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="631"/>
        <location filename="../configdialog.cpp" line="694"/>
        <source>Bibtex</source>
        <translation>Bibtex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="366"/>
        <location filename="../configdialog.cpp" line="634"/>
        <location filename="../configdialog.cpp" line="697"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="755"/>
        <location filename="../configdialog.cpp" line="619"/>
        <location filename="../configdialog.cpp" line="682"/>
        <source>Dvi Viewer</source>
        <translation>عارض Dvi</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="820"/>
        <location filename="../configdialog.cpp" line="622"/>
        <location filename="../configdialog.cpp" line="685"/>
        <source>PS Viewer</source>
        <translation>عارض PS</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="155"/>
        <location filename="../configdialog.cpp" line="613"/>
        <location filename="../configdialog.cpp" line="676"/>
        <source>PdfLaTeX</source>
        <translation>عارض PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="383"/>
        <location filename="../configdialog.cpp" line="625"/>
        <location filename="../configdialog.cpp" line="688"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="399"/>
        <location filename="../configdialog.cpp" line="628"/>
        <location filename="../configdialog.cpp" line="691"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="600"/>
        <location filename="../configdialog.cpp" line="637"/>
        <location filename="../configdialog.cpp" line="700"/>
        <source>Pdf Viewer</source>
        <translation>عارض Pdf</translation>
    </message>
    <message>
        <source>Built-in Viewer</source>
        <translation type="vanished">العارض المُدمَج</translation>
    </message>
    <message>
        <source>External Viewer</source>
        <translation type="vanished">عارض خارجي</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="309"/>
        <location filename="../configdialog.cpp" line="640"/>
        <location filename="../configdialog.cpp" line="703"/>
        <source>metapost</source>
        <translation>metapost</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="278"/>
        <location filename="../configdialog.cpp" line="643"/>
        <location filename="../configdialog.cpp" line="706"/>
        <source>ghostscript</source>
        <translation>ghostscript</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="271"/>
        <location filename="../configdialog.cpp" line="646"/>
        <location filename="../configdialog.cpp" line="709"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="319"/>
        <location filename="../configdialog.cpp" line="649"/>
        <location filename="../configdialog.cpp" line="712"/>
        <source>Latexmk</source>
        <translation>Latexmk</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="629"/>
        <source>Embed</source>
        <translation>ضمّن</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="446"/>
        <location filename="../configdialog.cpp" line="652"/>
        <location filename="../configdialog.cpp" line="715"/>
        <source>R Sweave</source>
        <translation>R Sweave</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="247"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &amp;quot;--output-directory=build&amp;quot; option will be automatically added to the (pdf)latex command while the compilation.&lt;/p&gt;&lt;p&gt;For the others commands like dvips, ps2pdf, bibtex,... you will have to manually replaced &amp;quot;%&amp;quot; by &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;خيار &amp;quot;--output-directory=build&amp;quot; سيتم إضافته تلقائيّا إلى أمر (pdf)latex إثر التجميع&lt;/p&gt;&lt;p&gt;للأوامر الأخرى مثل dvips, ps2pdf, bibtex,... يجب عليك أن تعوض يدويّا &amp;quot;%&amp;quot; بِ &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="704"/>
        <source>lp options for the printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="250"/>
        <source>Use a &quot;build&quot; subdirectory for output files</source>
        <translation>إستعمل مجلد فرعي &quot;build&quot; للملفات المنتجة</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="148"/>
        <location filename="../configdialog.cpp" line="655"/>
        <location filename="../configdialog.cpp" line="718"/>
        <source>XeLaTeX</source>
        <translation>XeLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="285"/>
        <source>Add to PATH</source>
        <translation>أضف إلى PATH</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="896"/>
        <source>Quick Build Command</source>
        <translation>أمر بناء سريع</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="918"/>
        <source>LaTeX + dvips + View PS</source>
        <translation>LaTeX + dvips + إعرض PS</translation>
    </message>
    <message>
        <source>LaTeX + View DVI</source>
        <translation type="vanished">LaTeX + إعرض DVI</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="904"/>
        <source>PdfLaTeX + View PDF</source>
        <translation>PdfLaTeX + إعرض PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="946"/>
        <source>LaTeX + dvipdfm + View PDF</source>
        <translation>LaTeX + dvipdfm + إعرض PDF</translation>
    </message>
    <message>
        <source>LaTeX + dvips + ps2pdf + View PDF</source>
        <translation type="vanished">LaTeX + dvips + ps2pdf + إعرض PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="183"/>
        <location filename="../configdialog.cpp" line="658"/>
        <location filename="../configdialog.cpp" line="721"/>
        <source>LuaLaTeX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="356"/>
        <source>Bib(la)tex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="559"/>
        <source>Launch the &quot;Clean&quot; tool when exiting Texmaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="925"/>
        <source>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + View Pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="953"/>
        <source>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + View Pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="960"/>
        <source>Sweave + PdfLaTeX + View Pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LaTeX + Asymptote + LaTeX + dvips + View PS</source>
        <translation type="vanished">LaTeX + Asymptote + LaTeX + dvips + إعرض PS</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="974"/>
        <source>PdfLaTeX + Asymptote + PdfLaTeX + View Pdf</source>
        <translation>PdfLaTeX + Asymptote + PdfLaTeX + إعرض Pdf</translation>
    </message>
    <message>
        <source>LatexMk + View PDF</source>
        <translation type="vanished">LatexMk + إعرض PDF</translation>
    </message>
    <message>
        <source>XeLaTeX + View PDF</source>
        <translation type="vanished">XeLaTeX + إعرض PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="995"/>
        <source>LuaLaTeX + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User : (% : filename without extension)</source>
        <translation type="vanished">المستخدم : (% : إسم الملف دون ملحق)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1030"/>
        <location filename="../configdialog.ui" line="1069"/>
        <source>wizard</source>
        <translation>السّاحر</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1043"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(الأوامر يجب أن تُفصل بِ &apos;|&apos;)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1059"/>
        <source>For .asy files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1107"/>
        <source>Don&apos;t launch a new instance of the viewer if the dvi/ps/pdf file is already opened</source>
        <translation>لا تطلق مثيل جديد من العارض إذا كان ملف dvi/ps/pdf مفتوحا</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1140"/>
        <location filename="../configdialog.cpp" line="187"/>
        <location filename="../configdialog.cpp" line="200"/>
        <source>Editor</source>
        <translation>محرّر</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1269"/>
        <source>Editor Font Family</source>
        <translation>عائلة خطّ المحرّر</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1324"/>
        <source>Word Wrap</source>
        <translation>لفّ الكلمة</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1283"/>
        <source>Editor Font Size</source>
        <translation>حجم خطّ المحرّر</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1331"/>
        <source>Completion</source>
        <translation>تكملة</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1300"/>
        <source>Editor Font Encoding</source>
        <translation>ترميز خطّ المحرّر</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1338"/>
        <source>Show Line Numbers</source>
        <translation>عرض أرقام الأسطر</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1183"/>
        <source>Item</source>
        <translation>نقطة</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1188"/>
        <source>Color</source>
        <translation>لون</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1160"/>
        <source>Colors</source>
        <translation>ألوان</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1146"/>
        <source>Default Theme</source>
        <translatorcomment>Original style</translatorcomment>
        <translation>زيّ أصلي</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1209"/>
        <source>Dark theme</source>
        <translatorcomment>Dark style</translatorcomment>
        <translation>زيّ داكن</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1310"/>
        <source>Check for external changes</source>
        <translation>تحقّق من التّغييرات الخارجية</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1317"/>
        <source>Backup documents every 10 min</source>
        <translation>أنسخ الوثيقة إحتياطيّا كلّ 10 دقائق</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1362"/>
        <source>Spelling dictionary</source>
        <translation>قاموس التدقيق الإملائي</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1396"/>
        <source>Inline Spell Checking</source>
        <translatorcomment>Automatic spell checking</translatorcomment>
        <translation>تدقيق إملائي تلقائي</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1399"/>
        <source>Inline</source>
        <translation>تلقائي</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1232"/>
        <source>Tab width (num of spaces)</source>
        <translation>عرض علامة التبويب (عدد المساحات)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="616"/>
        <source>Built-in &amp;Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="649"/>
        <source>E&amp;xternal Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="932"/>
        <source>LaTeX + &amp;View DVI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="939"/>
        <source>LaTeX + dvips + ps&amp;2pdf + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="967"/>
        <source>LaTeX + As&amp;ymptote + LaTeX + dvips + View PS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="981"/>
        <source>Latex&amp;Mk + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="988"/>
        <source>&amp;XeLaTeX + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1011"/>
        <source>User : (% : filename &amp;without extension)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1153"/>
        <source>Replace tab with spaces</source>
        <translation>عوّض علامات التبويب بمساحات</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1444"/>
        <source>Svn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1450"/>
        <source>Detect uncommitted lines at the opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1457"/>
        <source>Path to the svn command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1485"/>
        <location filename="../configdialog.cpp" line="193"/>
        <location filename="../configdialog.cpp" line="201"/>
        <source>Shortcuts</source>
        <translation>إختصارات</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1521"/>
        <source>Toggle focus editor/pdf viewer</source>
        <translation>بدّل التركيز بين المحرّر وعارض pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1528"/>
        <source>PushButton</source>
        <translation>زِرّ ضغط</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1495"/>
        <source>Action</source>
        <translation>إجراء</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1500"/>
        <source>Shortcut</source>
        <translation>إختصار</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="53"/>
        <source>Get more dictionaries at: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="174"/>
        <location filename="../configdialog.cpp" line="197"/>
        <location filename="../configdialog.cpp" line="198"/>
        <source>Commands</source>
        <translation>أوامر</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="181"/>
        <location filename="../configdialog.cpp" line="199"/>
        <source>Quick Build</source>
        <translation>بناء سريع</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="245"/>
        <source>Browse dictionary</source>
        <translation>تصفح القاموس</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>svn command directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <location filename="../configdialog.cpp" line="391"/>
        <location filename="../configdialog.cpp" line="402"/>
        <location filename="../configdialog.cpp" line="414"/>
        <location filename="../configdialog.cpp" line="425"/>
        <location filename="../configdialog.cpp" line="436"/>
        <location filename="../configdialog.cpp" line="447"/>
        <location filename="../configdialog.cpp" line="458"/>
        <location filename="../configdialog.cpp" line="469"/>
        <location filename="../configdialog.cpp" line="480"/>
        <location filename="../configdialog.cpp" line="491"/>
        <location filename="../configdialog.cpp" line="502"/>
        <location filename="../configdialog.cpp" line="513"/>
        <location filename="../configdialog.cpp" line="524"/>
        <location filename="../configdialog.cpp" line="535"/>
        <location filename="../configdialog.cpp" line="546"/>
        <source>Browse program</source>
        <translation>تصفح البرنامج</translation>
    </message>
</context>
<context>
    <name>DocumentView</name>
    <message>
        <source>Unlock %1</source>
        <translation type="vanished">إفتح القفل %1</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="vanished">كلمة السّر:</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="513"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation>طباعة &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1052"/>
        <source>Loading pdf...</source>
        <translation>تحميل pdf...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1358"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1374"/>
        <source>Number of words in the document</source>
        <translation>عدد الكلمات في الوثيقة</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1384"/>
        <source>Save As</source>
        <translation>إحفظ كَ</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1419"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1473"/>
        <source>Number of words in the page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncodingDialog</name>
    <message>
        <location filename="../encodingdialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="20"/>
        <source>It seems that this file cannot be correctly decoded
with the default encoding setting</source>
        <translation>يبدو أنّ هذا الملف لا يُمكن فكّ ترميزه بشكل صحيح باستعمال ضبط الترميز الأصلي</translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="31"/>
        <source>Use this encoding :</source>
        <translation>إستعمل هذا الترميز :</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../exportdialog.ui" line="14"/>
        <source>Export via TeX4ht</source>
        <translation>صدِّر عن طريق TeX4ht</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="22"/>
        <source>Path to htlatex</source>
        <translation>المسار إلى htlatex</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="45"/>
        <source>Mode</source>
        <translation>أسلوب</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="51"/>
        <source>export to Html</source>
        <translation>صدِّر إلى Html</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="61"/>
        <source>export to Html (new page for each section)</source>
        <translation>صدِّر إلى Html (صفحة جديدة لِكلّ قِسم)</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="68"/>
        <source>export to OpenDocumentFormat</source>
        <translation>صدِّر إلى OpenDocumentFormat</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="75"/>
        <source>export to MathML</source>
        <translation>صدِّر إلى MathML</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="82"/>
        <source>User</source>
        <translation>المستخدم</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="91"/>
        <source>Options</source>
        <translation>خيارات</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="125"/>
        <source>Run</source>
        <translation>شغّل</translation>
    </message>
    <message>
        <location filename="../exportdialog.cpp" line="49"/>
        <source>Browse program</source>
        <translation>تصفح البرنامج</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <location filename="../filechooser.ui" line="31"/>
        <source>File</source>
        <translation>ملف</translation>
    </message>
    <message>
        <location filename="../filechooser.cpp" line="44"/>
        <source>Select a File</source>
        <translation>إختر ملفّا</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <location filename="../findwidget.ui" line="23"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="47"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="57"/>
        <source>Forward</source>
        <translation>إلى الأمام</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="67"/>
        <source>Backward</source>
        <translation>إلى الخلف</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="92"/>
        <source>Whole words only</source>
        <translation>فقط كلمات كاملة</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="99"/>
        <source>Case sensitive</source>
        <translation>طابق الحالة</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="106"/>
        <source>Start at Beginning</source>
        <translation>إبدأ من البداية</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="163"/>
        <source>Regular Expression</source>
        <translation type="unfinished">تعبير نمطي</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="170"/>
        <location filename="../findwidget.cpp" line="158"/>
        <source>Text must be selected before checking this option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="173"/>
        <source>In selection only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RegularExpression</source>
        <translation type="obsolete">تعبير نمطي</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Invalid regular expression.</source>
        <translation>تعبير نمطي باطل.</translation>
    </message>
</context>
<context>
    <name>GotoLineWidget</name>
    <message>
        <location filename="../gotolinewidget.ui" line="20"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="59"/>
        <source>Line</source>
        <translation>سطر</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="37"/>
        <source>Goto</source>
        <translation>إذهب إلى</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">غلق</translation>
    </message>
</context>
<context>
    <name>GraphicFileChooser</name>
    <message>
        <location filename="../graphicfilechooser.ui" line="36"/>
        <source>File</source>
        <translation>ملف</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="95"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="221"/>
        <source>Use &quot;figure&quot; environment</source>
        <translation>إستعمل محيط &quot;figure&quot;</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="152"/>
        <source>Caption</source>
        <translation>شرح</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="176"/>
        <source>Above</source>
        <translation>فوق</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="181"/>
        <source>Below</source>
        <translation>تحت</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="195"/>
        <source>Placement</source>
        <translation>وضع</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="208"/>
        <source>hbtp</source>
        <translation>hbtp</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="233"/>
        <source>Center</source>
        <translation>مركز</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.cpp" line="49"/>
        <source>Select a File</source>
        <translation>إختر ملفّا</translation>
    </message>
</context>
<context>
    <name>KeySequenceDialog</name>
    <message>
        <location filename="../keysequencedialog.ui" line="14"/>
        <source>Shortcut</source>
        <translation>إختصار</translation>
    </message>
    <message>
        <location filename="../keysequencedialog.ui" line="37"/>
        <location filename="../keysequencedialog.cpp" line="43"/>
        <source>Clear</source>
        <translation>مسح</translation>
    </message>
</context>
<context>
    <name>LatexEditor</name>
    <message>
        <location filename="../latexeditor.cpp" line="914"/>
        <source>Undo</source>
        <translation>تراجع</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="917"/>
        <source>Redo</source>
        <translation>كرّر</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="921"/>
        <source>Cut</source>
        <translation>قصّ</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="924"/>
        <source>Copy</source>
        <translation>إنسخ</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="927"/>
        <source>Paste</source>
        <translation>لصّق</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="932"/>
        <source>Select All</source>
        <translation>إختر الكلّ</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="938"/>
        <source>Check Spelling Word</source>
        <translation>دقّق إملائيّا في الكلمة</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="943"/>
        <source>Check Spelling Selection</source>
        <translation>دقّق إملائيّا في الإختيار</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="946"/>
        <source>Check Spelling Document</source>
        <translation>دقّق إملائيّا في الوثيقة</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="953"/>
        <source>Jump to the end of this block</source>
        <translation>إذهب إلى آخر هذه الكتلة</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="959"/>
        <source>Jump to pdf</source>
        <translation>إذهب إلى pdf</translation>
    </message>
</context>
<context>
    <name>LetterDialog</name>
    <message>
        <location filename="../letterdialog.ui" line="40"/>
        <source>Typeface Size</source>
        <translation>حجم نمط الخطّ</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="53"/>
        <source>Encoding</source>
        <translation>ترميز</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="60"/>
        <source>AMS Packages</source>
        <translation>حزمة AMS</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="79"/>
        <source>Paper Size</source>
        <translation>حجم الورقة</translation>
    </message>
    <message>
        <location filename="../letterdialog.cpp" line="56"/>
        <source>Quick Letter</source>
        <translation>حرف سريع</translation>
    </message>
</context>
<context>
    <name>LightFindWidget</name>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Invalid regular expression.</source>
        <translation>تعبير نمطي باطل.</translation>
    </message>
</context>
<context>
    <name>LightLatexEditor</name>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>You do not have read permission to this file.</source>
        <translation>لا تملك رخصة قراءة هذا الملف.</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="900"/>
        <location filename="../lightlatexeditor.cpp" line="903"/>
        <location filename="../lightlatexeditor.cpp" line="906"/>
        <source>Click to jump to the bookmark</source>
        <translation>أنقر لتذهب إلى المرجعية</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="910"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="913"/>
        <source>Goto Line</source>
        <translation>إذهب إلى السّطر</translation>
    </message>
</context>
<context>
    <name>LightLineNumberWidget</name>
    <message>
        <location filename="../lightlinenumberwidget.cpp" line="181"/>
        <source>Click to add or remove a bookmark</source>
        <translation>أنقر لِتُضيف أو لِتُزيل مرجعية</translation>
    </message>
</context>
<context>
    <name>LineNumberWidget</name>
    <message>
        <location filename="../linenumberwidget.cpp" line="286"/>
        <source>Click to add or remove a bookmark</source>
        <translation>أنقر لِتُضيف أو لِتُزيل مرجعية</translation>
    </message>
</context>
<context>
    <name>PageItem</name>
    <message>
        <location filename="../pageitem.cpp" line="137"/>
        <location filename="../pageitem.cpp" line="139"/>
        <source>Click to jump to the line</source>
        <translation>أنقر لتذهب إلى السّطر</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="146"/>
        <source>Number of words in the document</source>
        <translation>عدد الكلمات في الوثيقة</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="150"/>
        <source>Number of words in the page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="154"/>
        <source>Convert page to png image</source>
        <translation>حوّل الصّفحة إلى صورة png</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="158"/>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="unfinished">دقّق إملائيّا ونحويّا في هذه الصّفحة</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="162"/>
        <source>Open the file browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="547"/>
        <source>Copy text</source>
        <translation>إنسخ النصّ</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="548"/>
        <source>Copy image</source>
        <translation>إنسخ الصّورة</translation>
    </message>
</context>
<context>
    <name>PdfViewer</name>
    <message>
        <location filename="../pdfviewer.cpp" line="110"/>
        <location filename="../pdfviewer.cpp" line="153"/>
        <location filename="../pdfviewer.cpp" line="1110"/>
        <source>Structure</source>
        <translation>هيكل</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="134"/>
        <location filename="../pdfviewer.cpp" line="1117"/>
        <source>Pages</source>
        <translation>صفحات</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="184"/>
        <source>&amp;File</source>
        <translation>&amp;ملف</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="188"/>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="190"/>
        <source>&amp;Edit</source>
        <translation>&amp;حرِّر</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="191"/>
        <location filename="../pdfviewer.cpp" line="288"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="201"/>
        <source>Previous</source>
        <translation>السّابق</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="205"/>
        <source>Next</source>
        <translation>اللّاحق</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="211"/>
        <source>&amp;View</source>
        <translation>&amp;إعرض</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="212"/>
        <source>Fit Width</source>
        <translation>ناسب العرض</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="215"/>
        <source>Fit Page</source>
        <translation>صفحة كاملة</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="218"/>
        <source>Zoom In</source>
        <translation>تكبير</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="223"/>
        <source>Zoom Out</source>
        <translation>تصغيير</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="228"/>
        <source>Continuous</source>
        <translation>متواصل</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="233"/>
        <source>Two pages</source>
        <translation>صفحتان</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="238"/>
        <source>Rotate left</source>
        <translation>دوّر إلى اليسار</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="242"/>
        <source>Rotate right</source>
        <translation>دوّر إلى اليمين</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="246"/>
        <source>Presentation...</source>
        <translation>عرض...</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="255"/>
        <source>Previous Position</source>
        <translation>الموضع السّابق</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="260"/>
        <source>Next Position</source>
        <translation>الموضع اللّاحق</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="299"/>
        <location filename="../pdfviewer.cpp" line="992"/>
        <source>Print</source>
        <translation>طباعة</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="302"/>
        <source>External Viewer</source>
        <translation>عارض خارجي</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">دقّق إملائيّا ونحويّا في هذه الصّفحة</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>File not found</source>
        <translation>ملف غير موجود</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="441"/>
        <location filename="../pdfviewer.cpp" line="669"/>
        <source>Page</source>
        <translation>صفحة</translation>
    </message>
</context>
<context>
    <name>PdfViewerWidget</name>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="104"/>
        <source>Show/Hide Table of contents</source>
        <translation>عرض أو إخفاء فهرس المحتويات</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="110"/>
        <source>Previous</source>
        <translation>السّابق</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="114"/>
        <source>Next</source>
        <translation>اللّاحق</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="120"/>
        <source>Fit Width</source>
        <translation>ناسب العرض</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="123"/>
        <source>Fit Page</source>
        <translation>صفحة كاملة</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="126"/>
        <source>Zoom In</source>
        <translation>تكبير</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="131"/>
        <source>Zoom Out</source>
        <translation>تصغيير</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="144"/>
        <source>Continuous</source>
        <translation>متواصل</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="149"/>
        <source>Two pages</source>
        <translation>صفحتان</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="154"/>
        <source>Rotate left</source>
        <translation>دوّر إلى اليسار</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="158"/>
        <source>Rotate right</source>
        <translation>دوّر إلى اليمين</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="162"/>
        <source>Presentation...</source>
        <translation>عرض...</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="195"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="206"/>
        <source>Previous Position</source>
        <translation>الموضع السّابق</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="211"/>
        <source>Next Position</source>
        <translation>الموضع اللّاحق</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="220"/>
        <location filename="../pdfviewerwidget.cpp" line="928"/>
        <source>Print</source>
        <translation>طباعة</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="223"/>
        <source>External Viewer</source>
        <translation>عارض خارجي</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">دقّق إملائيّا ونحويّا في هذه الصّفحة</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>File not found</source>
        <translation>ملف غير موجود</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>unknown</source>
        <translation type="vanished">مجهول</translation>
    </message>
    <message>
        <source>Type 1</source>
        <translation type="vanished">نوع 1</translation>
    </message>
    <message>
        <source>Type 1C</source>
        <translation type="vanished">نوع 1C</translation>
    </message>
    <message>
        <source>Type 3</source>
        <translation type="vanished">نوع 3</translation>
    </message>
    <message>
        <source>TrueType</source>
        <translation type="vanished">TrueType</translation>
    </message>
    <message>
        <source>CID Type 0</source>
        <translation type="vanished">CID نوع 0</translation>
    </message>
    <message>
        <source>CID Type 0C</source>
        <translation type="vanished">CID نوع 0C</translation>
    </message>
    <message>
        <source>CID TrueType</source>
        <translation type="vanished">CID TrueType</translation>
    </message>
    <message>
        <source>Type 1C (OpenType)</source>
        <translation type="vanished">نوع 1C (OpenType)</translation>
    </message>
    <message>
        <source>TrueType (OpenType)</source>
        <translation type="vanished">TrueType (OpenType)</translation>
    </message>
    <message>
        <source>CID Type 0C (OpenType)</source>
        <translation type="vanished">CID نوع 0C (OpenType)</translation>
    </message>
    <message>
        <source>CID TrueType (OpenType)</source>
        <translation type="vanished">CID TrueType (OpenType)</translation>
    </message>
    <message>
        <source>Bug: unexpected font type. Notify poppler mailing list!</source>
        <translation type="vanished">خطأ: نوع خطّ غير متوقَّع. أبلغ القائمة البريدية لِ poppler!</translation>
    </message>
</context>
<context>
    <name>QuickBeamerDialog</name>
    <message>
        <location filename="../quickbeamerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="22"/>
        <source>AMS Packages</source>
        <translation type="unfinished">حزمة AMS</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="29"/>
        <source>Encoding</source>
        <translation type="unfinished">ترميز</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="39"/>
        <source>Typeface Size</source>
        <translation type="unfinished">حجم نمط الخطّ</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="46"/>
        <source>babel Package</source>
        <translation type="unfinished">حزمة babel</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="59"/>
        <source>graphicx Package</source>
        <translation type="unfinished">حزمة graphicx</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="66"/>
        <source>Title</source>
        <translation type="unfinished">عنوان</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="79"/>
        <source>Author</source>
        <translation type="unfinished">مؤلّف</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="86"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.cpp" line="23"/>
        <source>Quick Start</source>
        <translation type="unfinished">بداية سريعة</translation>
    </message>
</context>
<context>
    <name>QuickDocumentDialog</name>
    <message>
        <location filename="../quickdocumentdialog.ui" line="157"/>
        <source>Author</source>
        <translation>مؤلّف</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="41"/>
        <location filename="../quickdocumentdialog.ui" line="70"/>
        <location filename="../quickdocumentdialog.ui" line="115"/>
        <location filename="../quickdocumentdialog.ui" line="144"/>
        <location filename="../quickdocumentdialog.ui" line="199"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="25"/>
        <source>Document Class</source>
        <translation>فئة الوثيقة</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="173"/>
        <source>Title</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="183"/>
        <source>babel Package</source>
        <translation>حزمة babel</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="206"/>
        <source>geometry Package</source>
        <translation>حزمة geometry</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="213"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation>left=2cm,right=2cm,top=2cm,bottom=2cm</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="220"/>
        <source>AMS Packages</source>
        <translation>حزمة AMS</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="237"/>
        <source>graphicx Package</source>
        <translation>حزمة graphicx</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="244"/>
        <source>lmodern Package</source>
        <translation>حزمة lmodern</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="251"/>
        <source>Kpfonts Package</source>
        <translation>حزمة Kpfonts</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="258"/>
        <source>Fourier Package</source>
        <translation>حزمة Fourier</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="83"/>
        <source>Typeface Size</source>
        <translation>حجم نمط الخطّ</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="99"/>
        <source>Paper Size</source>
        <translation>حجم الورقة</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="54"/>
        <source>Other Options</source>
        <translation>خيارات أخرى</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="128"/>
        <source>Encoding</source>
        <translation>ترميز</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="230"/>
        <source>makeidx Package</source>
        <translation>makeidx Package</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.cpp" line="32"/>
        <source>Quick Start</source>
        <translation>بداية سريعة</translation>
    </message>
</context>
<context>
    <name>QuickXelatexDialog</name>
    <message>
        <location filename="../quickxelatexdialog.ui" line="25"/>
        <location filename="../quickxelatexdialog.ui" line="83"/>
        <location filename="../quickxelatexdialog.ui" line="115"/>
        <location filename="../quickxelatexdialog.ui" line="170"/>
        <source>+</source>
        <translation type="unfinished">+</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="38"/>
        <source>Other Options</source>
        <translation type="unfinished">خيارات أخرى</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="51"/>
        <source>Document Class</source>
        <translation type="unfinished">فئة الوثيقة</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="70"/>
        <source>Paper Size</source>
        <translation type="unfinished">حجم الورقة</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="96"/>
        <source>Typeface Size</source>
        <translation type="unfinished">حجم نمط الخطّ</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="128"/>
        <source>Author</source>
        <translation type="unfinished">مؤلّف</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="147"/>
        <source>Title</source>
        <translation type="unfinished">عنوان</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="154"/>
        <source>polyglossia Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="177"/>
        <source>geometry Package</source>
        <translation type="unfinished">حزمة geometry</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="184"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation type="unfinished">left=2cm,right=2cm,top=2cm,bottom=2cm</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="191"/>
        <source>AMS Packages</source>
        <translation type="unfinished">حزمة AMS</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="201"/>
        <source>graphicx Package</source>
        <translation type="unfinished">حزمة graphicx</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.cpp" line="31"/>
        <source>Quick Start</source>
        <translation type="unfinished">بداية سريعة</translation>
    </message>
</context>
<context>
    <name>RefDialog</name>
    <message>
        <location filename="../refdialog.ui" line="14"/>
        <location filename="../refdialog.ui" line="40"/>
        <source>Labels</source>
        <translation>تمييزات</translation>
    </message>
</context>
<context>
    <name>ReplaceWidget</name>
    <message>
        <location filename="../replacewidget.ui" line="20"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="34"/>
        <location filename="../replacewidget.ui" line="60"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="70"/>
        <source>Forward</source>
        <translation>إلى الأمام</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="222"/>
        <location filename="../replacewidget.cpp" line="251"/>
        <source>Text must be selected before checking this option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="225"/>
        <source>In selection only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="130"/>
        <source>Backward</source>
        <translation>إلى الخلف</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="97"/>
        <source>Replace</source>
        <translation>تعويض</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="123"/>
        <source>Replace All</source>
        <translation>تعويض الكلّ</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="158"/>
        <source>Whole words only</source>
        <translation>فقط كلمات كاملة</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="165"/>
        <source>Case sensitive</source>
        <translation>طابق الحالة</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="172"/>
        <source>Start at Beginning</source>
        <translation>إبدأ من البداية</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="215"/>
        <source>Regular Expression</source>
        <translation>تعبير نمطي</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Invalid regular expression.</source>
        <translation>تعبير نمطي باطل.</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Replace this occurrence ? </source>
        <translation>عوّض هذا التواجد؟</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Yes</source>
        <translation>نعم</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>No</source>
        <translation>لا</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../scandialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="20"/>
        <source>In directory :</source>
        <translation>في المجلد :</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="57"/>
        <source>Text :</source>
        <translation>نَصّ :</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="67"/>
        <location filename="../scandialog.cpp" line="43"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="74"/>
        <source>Include subdirectories</source>
        <translation>إشمل المجلدات الفرعية</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="81"/>
        <source>Abort</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="100"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
</context>
<context>
    <name>SourceView</name>
    <message>
        <location filename="../sourceview.cpp" line="50"/>
        <source>Open</source>
        <translation>إفتح</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="61"/>
        <source>Check differences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="167"/>
        <source>Open File</source>
        <translation>إفتح ملفّا</translation>
    </message>
</context>
<context>
    <name>SpellerDialog</name>
    <message>
        <location filename="../spellerdialog.ui" line="13"/>
        <source>Check Spelling</source>
        <translation>دقّق إملائيّا</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="19"/>
        <source>Unknown word:</source>
        <translation>كلمة مجهولة:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="33"/>
        <source>Replace</source>
        <translation>تعويض</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="43"/>
        <source>Replace with:</source>
        <translation>تعويض بِ:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="53"/>
        <source>Ignore</source>
        <translation>تجاهل</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="73"/>
        <source>Always ignore</source>
        <translation>تجاهل دائما</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="80"/>
        <source>Suggested words :</source>
        <translation>كلمات مُقترَحة :</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="130"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="121"/>
        <source>Check spelling selection...</source>
        <translation>دقّق إملائيّا في الإختيار...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="131"/>
        <source>Check spelling from cursor...</source>
        <translation>دقّق إملائيّا من المؤشّر...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="152"/>
        <location filename="../spellerdialog.cpp" line="169"/>
        <location filename="../spellerdialog.cpp" line="193"/>
        <location filename="../spellerdialog.cpp" line="319"/>
        <source>No more misspelled words</source>
        <translation>لا مزيد من الإخطاء الإملائية</translation>
    </message>
</context>
<context>
    <name>StructDialog</name>
    <message>
        <location filename="../structdialog.ui" line="14"/>
        <source>Structure</source>
        <translation>هيكل</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="37"/>
        <source>Numeration</source>
        <translation>ترقيم</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="53"/>
        <source>Title</source>
        <translation>عنوان</translation>
    </message>
</context>
<context>
    <name>SymbolListWidget</name>
    <message>
        <location filename="../symbollistwidget.cpp" line="52"/>
        <source>Add to favorites</source>
        <translation>أضف إلى القائمة المفضلة</translation>
    </message>
    <message>
        <location filename="../symbollistwidget.cpp" line="53"/>
        <source>Remove from favorites</source>
        <translation>أزِل من القائمة المفضلة</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <location filename="../tabdialog.ui" line="37"/>
        <source>Num of Columns</source>
        <translation>عدد الأعمدة</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="78"/>
        <source>Columns</source>
        <translation>أعمدة</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="86"/>
        <source>Column :</source>
        <translation>عمود :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="113"/>
        <source>Alignment :</source>
        <translation>تراصف :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="127"/>
        <source>Left Border :</source>
        <translation>الحدّ الأيسر :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="167"/>
        <source>Apply to all columns</source>
        <translation>طبّق على كلّ الأعمدة</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="194"/>
        <source>Right Border (last column) :</source>
        <translation>الحدّ الأيسر (آخر عمود) :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="263"/>
        <source>Rows</source>
        <translation>صفوف</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="271"/>
        <source>Row :</source>
        <translation>صف :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="296"/>
        <source>Top Border</source>
        <translation>الحدّ الأعلى</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="305"/>
        <source>Merge columns :</source>
        <translation>أدمج الأعمدة :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="321"/>
        <source>-&gt;</source>
        <translation>-&gt;</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="364"/>
        <source>Apply to all rows</source>
        <translation>طبّق على كلّ الصّفوف</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="389"/>
        <source>Bottom Border (last row)</source>
        <translation>الحدّ الأسفل (آخر صفّ)</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="396"/>
        <source>Add vertical margin for each row</source>
        <translation>أضف هامشا عموديا لكلّ صفّ</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="232"/>
        <source>Num of Rows</source>
        <translation>عدد الصفوف</translation>
    </message>
    <message>
        <location filename="../tabdialog.cpp" line="108"/>
        <source>Quick Tabular</source>
        <translation>Tabular سريع</translation>
    </message>
</context>
<context>
    <name>TabbingDialog</name>
    <message>
        <location filename="../tabbingdialog.ui" line="67"/>
        <source>Spacing</source>
        <translation>مساحة</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="80"/>
        <source>Num of Rows</source>
        <translation>عدد الصفوف</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="93"/>
        <source>Num of Columns</source>
        <translation>عدد الأعمدة</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.cpp" line="27"/>
        <source>Quick Tabbing</source>
        <translation>تبويب سريع</translation>
    </message>
</context>
<context>
    <name>TexdocDialog</name>
    <message>
        <location filename="../texdocdialog.ui" line="20"/>
        <source>Search documentation about :</source>
        <translation>إبحث في التوثيق عن :</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="59"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="116"/>
        <source>Texdoc command :</source>
        <translation>أمر Texdoc :</translation>
    </message>
    <message>
        <location filename="../texdocdialog.cpp" line="32"/>
        <source>Browse program</source>
        <translation>تصفح البرنامج</translation>
    </message>
</context>
<context>
    <name>Texmaker</name>
    <message>
        <location filename="../texmaker.cpp" line="667"/>
        <location filename="../texmaker.cpp" line="837"/>
        <location filename="../texmaker.cpp" line="2658"/>
        <location filename="../texmaker.cpp" line="5806"/>
        <source>Structure</source>
        <translation>هيكل</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="686"/>
        <location filename="../texmaker.cpp" line="5811"/>
        <source>Relation symbols</source>
        <translation>رموز العلاقات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="696"/>
        <location filename="../texmaker.cpp" line="5816"/>
        <source>Arrow symbols</source>
        <translation>رموز السّهام</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="705"/>
        <location filename="../texmaker.cpp" line="5821"/>
        <source>Miscellaneous symbols</source>
        <translation>رموز أخرى</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="714"/>
        <location filename="../texmaker.cpp" line="5826"/>
        <source>Delimiters</source>
        <translation>محدّدات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="723"/>
        <location filename="../texmaker.cpp" line="5831"/>
        <source>Greek letters</source>
        <translation>حروف إغريقية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="732"/>
        <location filename="../texmaker.cpp" line="5836"/>
        <source>Most used symbols</source>
        <translation>الرموز الأكثر إستخداما</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="741"/>
        <location filename="../texmaker.cpp" line="5841"/>
        <source>Favorites symbols</source>
        <translation>الرموز المفضلة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="780"/>
        <location filename="../texmaker.cpp" line="813"/>
        <location filename="../texmaker.cpp" line="5846"/>
        <source>Pstricks Commands</source>
        <translation>أوامر Pstricks</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="788"/>
        <location filename="../texmaker.cpp" line="816"/>
        <location filename="../texmaker.cpp" line="5856"/>
        <source>MetaPost Commands</source>
        <translation>أوامر MetaPost</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="796"/>
        <location filename="../texmaker.cpp" line="819"/>
        <location filename="../texmaker.cpp" line="5861"/>
        <source>Tikz Commands</source>
        <translation>أوامر Tikz</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="804"/>
        <location filename="../texmaker.cpp" line="822"/>
        <location filename="../texmaker.cpp" line="5866"/>
        <source>Asymptote Commands</source>
        <translation>أوامر Asymptote</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2663"/>
        <source>Messages / Log File</source>
        <translation>رسائل / ملف السّجلات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1215"/>
        <source>Toggle between the master document and the current document</source>
        <translation>بدّل بين الوثيقة الرئيسية والوثيقة الآنيّة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1103"/>
        <source>Bold</source>
        <translation>غامق</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1108"/>
        <source>Italic</source>
        <translation>مائل</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1118"/>
        <source>Left</source>
        <translation>يسار</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1123"/>
        <source>Center</source>
        <translation>مركز</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1128"/>
        <source>Right</source>
        <translation>يمين</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1134"/>
        <location filename="../texmaker.cpp" line="1174"/>
        <source>New line</source>
        <translation>سطر جديد</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1383"/>
        <location filename="../texmaker.cpp" line="11064"/>
        <source>Normal Mode</source>
        <translation>نسق عادي</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1407"/>
        <source>&amp;File</source>
        <translation>&amp;ملف</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1408"/>
        <location filename="../texmaker.cpp" line="1409"/>
        <location filename="../texmaker.cpp" line="2914"/>
        <location filename="../texmaker.cpp" line="2915"/>
        <source>New</source>
        <translation>جديد</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1414"/>
        <source>New by copying an existing file</source>
        <translation>جديد بِنسخ ملف موجود</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1418"/>
        <location filename="../texmaker.cpp" line="1419"/>
        <location filename="../texmaker.cpp" line="2919"/>
        <location filename="../texmaker.cpp" line="2920"/>
        <source>Open</source>
        <translation>إفتح</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1424"/>
        <source>Open Recent</source>
        <translation>إفتح ملفّا فُتِحَ مؤخرا</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1438"/>
        <source>Restore previous session</source>
        <translation>إسترجاع جلسة سابقة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1450"/>
        <location filename="../texmaker.cpp" line="1451"/>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="12260"/>
        <source>Save</source>
        <translation>إحفظ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1458"/>
        <location filename="../texmaker.cpp" line="4020"/>
        <location filename="../texmaker.cpp" line="4093"/>
        <location filename="../texmaker.cpp" line="5732"/>
        <source>Save As</source>
        <translation>إحفظ كَ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1462"/>
        <source>Save All</source>
        <translation>إحفظ الكلّ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1242"/>
        <location filename="../texmaker.cpp" line="1470"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1476"/>
        <source>Close All</source>
        <translation>غلق الكلّ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1480"/>
        <source>Reload document from file</source>
        <translation>إعادة تحميل الوثيقة من الملف</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1488"/>
        <source>Print</source>
        <translation>طباعة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1494"/>
        <location filename="../texmaker.cpp" line="1495"/>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1501"/>
        <source>&amp;Edit</source>
        <translation>&amp;حرِّر</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1502"/>
        <location filename="../texmaker.cpp" line="1503"/>
        <source>Undo</source>
        <translation>تراجع</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1508"/>
        <location filename="../texmaker.cpp" line="1509"/>
        <source>Redo</source>
        <translation>كرّر</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1515"/>
        <location filename="../texmaker.cpp" line="1516"/>
        <source>Copy</source>
        <translation>إنسخ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1521"/>
        <location filename="../texmaker.cpp" line="1522"/>
        <source>Cut</source>
        <translation>قصّ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1527"/>
        <location filename="../texmaker.cpp" line="1528"/>
        <source>Paste</source>
        <translation>لصّق</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1533"/>
        <source>Select All</source>
        <translation>إختر الكلّ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1539"/>
        <source>Comment</source>
        <translation>حوّل إلى تعليق</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1545"/>
        <source>Uncomment</source>
        <translation>ألغ التّحويل إلى تعليق</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1551"/>
        <source>Indent</source>
        <translation>أضِف مسافة بادئة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1557"/>
        <source>Unindent</source>
        <translation>أزِل مسافة بادئة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1663"/>
        <source>Find</source>
        <translation>أعثر</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1669"/>
        <source>FindNext</source>
        <translation>أعثر على المُوالي</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1680"/>
        <source>Replace</source>
        <translation>تعويض</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1686"/>
        <source>Goto Line</source>
        <translation>إذهب إلى السّطر</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1693"/>
        <source>Check Spelling</source>
        <translation>دقّق إملائيّا</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1700"/>
        <source>Refresh Structure</source>
        <translation>أنعش الهيكل</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1706"/>
        <source>Refresh Bibliography</source>
        <translation>أنعش قائمة المراجع</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1711"/>
        <source>&amp;Tools</source>
        <translation>&amp;أدوات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1712"/>
        <location filename="../texmaker.cpp" line="2969"/>
        <location filename="../texmaker.cpp" line="9757"/>
        <source>Quick Build</source>
        <translation>بناء سريع</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1723"/>
        <location filename="../texmaker.cpp" line="2995"/>
        <source>View Dvi</source>
        <translation>إعرض Dvi</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1733"/>
        <location filename="../texmaker.cpp" line="2996"/>
        <source>View PS</source>
        <translation>إعرض PS</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1743"/>
        <location filename="../texmaker.cpp" line="2997"/>
        <source>View PDF</source>
        <translation>إعرض PDF</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1758"/>
        <location filename="../texmaker.cpp" line="3010"/>
        <source>View Log</source>
        <translation>إعرض السّجلات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1433"/>
        <location filename="../texmaker.cpp" line="1799"/>
        <location filename="../texmaker.cpp" line="2547"/>
        <source>Clean</source>
        <translation>مسح</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="764"/>
        <location filename="../texmaker.cpp" line="5872"/>
        <source>User</source>
        <translation>المستخدم</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1437"/>
        <source>Session</source>
        <translation>جلسة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1443"/>
        <source>Save session</source>
        <translation>إحفظ جلسة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1446"/>
        <source>Load session</source>
        <translation>حمّل جلسة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1466"/>
        <source>Save A Copy</source>
        <translation>إحفظ نسخة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1484"/>
        <source>Reload all documents from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1564"/>
        <source>Fold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1568"/>
        <source>Unfold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1573"/>
        <source>&amp;Fold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1616"/>
        <source>&amp;Unfold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1675"/>
        <source>Find In Directory</source>
        <translation>أعثر في مجلد</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1804"/>
        <source>Open Terminal</source>
        <translation>إفتح وحدة تحكم Terminal</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1808"/>
        <source>Export via TeX4ht</source>
        <translation>صدِّر عن طريق TeX4ht</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1813"/>
        <source>Convert to unicode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1818"/>
        <location filename="../texmaker.cpp" line="3020"/>
        <source>Previous LaTeX Error</source>
        <translation>الخطأ السابق لِ LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1820"/>
        <location filename="../texmaker.cpp" line="3014"/>
        <source>Next LaTeX Error</source>
        <translation>الخطأ اللاحق لِ LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1823"/>
        <source>&amp;LaTeX</source>
        <translation>&amp;LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1857"/>
        <source>&amp;Sectioning</source>
        <translation>&amp;تقسيم</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1887"/>
        <source>&amp;Environment</source>
        <translation>&amp;محيط</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1933"/>
        <source>&amp;List Environment</source>
        <translation>محيط &amp;قائمة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1957"/>
        <source>Font St&amp;yles</source>
        <translation>نمط ال&amp;خطّ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1994"/>
        <source>&amp;Tabular Environment</source>
        <translation>محيط &amp;Tabular</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2020"/>
        <source>S&amp;pacing</source>
        <translation>&amp;مساحة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2047"/>
        <source>International &amp;Accents</source>
        <translation>&amp;لهجات دولية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2089"/>
        <source>International &amp;Quotes</source>
        <translation>إقتباسات &amp;دولية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2138"/>
        <source>&amp;Math</source>
        <translation>&amp;رياضيّات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2139"/>
        <source>Inline math mode $...$</source>
        <translation>نمط رياضيّات على السّطر $...$</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2144"/>
        <source>Display math mode \[...\]</source>
        <translation>نمط إبراز رياضيّات \[...\]</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2149"/>
        <source>Numbered equations \begin{equation}</source>
        <translation>مُعادَلات مُرقّمة \begin{equation}</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2202"/>
        <source>Math &amp;Functions</source>
        <translation>&amp;وظائف رياضيّات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2324"/>
        <source>Math Font St&amp;yles</source>
        <translation>&amp;أنماط خطوط الرّياضيّات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2358"/>
        <source>Math &amp;Accents</source>
        <translation>&amp;لهجات الرّياضيّات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2400"/>
        <source>Math S&amp;paces</source>
        <translation>&amp;مساحات الرّياضيّات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2422"/>
        <source>&amp;Wizard</source>
        <translation>&amp;السّاحر</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2423"/>
        <source>Quick Start</source>
        <translation>بداية سريعة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2426"/>
        <source>Quick Beamer Presentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2429"/>
        <source>Quick Xelatex Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2432"/>
        <source>Quick Letter</source>
        <translation>حرف سريع</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2436"/>
        <source>Quick Tabular</source>
        <translation>Tabular سريع</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2439"/>
        <source>Quick Tabbing</source>
        <translation>تبويب سريع</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2442"/>
        <source>Quick Array</source>
        <translation>مجموعة سريعة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2446"/>
        <source>&amp;Bibliography</source>
        <translation>قائمة ال&amp;مراجع</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2552"/>
        <source>&amp;User</source>
        <translation>&amp;المستخدم</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2553"/>
        <source>User &amp;Tags</source>
        <translation>كلمات المستخدم ال&amp;دلالية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2595"/>
        <location filename="../texmaker.cpp" line="8156"/>
        <location filename="../texmaker.cpp" line="8212"/>
        <source>Edit User &amp;Tags</source>
        <translation>&amp;حرِّر كلمات المستخدم الدلالية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2598"/>
        <source>User &amp;Commands</source>
        <translation>أوا&amp;مر المستخدم</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2620"/>
        <location filename="../texmaker.cpp" line="9717"/>
        <location filename="../texmaker.cpp" line="9753"/>
        <source>Edit User &amp;Commands</source>
        <translation>حرِّر أوامر الم&amp;ستخدم</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2624"/>
        <source>Customize Completion</source>
        <translation>تخصيص التكملة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2629"/>
        <source>Run script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2640"/>
        <source>Other script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2645"/>
        <source>&amp;View</source>
        <translation>&amp;إعرض</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2749"/>
        <source>Manage Settings File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2796"/>
        <source>Check for Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4774"/>
        <source>Browse script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8441"/>
        <source>A document must be saved with an extension (and without spaces or accents in the name) before being used by a command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Make a copy of the %1.pdf/ps document in the &quot;build&quot; subdirectory and delete all the others %1.* files?</source>
        <translation>هل أنسخ الوثيقة %1.pdf/ps في المجلد &quot;build&quot; وأمحو بقيّة الملفات %1.*?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1224"/>
        <location filename="../texmaker.cpp" line="2646"/>
        <source>Next Document</source>
        <translation>الوثيقة الموالية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1220"/>
        <location filename="../texmaker.cpp" line="2651"/>
        <source>Previous Document</source>
        <translation>الوثيقة السابقة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2701"/>
        <source>&amp;Options</source>
        <translation>&amp;خيارات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2702"/>
        <source>Configure Texmaker</source>
        <translation>إعدادات Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2707"/>
        <location filename="../texmaker.cpp" line="11055"/>
        <source>Define Current Document as &apos;Master Document&apos;</source>
        <translation>عرّف الوثيقة الحالية كوثيقة رئيسية</translation>
    </message>
    <message>
        <source>Interface Appearance</source>
        <translation type="vanished">مظهر الواجهة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2727"/>
        <source>Change Interface Font</source>
        <translation>غيّر خطّ الواجهة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2732"/>
        <source>Interface Language</source>
        <translation>لُغة الواجهة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2763"/>
        <source>&amp;Help</source>
        <translation>&amp;مساعدة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2764"/>
        <location filename="../texmaker.cpp" line="2765"/>
        <source>LaTeX Reference</source>
        <translation>مرجع LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2768"/>
        <location filename="../texmaker.cpp" line="2769"/>
        <source>User Manual</source>
        <translation>دليل المستخدم</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2801"/>
        <source>About Texmaker</source>
        <translation>حول Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2991"/>
        <source>Run</source>
        <translation>شغّل</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3004"/>
        <source>View</source>
        <translation>إعرض</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3026"/>
        <source>Stop Process</source>
        <translation>أوقف العملية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1261"/>
        <location filename="../texmaker.cpp" line="1264"/>
        <location filename="../texmaker.cpp" line="1267"/>
        <source>Click to jump to the bookmark</source>
        <translation>أنقر لتذهب إلى المرجعية</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2669"/>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Pdf Viewer</source>
        <translation>عارض Pdf</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2686"/>
        <source>List of opened files</source>
        <translation>قائمة الملفات المفتوحة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2693"/>
        <source>Full Screen</source>
        <translation>كامل الشاشة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2751"/>
        <source>Settings File</source>
        <translation>ملف الإعدادات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2753"/>
        <source>Reset Settings</source>
        <translation>إعادة تهيئة الإعدادات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2756"/>
        <source>Save a copy of the settings file</source>
        <translation>سجّل نسخة من ملف الإعدادات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2759"/>
        <source>Replace the settings file by a new one</source>
        <translation>عوّض ملف الإعدادات بِملف جديد</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="4753"/>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="8441"/>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10020"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <source>You do not have read permission to this file.</source>
        <translation>لا تملك رخصة قراءة هذا الملف.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3595"/>
        <location filename="../texmaker.cpp" line="3655"/>
        <location filename="../texmaker.cpp" line="12312"/>
        <source>Open File</source>
        <translation>إفتح ملفّا</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3751"/>
        <location filename="../texmaker.cpp" line="3900"/>
        <location filename="../texmaker.cpp" line="3959"/>
        <source>The document has been changed outside Texmaker.Do you want to reload it (and discard your changes) or save it (and overwrite the file)?</source>
        <translation>الوثيقة تغيّرت خارج Texmaker.هل تريد إعادة تحميلها (وتجاهل التغييرات التي أحدثتها) أو حفظها (والكتابة فوق الملف)?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <source>Reload the file</source>
        <translation>أعِد تحميل الملف</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>الملف لم يُمكن حفظه. تأكّد من أن لديك رخصة الكتابة في هذا الملف.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4130"/>
        <location filename="../texmaker.cpp" line="4194"/>
        <location filename="../texmaker.cpp" line="4264"/>
        <location filename="../texmaker.cpp" line="4370"/>
        <source>The document contains unsaved work. Do you want to save it before closing?</source>
        <translation>الوثيقة تحتوي على عمل غير محفوظ. هل تريد حفظه قبل الغلق؟</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Save and Close</source>
        <translation>سجّل وأغلق</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Don&apos;t Save and Close</source>
        <translation>لا تسجّل وتغلق</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4539"/>
        <source>The document contains unsaved work.You will lose changes by reloading the document.</source>
        <translation>الوثيقة تحتوي على عمل غير محفوظ. ستخسر التغييرات التي أحدثتها إن أعدت تحميل الوثيقة.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4753"/>
        <source>Error : Can&apos;t open the dictionary</source>
        <translation>خطأ: لا يمكن فتح القاموس</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5710"/>
        <source>Delete settings file?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>هل أمحو ملف الإعدادات؟
(Texmaker سيتم إغلاقه ويجب عليك إعادة فتحه)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Ok</source>
        <translation>تماما</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5753"/>
        <source>Replace settings file by a new one?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>هل أعوض ملف الإعدادات بآخر جديد؟
(Texmaker سيتم إغلاقه ويجب عليك إعادة فتحه)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5778"/>
        <source>Opened Files</source>
        <translation>ملفات مفتوحة</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="6667"/>
        <source>Select an image File</source>
        <translation>إختر صورة من ملف</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4311"/>
        <location filename="../texmaker.cpp" line="6698"/>
        <location filename="../texmaker.cpp" line="6723"/>
        <source>Select a File</source>
        <translation>إختر ملفّا</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <source>Can&apos;t detect the file name</source>
        <translation>لم يمكن كشف إسم الملف</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Could not start the command.</source>
        <translation>لم يمكن تشغيل الأمر.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete Files</source>
        <translation>إحذف الملفات</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete the output files generated by LaTeX ?</source>
        <translation>إحذف الملفات المنتجة من خلال LaTeX؟</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PdfLaTeX</source>
        <translation>عارض PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvi Viewer</source>
        <translation>عارض Dvi</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PS Viewer</source>
        <translation>عارض PS</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Bibtex</source>
        <translation>Bibtex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>metapost</source>
        <translation>metapost</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ghostscript</source>
        <translation>ghostscript</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Latexmk</source>
        <translation>Latexmk</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>R Sweave</source>
        <translation>R Sweave</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>XeLaTex</source>
        <translation>XeLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LuaLaTex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10020"/>
        <source>Log File not found !</source>
        <translation>ملف السّجلات غير موجود !</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10360"/>
        <location filename="../texmaker.cpp" line="10400"/>
        <source>Click to jump to the line</source>
        <translation>أنقر لتذهب إلى السّطر</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10474"/>
        <location filename="../texmaker.cpp" line="10509"/>
        <source>No LaTeX errors detected !</source>
        <translation>لم أكشف عن أخطاء LaTeX !</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <source>File not found</source>
        <translation>ملف غير موجود</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11079"/>
        <location filename="../texmaker.cpp" line="12629"/>
        <source>Normal Mode (current master document :</source>
        <translation>نسق عادي (الوثيقة الرئيسية الحالية :</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11081"/>
        <location filename="../texmaker.cpp" line="12631"/>
        <source>Master Document :</source>
        <translation>وثيقة رئيسية :</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11623"/>
        <source>The language setting will take effect after restarting the application.</source>
        <translation>إعدادات اللّغة سيتم تطبيقها بعد إعادة تشغيل البرنامج.</translation>
    </message>
    <message>
        <source>The appearance setting will take effect after restarting the application.</source>
        <translation type="vanished">إعدادات المنظر سيتم تطبيقها بعد إعادة تشغيل البرنامج.</translation>
    </message>
</context>
<context>
    <name>UnicodeDialog</name>
    <message>
        <location filename="../unicodedialog.ui" line="14"/>
        <location filename="../unicodedialog.ui" line="98"/>
        <source>Convert to unicode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="28"/>
        <source>File</source>
        <translation type="unfinished">ملف</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="75"/>
        <source>Original Encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="145"/>
        <source>Close</source>
        <translation type="unfinished">غلق</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="56"/>
        <source>Select a File</source>
        <translation type="unfinished">إختر ملفّا</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>Error</source>
        <translation type="unfinished">خطأ</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>You do not have read permission to this file.</source>
        <translation type="unfinished">لا تملك رخصة قراءة هذا الملف.</translation>
    </message>
</context>
<context>
    <name>UnicodeView</name>
    <message>
        <location filename="../unicodeview.cpp" line="56"/>
        <source>Save</source>
        <translation type="unfinished">إحفظ</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="152"/>
        <source>Save As</source>
        <translation type="unfinished">إحفظ كَ</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>Error</source>
        <translation type="unfinished">خطأ</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation type="unfinished">الملف لم يُمكن حفظه. تأكّد من أن لديك رخصة الكتابة في هذا الملف.</translation>
    </message>
</context>
<context>
    <name>UserCompletionDialog</name>
    <message>
        <location filename="../usercompletiondialog.ui" line="14"/>
        <source>Completion</source>
        <translation>تكملة</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="25"/>
        <source>Add</source>
        <translation>أضِف</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="48"/>
        <source>Delete</source>
        <translation>أزِل</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="55"/>
        <source>( @ : placeholder )</source>
        <translation>( @ : عنصر )</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="62"/>
        <source>( #bib# : bibliography item )</source>
        <translation>( #bib# : نقطة مرجعية )</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="69"/>
        <source>( #label# : label item )</source>
        <translation>( #label# : نقطة تمييز )</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="76"/>
        <source>Replace</source>
        <translation type="unfinished">تعويض</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="85"/>
        <source>Items already supplied by Texmaker</source>
        <translation>نقاط متوفرة حاليّا عن طريق Texmaker</translation>
    </message>
</context>
<context>
    <name>UserMenuDialog</name>
    <message>
        <location filename="../usermenudialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>حرِّر الكلمات الدلالية</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="45"/>
        <source>Menu Item</source>
        <translation>عنصر القائمة</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="55"/>
        <source>LaTeX Content</source>
        <translation>محتوى LaTeX</translation>
    </message>
</context>
<context>
    <name>UserQuickDialog</name>
    <message>
        <location filename="../userquickdialog.ui" line="14"/>
        <source>Quick Build Command</source>
        <translation>أمر بناء سريع</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="42"/>
        <source>Add</source>
        <translation>أضِف</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="70"/>
        <source>Ordered list of commands :</source>
        <translation>قائمة مرتبة من الأوامر :</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="80"/>
        <source>Up</source>
        <translation>أعلى</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="87"/>
        <source>Down</source>
        <translation>أسفل</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="94"/>
        <source>Delete</source>
        <translation>أزِل</translation>
    </message>
</context>
<context>
    <name>UserTagsListWidget</name>
    <message>
        <location filename="../usertagslistwidget.cpp" line="30"/>
        <source>Add tag</source>
        <translation>أضِف وسما</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="32"/>
        <source>Remove</source>
        <translation>أزِل</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="34"/>
        <source>Modify</source>
        <translation>بدّل</translation>
    </message>
</context>
<context>
    <name>UserToolDialog</name>
    <message>
        <location filename="../usertooldialog.ui" line="14"/>
        <source>Edit User Commands</source>
        <translation>حرِّر أوامر المستخدم</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="51"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(الأوامر يجب أن تُفصل بِ &apos;|&apos;)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="70"/>
        <source>Menu Item</source>
        <translation>عنصر القائمة</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="77"/>
        <source>Command (% : filename without extension)</source>
        <translation>أمر : (% : إسم الملف دون ملحق)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="103"/>
        <source>wizard</source>
        <translation>السّاحر</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../versiondialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation type="unfinished">Texmaker</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="20"/>
        <source>Check for available version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="34"/>
        <source>Available version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="48"/>
        <source>Go to the download page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="68"/>
        <source>You&apos;re using the version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../versiondialog.cpp" line="53"/>
        <location filename="../versiondialog.cpp" line="60"/>
        <source>Error</source>
        <translation type="unfinished">خطأ</translation>
    </message>
</context>
<context>
    <name>X11FontDialog</name>
    <message>
        <location filename="../x11fontdialog.ui" line="14"/>
        <source>Select a Font</source>
        <translation>إختر خطّا</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="34"/>
        <source>Font Family</source>
        <translation>عائلة الخطّ</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="44"/>
        <source>Font Size</source>
        <translation>حجم الخطّ</translation>
    </message>
</context>
</TS>
