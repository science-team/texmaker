<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.ui" line="14"/>
        <source>About Texmaker</source>
        <translation>A Texmaker névjegye</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="76"/>
        <source>Version</source>
        <translation>Verzió</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="86"/>
        <source>Authors</source>
        <translation>Szerzők</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="96"/>
        <source>Thanks</source>
        <translation>Köszönet</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="106"/>
        <source>License</source>
        <translation>Licenc</translation>
    </message>
</context>
<context>
    <name>AddOptionDialog</name>
    <message>
        <location filename="../addoptiondialog.ui" line="14"/>
        <source>New</source>
        <translation>Új</translation>
    </message>
</context>
<context>
    <name>AddTagDialog</name>
    <message>
        <location filename="../addtagdialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>Felhasználói sablonok szerkesztése</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="22"/>
        <source>Item</source>
        <translation>Elem</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="32"/>
        <source>LaTeX Content</source>
        <translation>LaTeX tartalom</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="55"/>
        <source>Keyboard Trigger</source>
        <translation>Billentyűparancs</translation>
    </message>
</context>
<context>
    <name>ArrayDialog</name>
    <message>
        <location filename="../arraydialog.ui" line="45"/>
        <source>Num of Columns</source>
        <translation>Oszlopok száma</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="59"/>
        <source>Columns Alignment</source>
        <translation>Oszlopok igazítása</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="76"/>
        <source>Environment</source>
        <translation>Környezet</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="35"/>
        <source>Num of Rows</source>
        <translation>Sorok száma</translation>
    </message>
    <message>
        <location filename="../arraydialog.cpp" line="43"/>
        <source>Quick Array</source>
        <translation>Tömb varázsló</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../browser.cpp" line="78"/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="79"/>
        <source>Print</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="81"/>
        <source>Exit</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="89"/>
        <source>Index</source>
        <translation>Tárgymutató</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="107"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../configdialog.ui" line="14"/>
        <source>Configure Texmaker</source>
        <translation>A Texmaker beállítása</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="130"/>
        <source>Commands (% : filename without extension - @ : line number)</source>
        <translation>Parancsok (% : fájlnév kiterjesztés nélkül - @ : sor száma)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="141"/>
        <location filename="../configdialog.cpp" line="610"/>
        <location filename="../configdialog.cpp" line="673"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="376"/>
        <location filename="../configdialog.cpp" line="616"/>
        <location filename="../configdialog.cpp" line="679"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="631"/>
        <location filename="../configdialog.cpp" line="694"/>
        <source>Bibtex</source>
        <translation>Bibtex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="366"/>
        <location filename="../configdialog.cpp" line="634"/>
        <location filename="../configdialog.cpp" line="697"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="755"/>
        <location filename="../configdialog.cpp" line="619"/>
        <location filename="../configdialog.cpp" line="682"/>
        <source>Dvi Viewer</source>
        <translation>Dvi megjelenítő</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="820"/>
        <location filename="../configdialog.cpp" line="622"/>
        <location filename="../configdialog.cpp" line="685"/>
        <source>PS Viewer</source>
        <translation>PS megjelenítő</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="155"/>
        <location filename="../configdialog.cpp" line="613"/>
        <location filename="../configdialog.cpp" line="676"/>
        <source>PdfLaTeX</source>
        <translation>PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="383"/>
        <location filename="../configdialog.cpp" line="625"/>
        <location filename="../configdialog.cpp" line="688"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="399"/>
        <location filename="../configdialog.cpp" line="628"/>
        <location filename="../configdialog.cpp" line="691"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="600"/>
        <location filename="../configdialog.cpp" line="637"/>
        <location filename="../configdialog.cpp" line="700"/>
        <source>Pdf Viewer</source>
        <translation>Pdf megjelenítő</translation>
    </message>
    <message>
        <source>Built-in Viewer</source>
        <translation type="vanished">Beépített megjelenítő</translation>
    </message>
    <message>
        <source>External Viewer</source>
        <translation type="vanished">Külső megjelenítő</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="309"/>
        <location filename="../configdialog.cpp" line="640"/>
        <location filename="../configdialog.cpp" line="703"/>
        <source>metapost</source>
        <translation>metapost</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="278"/>
        <location filename="../configdialog.cpp" line="643"/>
        <location filename="../configdialog.cpp" line="706"/>
        <source>ghostscript</source>
        <translation>ghostscript</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="271"/>
        <location filename="../configdialog.cpp" line="646"/>
        <location filename="../configdialog.cpp" line="709"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="319"/>
        <location filename="../configdialog.cpp" line="649"/>
        <location filename="../configdialog.cpp" line="712"/>
        <source>Latexmk</source>
        <translation>Latexmk</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="629"/>
        <source>Embed</source>
        <translation>Beágyazott</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="446"/>
        <location filename="../configdialog.cpp" line="652"/>
        <location filename="../configdialog.cpp" line="715"/>
        <source>R Sweave</source>
        <translation>R Sweave</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="247"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &amp;quot;--output-directory=build&amp;quot; option will be automatically added to the (pdf)latex command while the compilation.&lt;/p&gt;&lt;p&gt;For the others commands like dvips, ps2pdf, bibtex,... you will have to manually replaced &amp;quot;%&amp;quot; by &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;A &amp;quot;--output-directory=build&amp;quot; kapcsoló automatikusan hozzá lesz adva a (pdf)latex parancshoz a fordítás során.&lt;/p&gt;&lt;p&gt;Más parancsoknál, mint pl. dvips, ps2pdf, bibtex,... kézzel kell lecserélnie a &amp;quot;%&amp;quot;-t &amp;quot;build/%&amp;quot; ra.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="250"/>
        <source>Use a &quot;build&quot; subdirectory for output files</source>
        <translation>Használjon egy &quot;build&quot;alkönyvtárat a kimeneti fájlokhoz</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="148"/>
        <location filename="../configdialog.cpp" line="655"/>
        <location filename="../configdialog.cpp" line="718"/>
        <source>XeLaTeX</source>
        <translation>XeLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="285"/>
        <source>Add to PATH</source>
        <translation>Könyvtár hozzáadása a rendszer PATH-hoz</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="704"/>
        <source>lp options for the printer</source>
        <translation>lp beállítások a nyomtatóhoz</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="896"/>
        <source>Quick Build Command</source>
        <translation>Gyorsfordítás parancs</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="918"/>
        <source>LaTeX + dvips + View PS</source>
        <translation>LaTeX + dvips + PS megtekintése</translation>
    </message>
    <message>
        <source>LaTeX + View DVI</source>
        <translation type="vanished">LaTeX + DVI megtekintése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="904"/>
        <source>PdfLaTeX + View PDF</source>
        <translation>PdfLaTeX + PDF megtekintése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="946"/>
        <source>LaTeX + dvipdfm + View PDF</source>
        <translation>LaTeX + dvipdfm + PDF megtekintése</translation>
    </message>
    <message>
        <source>LaTeX + dvips + ps2pdf + View PDF</source>
        <translation type="vanished">LaTeX + dvips + ps2pdf + PDF megtekintése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="183"/>
        <location filename="../configdialog.cpp" line="658"/>
        <location filename="../configdialog.cpp" line="721"/>
        <source>LuaLaTeX</source>
        <translation>LuaLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="356"/>
        <source>Bib(la)tex</source>
        <translation>Bib(la)tex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="559"/>
        <source>Launch the &quot;Clean&quot; tool when exiting Texmaker</source>
        <translation>A &quot;tisztító&quot; eszköz futtatása kilépéskor</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="925"/>
        <source>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + View Pdf</source>
        <translation>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + PDF megtekintése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="953"/>
        <source>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + View Pdf</source>
        <translation>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + PDF megtekintése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="960"/>
        <source>Sweave + PdfLaTeX + View Pdf</source>
        <translation>Sweave + PdfLaTeX + PDF megtekintése</translation>
    </message>
    <message>
        <source>LaTeX + Asymptote + LaTeX + dvips + View PS</source>
        <translation type="vanished">LaTeX + Asymptote + LaTeX + dvips + PS megtekintése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="974"/>
        <source>PdfLaTeX + Asymptote + PdfLaTeX + View Pdf</source>
        <translation>PdfLaTeX + Asymptote + PdfLaTeX + PDF megtekintése</translation>
    </message>
    <message>
        <source>LatexMk + View PDF</source>
        <translation type="vanished">LatexMk + PDF megtekintése</translation>
    </message>
    <message>
        <source>XeLaTeX + View PDF</source>
        <translation type="vanished">XeLaTeX + PDF megtekintése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="995"/>
        <source>LuaLaTeX + View PDF</source>
        <translation>LuaLaTeX + PDF megtekintése</translation>
    </message>
    <message>
        <source>User : (% : filename without extension)</source>
        <translation type="vanished">Egyéni: (% : fájlnév kiterjesztés nélkül)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1030"/>
        <location filename="../configdialog.ui" line="1069"/>
        <source>wizard</source>
        <translation>varázsló</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1043"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(a parancsokat &apos;|&apos; jellel kell elválasztani)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1059"/>
        <source>For .asy files</source>
        <translation>.asy fájlokhoz</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1107"/>
        <source>Don&apos;t launch a new instance of the viewer if the dvi/ps/pdf file is already opened</source>
        <translation>Ne futassa a megjelenítő új példányát ha a dvi/ps/pdf fájl már meg van nyitva</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1140"/>
        <location filename="../configdialog.cpp" line="187"/>
        <location filename="../configdialog.cpp" line="200"/>
        <source>Editor</source>
        <translation>Szerkesztő</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1269"/>
        <source>Editor Font Family</source>
        <translation>Szerkesztő betűtípusa</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1324"/>
        <source>Word Wrap</source>
        <translation>Sortörés</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1283"/>
        <source>Editor Font Size</source>
        <translation>Szerkesztő betűmérete</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1331"/>
        <source>Completion</source>
        <translation>Kiegészítés</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1300"/>
        <source>Editor Font Encoding</source>
        <translation>Szerkesztő karakterkódolása</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1338"/>
        <source>Show Line Numbers</source>
        <translation>Sorok számának mutatása</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1183"/>
        <source>Item</source>
        <translation>Elem</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1188"/>
        <source>Color</source>
        <translation>Szín</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1160"/>
        <source>Colors</source>
        <translation>Színek</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1146"/>
        <source>Default Theme</source>
        <translation>Alapértelmezett téma</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1209"/>
        <source>Dark theme</source>
        <translation>Sötét téma</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1310"/>
        <source>Check for external changes</source>
        <translation>Külső változások ellenőrzése</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1317"/>
        <source>Backup documents every 10 min</source>
        <translation>Dokumentumok biztonsági mentése 10 percenként</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1362"/>
        <source>Spelling dictionary</source>
        <translation>Helyesírás szótára</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1396"/>
        <source>Inline Spell Checking</source>
        <translation>Helyesírás-ellenőrzés gépelés közben</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1399"/>
        <source>Inline</source>
        <translation>Gépelés közben</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1232"/>
        <source>Tab width (num of spaces)</source>
        <translation>Tabulátor szélessége (szóközök száma)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="616"/>
        <source>Built-in &amp;Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="649"/>
        <source>E&amp;xternal Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="932"/>
        <source>LaTeX + &amp;View DVI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="939"/>
        <source>LaTeX + dvips + ps&amp;2pdf + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="967"/>
        <source>LaTeX + As&amp;ymptote + LaTeX + dvips + View PS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="981"/>
        <source>Latex&amp;Mk + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="988"/>
        <source>&amp;XeLaTeX + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1011"/>
        <source>User : (% : filename &amp;without extension)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1153"/>
        <source>Replace tab with spaces</source>
        <translation>Tabulátor cseréje szóközökre</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1444"/>
        <source>Svn</source>
        <translation>Svn</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1450"/>
        <source>Detect uncommitted lines at the opening</source>
        <translation>Detect uncommitted lines at the opening</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1457"/>
        <source>Path to the svn command</source>
        <translation>Útvonal az svn parancshoz</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1485"/>
        <location filename="../configdialog.cpp" line="193"/>
        <location filename="../configdialog.cpp" line="201"/>
        <source>Shortcuts</source>
        <translation>Gyorsbillentyűk</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1521"/>
        <source>Toggle focus editor/pdf viewer</source>
        <translation>Váltás a szerkesztő és a PDF megjelenítő között</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1528"/>
        <source>PushButton</source>
        <translation>Nyomógomb</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1495"/>
        <source>Action</source>
        <translation>Művelet</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1500"/>
        <source>Shortcut</source>
        <translation>Gyorsbillentyű</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="53"/>
        <source>Get more dictionaries at: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="174"/>
        <location filename="../configdialog.cpp" line="197"/>
        <location filename="../configdialog.cpp" line="198"/>
        <source>Commands</source>
        <translation>Parancsok</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="181"/>
        <location filename="../configdialog.cpp" line="199"/>
        <source>Quick Build</source>
        <translation>Gyorsfordítás</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="245"/>
        <source>Browse dictionary</source>
        <translation>Szótár böngészése</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>svn command directory</source>
        <translation>svn parancs könyvtára</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <location filename="../configdialog.cpp" line="391"/>
        <location filename="../configdialog.cpp" line="402"/>
        <location filename="../configdialog.cpp" line="414"/>
        <location filename="../configdialog.cpp" line="425"/>
        <location filename="../configdialog.cpp" line="436"/>
        <location filename="../configdialog.cpp" line="447"/>
        <location filename="../configdialog.cpp" line="458"/>
        <location filename="../configdialog.cpp" line="469"/>
        <location filename="../configdialog.cpp" line="480"/>
        <location filename="../configdialog.cpp" line="491"/>
        <location filename="../configdialog.cpp" line="502"/>
        <location filename="../configdialog.cpp" line="513"/>
        <location filename="../configdialog.cpp" line="524"/>
        <location filename="../configdialog.cpp" line="535"/>
        <location filename="../configdialog.cpp" line="546"/>
        <source>Browse program</source>
        <translation>Program böngészése</translation>
    </message>
</context>
<context>
    <name>DocumentView</name>
    <message>
        <source>Unlock %1</source>
        <translation type="vanished">%1 feloldása</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="vanished">Jelszó:</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="513"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation>&apos;%1&apos; nyomtatása...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1052"/>
        <source>Loading pdf...</source>
        <translation>PDF betöltése...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1358"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1374"/>
        <source>Number of words in the document</source>
        <translation>Szava száma ebben a dokumentumban</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1384"/>
        <source>Save As</source>
        <translation>Mentés másként</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1419"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1473"/>
        <source>Number of words in the page</source>
        <translation>Szavak száma az oldalon</translation>
    </message>
</context>
<context>
    <name>EncodingDialog</name>
    <message>
        <location filename="../encodingdialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="20"/>
        <source>It seems that this file cannot be correctly decoded
with the default encoding setting</source>
        <translation>Úgy tűnik, ezt a fájlt nem lehet helyesen dekódolni
az alapértelmezett kódolási beállítással</translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="31"/>
        <source>Use this encoding :</source>
        <translation>Használja ezt a kódolást:</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../exportdialog.ui" line="14"/>
        <source>Export via TeX4ht</source>
        <translation>Exportálás TeX4ht-vel</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="22"/>
        <source>Path to htlatex</source>
        <translation>Útvonal a htlatex-hez</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="45"/>
        <source>Mode</source>
        <translation>Mód</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="51"/>
        <source>export to Html</source>
        <translation>exportálás html-ként</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="61"/>
        <source>export to Html (new page for each section)</source>
        <translation>Exportálás html-ként (minden szakasz új oldalon)</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="68"/>
        <source>export to OpenDocumentFormat</source>
        <translation>Exportálás OpenDocumentFormat-ként</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="75"/>
        <source>export to MathML</source>
        <translation>Exportálás MathML-ként</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="82"/>
        <source>User</source>
        <translation>Felhasználó</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="91"/>
        <source>Options</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="125"/>
        <source>Run</source>
        <translation>Futtatás</translation>
    </message>
    <message>
        <location filename="../exportdialog.cpp" line="49"/>
        <source>Browse program</source>
        <translation>Program böngészése</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <location filename="../filechooser.ui" line="31"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location filename="../filechooser.cpp" line="44"/>
        <source>Select a File</source>
        <translation>Válasszon egy fáljt</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <location filename="../findwidget.ui" line="23"/>
        <source>Form</source>
        <translation>Formula</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="47"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="57"/>
        <source>Forward</source>
        <translation>Előre</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="67"/>
        <source>Backward</source>
        <translation>Visszafelé</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="92"/>
        <source>Whole words only</source>
        <translation>Csak teljes szavak</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="99"/>
        <source>Case sensitive</source>
        <translation>Kis- és nagybetű</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="106"/>
        <source>Start at Beginning</source>
        <translation>Kezdés az elejétől</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="163"/>
        <source>Regular Expression</source>
        <translation>Reguláris kifejezés</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="170"/>
        <location filename="../findwidget.cpp" line="158"/>
        <source>Text must be selected before checking this option.</source>
        <translation>A beállítás használata előtt ki kell jelölni egy szövegrészt.</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="173"/>
        <source>In selection only</source>
        <translation>Csak a kijelölésben</translation>
    </message>
    <message>
        <source>RegularExpression</source>
        <translation type="obsolete">Reguláris kifejezés</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Bezárás</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Invalid regular expression.</source>
        <translation>Érvénytelen reguláris kifejezés.</translation>
    </message>
</context>
<context>
    <name>GotoLineWidget</name>
    <message>
        <location filename="../gotolinewidget.ui" line="20"/>
        <source>Form</source>
        <translation>Formula</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="59"/>
        <source>Line</source>
        <translation>Sor</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="37"/>
        <source>Goto</source>
        <translation>Ugrás</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Bezárás</translation>
    </message>
</context>
<context>
    <name>GraphicFileChooser</name>
    <message>
        <location filename="../graphicfilechooser.ui" line="36"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="95"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="221"/>
        <source>Use &quot;figure&quot; environment</source>
        <translation>&quot;figure&quot; környezet használata</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="152"/>
        <source>Caption</source>
        <translation>Felirat</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="176"/>
        <source>Above</source>
        <translation>Felül</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="181"/>
        <source>Below</source>
        <translation>Alul</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="195"/>
        <source>Placement</source>
        <translation>Elhelyezés</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="208"/>
        <source>hbtp</source>
        <translation>hbtp</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="233"/>
        <source>Center</source>
        <translation>Középre</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.cpp" line="49"/>
        <source>Select a File</source>
        <translation>Válasszon egy fájlt</translation>
    </message>
</context>
<context>
    <name>KeySequenceDialog</name>
    <message>
        <location filename="../keysequencedialog.ui" line="14"/>
        <source>Shortcut</source>
        <translation>Gyorsbillentyű</translation>
    </message>
    <message>
        <location filename="../keysequencedialog.ui" line="37"/>
        <location filename="../keysequencedialog.cpp" line="43"/>
        <source>Clear</source>
        <translation>Törlés</translation>
    </message>
</context>
<context>
    <name>LatexEditor</name>
    <message>
        <location filename="../latexeditor.cpp" line="914"/>
        <source>Undo</source>
        <translation>Visszavonás</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="917"/>
        <source>Redo</source>
        <translation>Újra</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="921"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="924"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="927"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="932"/>
        <source>Select All</source>
        <translation>Mind kijelölése</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="938"/>
        <source>Check Spelling Word</source>
        <translation>Szó helyesírás-ellenőrzése</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="943"/>
        <source>Check Spelling Selection</source>
        <translation>Kijelölés helyesírás-ellenőrzése</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="946"/>
        <source>Check Spelling Document</source>
        <translation>Dokumentum helyesírás-ellenőrzése</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="953"/>
        <source>Jump to the end of this block</source>
        <translation>Ugrás a blokk végére</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="959"/>
        <source>Jump to pdf</source>
        <translation>Ugrás a pdf-hez</translation>
    </message>
</context>
<context>
    <name>LetterDialog</name>
    <message>
        <location filename="../letterdialog.ui" line="40"/>
        <source>Typeface Size</source>
        <translation>Betűméret</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="53"/>
        <source>Encoding</source>
        <translation>Kódolás</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="60"/>
        <source>AMS Packages</source>
        <translation>AMS csomagok</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="79"/>
        <source>Paper Size</source>
        <translation>Papírméret</translation>
    </message>
    <message>
        <location filename="../letterdialog.cpp" line="56"/>
        <source>Quick Letter</source>
        <translation>Levél varázsló</translation>
    </message>
</context>
<context>
    <name>LightFindWidget</name>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Invalid regular expression.</source>
        <translation>Érvénytelen reguláris kifejezés.</translation>
    </message>
</context>
<context>
    <name>LightLatexEditor</name>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>You do not have read permission to this file.</source>
        <translation>Nincs olvasási jogosultsága a fájlhoz.</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="900"/>
        <location filename="../lightlatexeditor.cpp" line="903"/>
        <location filename="../lightlatexeditor.cpp" line="906"/>
        <source>Click to jump to the bookmark</source>
        <translation>Ugrás a könyvjelzőhöz</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="910"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="913"/>
        <source>Goto Line</source>
        <translation>Ugrás a sorhoz</translation>
    </message>
</context>
<context>
    <name>LightLineNumberWidget</name>
    <message>
        <location filename="../lightlinenumberwidget.cpp" line="181"/>
        <source>Click to add or remove a bookmark</source>
        <translation>Kattintson egy könyvjelző hozzáadásához vagy eltávolításához</translation>
    </message>
</context>
<context>
    <name>LineNumberWidget</name>
    <message>
        <location filename="../linenumberwidget.cpp" line="286"/>
        <source>Click to add or remove a bookmark</source>
        <translation>Kattintson egy könyvjelző hozzáadásához vagy eltávolításához</translation>
    </message>
</context>
<context>
    <name>PageItem</name>
    <message>
        <location filename="../pageitem.cpp" line="137"/>
        <location filename="../pageitem.cpp" line="139"/>
        <source>Click to jump to the line</source>
        <translation>Kattintson az adott sorba ugráshoz</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="146"/>
        <source>Number of words in the document</source>
        <translation>Szava száma ebben a dokumentumban</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="150"/>
        <source>Number of words in the page</source>
        <translation>Szavak száma az oldalon</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="154"/>
        <source>Convert page to png image</source>
        <translation>Oldal átalakítása png képpé</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="158"/>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="unfinished">Helyesírás ellenőrzés ezen az oldalon</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="162"/>
        <source>Open the file browser</source>
        <translation>Fájlböngésző megnyitása</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="547"/>
        <source>Copy text</source>
        <translation>Szöveg másolása</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="548"/>
        <source>Copy image</source>
        <translation>Kép másolása</translation>
    </message>
</context>
<context>
    <name>PaperDialog</name>
    <message>
        <source>Print</source>
        <translation type="obsolete">Nyomtatás</translation>
    </message>
    <message>
        <source>Paper Size</source>
        <translation type="obsolete">Papírméret</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="obsolete">Nyomtatandó tartomány</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="obsolete">Az összes nyomtatása</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="obsolete">Oldalak ettől</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">eddig</translation>
    </message>
</context>
<context>
    <name>PdfDocumentWidget</name>
    <message>
        <source>Click to jump to the line</source>
        <translation type="obsolete">Kattintson az adott sorba ugráshoz</translation>
    </message>
</context>
<context>
    <name>PdfViewer</name>
    <message>
        <location filename="../pdfviewer.cpp" line="110"/>
        <location filename="../pdfviewer.cpp" line="153"/>
        <location filename="../pdfviewer.cpp" line="1110"/>
        <source>Structure</source>
        <translation>Szerkezet</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="134"/>
        <location filename="../pdfviewer.cpp" line="1117"/>
        <source>Pages</source>
        <translation>Oldalak</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="184"/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="188"/>
        <source>Exit</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="190"/>
        <source>&amp;Edit</source>
        <translation>&amp;Szerkeszt</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="191"/>
        <location filename="../pdfviewer.cpp" line="288"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="201"/>
        <source>Previous</source>
        <translation>Előző</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="205"/>
        <source>Next</source>
        <translation>Következő</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="211"/>
        <source>&amp;View</source>
        <translation>&amp;Nézet</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="212"/>
        <source>Fit Width</source>
        <translation>Igazítás a szélességhez</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="215"/>
        <source>Fit Page</source>
        <translation>Igazítás az oldalhoz</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="218"/>
        <source>Zoom In</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="223"/>
        <source>Zoom Out</source>
        <translation>Kicsinyítés</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="228"/>
        <source>Continuous</source>
        <translation>Folyamatos</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="233"/>
        <source>Two pages</source>
        <translation>Kétoldalas</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="238"/>
        <source>Rotate left</source>
        <translation>Forgatás balra</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="242"/>
        <source>Rotate right</source>
        <translation>Forgatás jobbra</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="246"/>
        <source>Presentation...</source>
        <translation>Bemutató...</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="255"/>
        <source>Previous Position</source>
        <translation>Előző pozíció</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="260"/>
        <source>Next Position</source>
        <translation>Következő pozíció</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="299"/>
        <location filename="../pdfviewer.cpp" line="992"/>
        <source>Print</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="302"/>
        <source>External Viewer</source>
        <translation>Külső megjelenítő</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">Helyesírás ellenőrzés ezen az oldalon</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>File not found</source>
        <translation>A fájl nem található</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="441"/>
        <location filename="../pdfviewer.cpp" line="669"/>
        <source>Page</source>
        <translation>Oldal</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Mégse</translation>
    </message>
    <message>
        <source>Can&apos;t print : the ghostscript command (gswin32c.exe) was not found on your system.</source>
        <translation type="obsolete">Nem sikerült nyomtatni: a ghostscript parancs (gswin32c.exe) nem található a rendszerén.</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Mentés másként</translation>
    </message>
</context>
<context>
    <name>PdfViewerWidget</name>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="104"/>
        <source>Show/Hide Table of contents</source>
        <translation>Tartalomjegyzék mutatása/elrejtése</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="110"/>
        <source>Previous</source>
        <translation>Előző</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="114"/>
        <source>Next</source>
        <translation>Következő</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="120"/>
        <source>Fit Width</source>
        <translation>Igazítás a szélességhez</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="123"/>
        <source>Fit Page</source>
        <translation>Igazítás az oldalhoz</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="126"/>
        <source>Zoom In</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="131"/>
        <source>Zoom Out</source>
        <translation>Kicsinyítés</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="144"/>
        <source>Continuous</source>
        <translation>Folyamatos</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="149"/>
        <source>Two pages</source>
        <translation>Kétoldalas</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="154"/>
        <source>Rotate left</source>
        <translation>Forgatás balra</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="158"/>
        <source>Rotate right</source>
        <translation>Forgatás jobbra</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="162"/>
        <source>Presentation...</source>
        <translation>Bemutató...</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="195"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="206"/>
        <source>Previous Position</source>
        <translation>Előző pozíció</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="211"/>
        <source>Next Position</source>
        <translation>Következő pozíció</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="220"/>
        <location filename="../pdfviewerwidget.cpp" line="928"/>
        <source>Print</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="223"/>
        <source>External Viewer</source>
        <translation>Külső megjelenítő</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">Helyesírás ellenőrzés ezen az oldalon</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>File not found</source>
        <translation>A fájl nem található</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="obsolete">Oldal</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Mégse</translation>
    </message>
    <message>
        <source>Can&apos;t print : the ghostscript command (gswin32c.exe) was not found on your system.</source>
        <translation type="obsolete">Nem sikerült nyomtatni: a ghostscript parancs (gswin32c.exe) nem található a rendszerén.</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Mentés másként</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>unknown</source>
        <translation type="vanished">ismeretlen</translation>
    </message>
    <message>
        <source>Type 1</source>
        <translation type="vanished">Type 1</translation>
    </message>
    <message>
        <source>Type 1C</source>
        <translation type="vanished">Type 1C</translation>
    </message>
    <message>
        <source>Type 3</source>
        <translation type="vanished">Type 3</translation>
    </message>
    <message>
        <source>TrueType</source>
        <translation type="vanished">TrueType</translation>
    </message>
    <message>
        <source>CID Type 0</source>
        <translation type="vanished">CID Type 0</translation>
    </message>
    <message>
        <source>CID Type 0C</source>
        <translation type="vanished">CID Type 0C</translation>
    </message>
    <message>
        <source>CID TrueType</source>
        <translation type="vanished">CID TrueType</translation>
    </message>
    <message>
        <source>Type 1C (OpenType)</source>
        <translation type="vanished">Type 1C (OpenType)</translation>
    </message>
    <message>
        <source>TrueType (OpenType)</source>
        <translation type="vanished">TrueType (OpenType)</translation>
    </message>
    <message>
        <source>CID Type 0C (OpenType)</source>
        <translation type="vanished">CID Type 0C (OpenType)</translation>
    </message>
    <message>
        <source>CID TrueType (OpenType)</source>
        <translation type="vanished">CID TrueType (OpenType)</translation>
    </message>
    <message>
        <source>Bug: unexpected font type. Notify poppler mailing list!</source>
        <translation type="vanished">Hiba: ismeretlen font. Értesítse a poppler levelezőlistát!</translation>
    </message>
</context>
<context>
    <name>QuickBeamerDialog</name>
    <message>
        <location filename="../quickbeamerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Párbeszédablak</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="22"/>
        <source>AMS Packages</source>
        <translation>AMS csomagok</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="29"/>
        <source>Encoding</source>
        <translation>Kódolás</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="39"/>
        <source>Typeface Size</source>
        <translation>Betűméret</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="46"/>
        <source>babel Package</source>
        <translation>babel csomag</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="59"/>
        <source>graphicx Package</source>
        <translation>graphicx csomag</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="66"/>
        <source>Title</source>
        <translation>Cím</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="79"/>
        <source>Author</source>
        <translation>Szerző</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="86"/>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.cpp" line="23"/>
        <source>Quick Start</source>
        <translation>Gyorsindítás</translation>
    </message>
</context>
<context>
    <name>QuickDocumentDialog</name>
    <message>
        <location filename="../quickdocumentdialog.ui" line="157"/>
        <source>Author</source>
        <translation>Szerző</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="41"/>
        <location filename="../quickdocumentdialog.ui" line="70"/>
        <location filename="../quickdocumentdialog.ui" line="115"/>
        <location filename="../quickdocumentdialog.ui" line="144"/>
        <location filename="../quickdocumentdialog.ui" line="199"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="25"/>
        <source>Document Class</source>
        <translation>Dokumentumosztály</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="173"/>
        <source>Title</source>
        <translation>Cím</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="183"/>
        <source>babel Package</source>
        <translation>babel csomag</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="206"/>
        <source>geometry Package</source>
        <translation>geometry csomag</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="213"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation>left=2cm,right=2cm,top=2cm,bottom=2cm</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="220"/>
        <source>AMS Packages</source>
        <translation>AMS csomagok</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="237"/>
        <source>graphicx Package</source>
        <translation>graphicx csomag</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="244"/>
        <source>lmodern Package</source>
        <translation>lmodern csomag</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="251"/>
        <source>Kpfonts Package</source>
        <translation>Kpfonts csomag</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="258"/>
        <source>Fourier Package</source>
        <translation>Fourier csomag</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="83"/>
        <source>Typeface Size</source>
        <translation>Betűméret</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="99"/>
        <source>Paper Size</source>
        <translation>Papírméret</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="54"/>
        <source>Other Options</source>
        <translation>Egyéb beállítások</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="128"/>
        <source>Encoding</source>
        <translation>Kódolás</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="230"/>
        <source>makeidx Package</source>
        <translation>makeidx csomag</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.cpp" line="32"/>
        <source>Quick Start</source>
        <translation>Gyorsindítás</translation>
    </message>
</context>
<context>
    <name>QuickXelatexDialog</name>
    <message>
        <location filename="../quickxelatexdialog.ui" line="25"/>
        <location filename="../quickxelatexdialog.ui" line="83"/>
        <location filename="../quickxelatexdialog.ui" line="115"/>
        <location filename="../quickxelatexdialog.ui" line="170"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="38"/>
        <source>Other Options</source>
        <translation>Egyéb beállítások</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="51"/>
        <source>Document Class</source>
        <translation>Dokumentumosztály</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="70"/>
        <source>Paper Size</source>
        <translation>Papírméret</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="96"/>
        <source>Typeface Size</source>
        <translation>Betűméret</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="128"/>
        <source>Author</source>
        <translation>Szerző</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="147"/>
        <source>Title</source>
        <translation>Cím</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="154"/>
        <source>polyglossia Package</source>
        <translation>polyglossia csomag</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="177"/>
        <source>geometry Package</source>
        <translation>geometry csomag</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="184"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation>left=2cm,right=2cm,top=2cm,bottom=2cm</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="191"/>
        <source>AMS Packages</source>
        <translation>AMS csomagok</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="201"/>
        <source>graphicx Package</source>
        <translation>graphicx csomag</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.cpp" line="31"/>
        <source>Quick Start</source>
        <translation>Gyorsindítás</translation>
    </message>
</context>
<context>
    <name>RefDialog</name>
    <message>
        <location filename="../refdialog.ui" line="14"/>
        <location filename="../refdialog.ui" line="40"/>
        <source>Labels</source>
        <translation>Címkék</translation>
    </message>
</context>
<context>
    <name>ReplaceWidget</name>
    <message>
        <location filename="../replacewidget.ui" line="20"/>
        <source>Form</source>
        <translation>Formula</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="34"/>
        <location filename="../replacewidget.ui" line="60"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="70"/>
        <source>Forward</source>
        <translation>Előre</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="222"/>
        <location filename="../replacewidget.cpp" line="251"/>
        <source>Text must be selected before checking this option.</source>
        <translation>A beállítás használata előtt ki kell jelölni egy szövegrészt.</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="225"/>
        <source>In selection only</source>
        <translation>Csak a kijelölésben</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="130"/>
        <source>Backward</source>
        <translation>Visszafelé</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="97"/>
        <source>Replace</source>
        <translation>Csere</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="123"/>
        <source>Replace All</source>
        <translation>Mindet cseréli</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="158"/>
        <source>Whole words only</source>
        <translation>Csak teljes szavak</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="165"/>
        <source>Case sensitive</source>
        <translation>Kis- és nagybetűk</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="172"/>
        <source>Start at Beginning</source>
        <translation>Kezdés az elejétől</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="215"/>
        <source>Regular Expression</source>
        <translation>Reguláris kifejezés</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Bezárás</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Invalid regular expression.</source>
        <translation>Érvénytelen reguláris kifejezés.</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Replace this occurrence ? </source>
        <translation>Cseréli ezt az előfordulást?</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Yes</source>
        <translation>Igen</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>No</source>
        <translation>Nem</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../scandialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="20"/>
        <source>In directory :</source>
        <translation>Könyvtár:</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="57"/>
        <source>Text :</source>
        <translation>Szöveg:</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="67"/>
        <location filename="../scandialog.cpp" line="43"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="74"/>
        <source>Include subdirectories</source>
        <translation>Alkönyvtárakban is</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="81"/>
        <source>Abort</source>
        <translation>Megszakítás</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="100"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
</context>
<context>
    <name>SourceView</name>
    <message>
        <location filename="../sourceview.cpp" line="50"/>
        <source>Open</source>
        <translation>Megnyitás</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="61"/>
        <source>Check differences</source>
        <translation>Különbségek ellenőrzése</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="167"/>
        <source>Open File</source>
        <translation>Fájl megnyitása</translation>
    </message>
</context>
<context>
    <name>SpellerDialog</name>
    <message>
        <location filename="../spellerdialog.ui" line="13"/>
        <source>Check Spelling</source>
        <translation>Helyesírás-ellenőrzés</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="19"/>
        <source>Unknown word:</source>
        <translation>Ismeretlen szó:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="33"/>
        <source>Replace</source>
        <translation>Csere</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="43"/>
        <source>Replace with:</source>
        <translation>Csere ezzel:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="53"/>
        <source>Ignore</source>
        <translation>Mellőzés</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="73"/>
        <source>Always ignore</source>
        <translation>Mindig mellőz</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="80"/>
        <source>Suggested words :</source>
        <translation>Javasolt szavak:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="130"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="121"/>
        <source>Check spelling selection...</source>
        <translation>Kijelölés helyesírás-ellenőrzése...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="131"/>
        <source>Check spelling from cursor...</source>
        <translation>Helyesírás-ellenőrzés a kurzortól...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="152"/>
        <location filename="../spellerdialog.cpp" line="169"/>
        <location filename="../spellerdialog.cpp" line="193"/>
        <location filename="../spellerdialog.cpp" line="319"/>
        <source>No more misspelled words</source>
        <translation>Nincs több helytelenül írt szó</translation>
    </message>
</context>
<context>
    <name>StructDialog</name>
    <message>
        <location filename="../structdialog.ui" line="14"/>
        <source>Structure</source>
        <translation>Szerkezet</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="37"/>
        <source>Numeration</source>
        <translation>Számozás</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="53"/>
        <source>Title</source>
        <translation>Cím</translation>
    </message>
</context>
<context>
    <name>SymbolListWidget</name>
    <message>
        <location filename="../symbollistwidget.cpp" line="52"/>
        <source>Add to favorites</source>
        <translation>Hozzáadás a kedvencekhez</translation>
    </message>
    <message>
        <location filename="../symbollistwidget.cpp" line="53"/>
        <source>Remove from favorites</source>
        <translation>Eltávolítás a kedvencek közül</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <location filename="../tabdialog.ui" line="37"/>
        <source>Num of Columns</source>
        <translation>Oszlopok száma</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="78"/>
        <source>Columns</source>
        <translation>Oszlopok</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="86"/>
        <source>Column :</source>
        <translation>Oszlop:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="113"/>
        <source>Alignment :</source>
        <translation>Igazítás:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="127"/>
        <source>Left Border :</source>
        <translation>Bal szegély:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="167"/>
        <source>Apply to all columns</source>
        <translation>Alkalmazás minden oszlopra</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="194"/>
        <source>Right Border (last column) :</source>
        <translation>Jobb szegély (utolsó oszlop):</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="263"/>
        <source>Rows</source>
        <translation>Sorok</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="271"/>
        <source>Row :</source>
        <translation>Sor:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="296"/>
        <source>Top Border</source>
        <translation>Felső szegély</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="305"/>
        <source>Merge columns :</source>
        <translation>Oszlopok egyesítése:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="321"/>
        <source>-&gt;</source>
        <translation>-&gt;</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="364"/>
        <source>Apply to all rows</source>
        <translation>Alkalmazás minden sorra</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="389"/>
        <source>Bottom Border (last row)</source>
        <translation>Alsó szegély (utolsó sor)</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="396"/>
        <source>Add vertical margin for each row</source>
        <translation>Függőleges margó hozzáadása minden sorhoz</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="232"/>
        <source>Num of Rows</source>
        <translation>Sorok száma</translation>
    </message>
    <message>
        <location filename="../tabdialog.cpp" line="108"/>
        <source>Quick Tabular</source>
        <translation>Táblázat varázsló</translation>
    </message>
</context>
<context>
    <name>TabbingDialog</name>
    <message>
        <location filename="../tabbingdialog.ui" line="67"/>
        <source>Spacing</source>
        <translation>Térköz</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="80"/>
        <source>Num of Rows</source>
        <translation>Sorok száma</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="93"/>
        <source>Num of Columns</source>
        <translation>Oszlopok száma</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.cpp" line="27"/>
        <source>Quick Tabbing</source>
        <translation>Tabulátor varázsló</translation>
    </message>
</context>
<context>
    <name>TexdocDialog</name>
    <message>
        <location filename="../texdocdialog.ui" line="20"/>
        <source>Search documentation about :</source>
        <translation>Dokumentáció keresése erről:</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="59"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="116"/>
        <source>Texdoc command :</source>
        <translation>Texdoc parancs:</translation>
    </message>
    <message>
        <location filename="../texdocdialog.cpp" line="32"/>
        <source>Browse program</source>
        <translation>Program böngészése</translation>
    </message>
</context>
<context>
    <name>Texmaker</name>
    <message>
        <location filename="../texmaker.cpp" line="667"/>
        <location filename="../texmaker.cpp" line="837"/>
        <location filename="../texmaker.cpp" line="2658"/>
        <location filename="../texmaker.cpp" line="5806"/>
        <source>Structure</source>
        <translation>Szerkezet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="686"/>
        <location filename="../texmaker.cpp" line="5811"/>
        <source>Relation symbols</source>
        <translation>Relációjelek</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="696"/>
        <location filename="../texmaker.cpp" line="5816"/>
        <source>Arrow symbols</source>
        <translation>Nyilak</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="705"/>
        <location filename="../texmaker.cpp" line="5821"/>
        <source>Miscellaneous symbols</source>
        <translation>Egyéb jelek</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="714"/>
        <location filename="../texmaker.cpp" line="5826"/>
        <source>Delimiters</source>
        <translation>Határolók</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="723"/>
        <location filename="../texmaker.cpp" line="5831"/>
        <source>Greek letters</source>
        <translation>Görög betűk</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="732"/>
        <location filename="../texmaker.cpp" line="5836"/>
        <source>Most used symbols</source>
        <translation>Leggyakrabban használt szimbólumok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="741"/>
        <location filename="../texmaker.cpp" line="5841"/>
        <source>Favorites symbols</source>
        <translation>Kedvenc szimbólumok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="780"/>
        <location filename="../texmaker.cpp" line="813"/>
        <location filename="../texmaker.cpp" line="5846"/>
        <source>Pstricks Commands</source>
        <translation>Pstricks parancsok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="788"/>
        <location filename="../texmaker.cpp" line="816"/>
        <location filename="../texmaker.cpp" line="5856"/>
        <source>MetaPost Commands</source>
        <translation>MetaPost parancsok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="796"/>
        <location filename="../texmaker.cpp" line="819"/>
        <location filename="../texmaker.cpp" line="5861"/>
        <source>Tikz Commands</source>
        <translation>Tikz parancsok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="804"/>
        <location filename="../texmaker.cpp" line="822"/>
        <location filename="../texmaker.cpp" line="5866"/>
        <source>Asymptote Commands</source>
        <translation>Asymptote parancsok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2663"/>
        <source>Messages / Log File</source>
        <translation>Üzenetek/Naplófájl</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1215"/>
        <source>Toggle between the master document and the current document</source>
        <translation>Váltás a fődokumentum és a jelenlegi dokumentum között</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1103"/>
        <source>Bold</source>
        <translation>Félkövér</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1108"/>
        <source>Italic</source>
        <translation>Kurzív</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1118"/>
        <source>Left</source>
        <translation>Balra</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1123"/>
        <source>Center</source>
        <translation>Középre</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1128"/>
        <source>Right</source>
        <translation>Jobbra</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1134"/>
        <location filename="../texmaker.cpp" line="1174"/>
        <source>New line</source>
        <translation>Új sor</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1383"/>
        <location filename="../texmaker.cpp" line="11064"/>
        <source>Normal Mode</source>
        <translation>Normál mód</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1407"/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1408"/>
        <location filename="../texmaker.cpp" line="1409"/>
        <location filename="../texmaker.cpp" line="2914"/>
        <location filename="../texmaker.cpp" line="2915"/>
        <source>New</source>
        <translation>Új</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1414"/>
        <source>New by copying an existing file</source>
        <translation>Új, egy meglévő fájl másolásával </translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1418"/>
        <location filename="../texmaker.cpp" line="1419"/>
        <location filename="../texmaker.cpp" line="2919"/>
        <location filename="../texmaker.cpp" line="2920"/>
        <source>Open</source>
        <translation>Megnyitás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1424"/>
        <source>Open Recent</source>
        <translation>Legutóbbi megnyitása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1438"/>
        <source>Restore previous session</source>
        <translation>Előző munkamenet helyreállítása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1450"/>
        <location filename="../texmaker.cpp" line="1451"/>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="12260"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1458"/>
        <location filename="../texmaker.cpp" line="4020"/>
        <location filename="../texmaker.cpp" line="4093"/>
        <location filename="../texmaker.cpp" line="5732"/>
        <source>Save As</source>
        <translation>Mentés másként</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1462"/>
        <source>Save All</source>
        <translation>Összes mentése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1242"/>
        <location filename="../texmaker.cpp" line="1470"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1476"/>
        <source>Close All</source>
        <translation>Összes bezárása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1480"/>
        <source>Reload document from file</source>
        <translation>Dokumentum újratöltése fájlból</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1488"/>
        <source>Print</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1494"/>
        <location filename="../texmaker.cpp" line="1495"/>
        <source>Exit</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1501"/>
        <source>&amp;Edit</source>
        <translation>&amp;Szerkesztés</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1502"/>
        <location filename="../texmaker.cpp" line="1503"/>
        <source>Undo</source>
        <translation>Visszavonás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1508"/>
        <location filename="../texmaker.cpp" line="1509"/>
        <source>Redo</source>
        <translation>Ismétlés</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1515"/>
        <location filename="../texmaker.cpp" line="1516"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1521"/>
        <location filename="../texmaker.cpp" line="1522"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1527"/>
        <location filename="../texmaker.cpp" line="1528"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1533"/>
        <source>Select All</source>
        <translation>Mind kijelölése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1539"/>
        <source>Comment</source>
        <translation>Megjegyzésként megjelöl</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1545"/>
        <source>Uncomment</source>
        <translation>Megjegyzés törlése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1551"/>
        <source>Indent</source>
        <translation>Behúzás növelése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1557"/>
        <source>Unindent</source>
        <translation>Behúzás csökkentése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1663"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1669"/>
        <source>FindNext</source>
        <translation>Következő keresése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1680"/>
        <source>Replace</source>
        <translation>Csere</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1686"/>
        <source>Goto Line</source>
        <translation>Ugrás a sorhoz</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1693"/>
        <source>Check Spelling</source>
        <translation>Helyesírás-ellenőrzés</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1700"/>
        <source>Refresh Structure</source>
        <translation>Szerkezet frissítése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1706"/>
        <source>Refresh Bibliography</source>
        <translation>Irodalomjegyzék frissítése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1711"/>
        <source>&amp;Tools</source>
        <translation>&amp;Eszközök</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1712"/>
        <location filename="../texmaker.cpp" line="2969"/>
        <location filename="../texmaker.cpp" line="9757"/>
        <source>Quick Build</source>
        <translation>Gyorsfordítás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1723"/>
        <location filename="../texmaker.cpp" line="2995"/>
        <source>View Dvi</source>
        <translation>DVI megtekintése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1733"/>
        <location filename="../texmaker.cpp" line="2996"/>
        <source>View PS</source>
        <translation>PS megtekintése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1743"/>
        <location filename="../texmaker.cpp" line="2997"/>
        <source>View PDF</source>
        <translation>PDF megtekintése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1758"/>
        <location filename="../texmaker.cpp" line="3010"/>
        <source>View Log</source>
        <translation>Napló megtekintése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1433"/>
        <location filename="../texmaker.cpp" line="1799"/>
        <location filename="../texmaker.cpp" line="2547"/>
        <source>Clean</source>
        <translation>Takarítás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="764"/>
        <location filename="../texmaker.cpp" line="5872"/>
        <source>User</source>
        <translation>Felhasználó</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1437"/>
        <source>Session</source>
        <translation>Munkamenet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1443"/>
        <source>Save session</source>
        <translation>Munkamenet mentése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1446"/>
        <source>Load session</source>
        <translation>Munkamenet betöltése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1466"/>
        <source>Save A Copy</source>
        <translation>Másolat mentése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1484"/>
        <source>Reload all documents from file</source>
        <translation>Minden dokumentum újratöltése fáljból</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1564"/>
        <source>Fold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1568"/>
        <source>Unfold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1573"/>
        <source>&amp;Fold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1616"/>
        <source>&amp;Unfold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1675"/>
        <source>Find In Directory</source>
        <translation>Keresés könyvtárban</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1804"/>
        <source>Open Terminal</source>
        <translation>Terminál megnyitása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1808"/>
        <source>Export via TeX4ht</source>
        <translation>Exportálás TeX4ht-vel</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1813"/>
        <source>Convert to unicode</source>
        <translation>Kódolás unicode-ra</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1818"/>
        <location filename="../texmaker.cpp" line="3020"/>
        <source>Previous LaTeX Error</source>
        <translation>Előző LaTeX hiba</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1820"/>
        <location filename="../texmaker.cpp" line="3014"/>
        <source>Next LaTeX Error</source>
        <translation>Következő LaTeX hiba</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1823"/>
        <source>&amp;LaTeX</source>
        <translation>&amp;LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1857"/>
        <source>&amp;Sectioning</source>
        <translation>&amp;Szakaszolás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1887"/>
        <source>&amp;Environment</source>
        <translation>&amp;Környezet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1933"/>
        <source>&amp;List Environment</source>
        <translation>&amp;Listakörnyezet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1957"/>
        <source>Font St&amp;yles</source>
        <translation>&amp;Betűstílusok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1994"/>
        <source>&amp;Tabular Environment</source>
        <translation>&amp;Táblázatkörnyezet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2020"/>
        <source>S&amp;pacing</source>
        <translation>Té&amp;rköz</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2047"/>
        <source>International &amp;Accents</source>
        <translation>Nemzetközi é&amp;kezetek</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2089"/>
        <source>International &amp;Quotes</source>
        <translation>Nemzetközi i&amp;dézőjelek</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2138"/>
        <source>&amp;Math</source>
        <translation>&amp;Matematika</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2139"/>
        <source>Inline math mode $...$</source>
        <translation>Szövegközi matematikai mód $...$</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2144"/>
        <source>Display math mode \[...\]</source>
        <translation>Kiemelt matematikai mód \[...\]</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2149"/>
        <source>Numbered equations \begin{equation}</source>
        <translation>Számozott egyenletek \begin{equation}</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2202"/>
        <source>Math &amp;Functions</source>
        <translation>Matematikai &amp;funkciók</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2324"/>
        <source>Math Font St&amp;yles</source>
        <translation>Matematikai betűstí&amp;lusok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2358"/>
        <source>Math &amp;Accents</source>
        <translation>Matematikai é&amp;kezetek</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2400"/>
        <source>Math S&amp;paces</source>
        <translation>Matematikai té&amp;rközök</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2422"/>
        <source>&amp;Wizard</source>
        <translation>&amp;Varázsló</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2423"/>
        <source>Quick Start</source>
        <translation>Gyorsindítás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2426"/>
        <source>Quick Beamer Presentation</source>
        <translation>Gyors beamer-bemutató</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2429"/>
        <source>Quick Xelatex Document</source>
        <translation>Gyors Xelatex dokumentum</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2432"/>
        <source>Quick Letter</source>
        <translation>Levél varázsló</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2436"/>
        <source>Quick Tabular</source>
        <translation>Táblázat varázsló</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2439"/>
        <source>Quick Tabbing</source>
        <translation>Tabulátor varázsló</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2442"/>
        <source>Quick Array</source>
        <translation>Tömb varázsló</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2446"/>
        <source>&amp;Bibliography</source>
        <translation>&amp;Irodalomjegyzék</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2552"/>
        <source>&amp;User</source>
        <translation>Fel&amp;használó</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2553"/>
        <source>User &amp;Tags</source>
        <translation>Felhasználói &amp;sablonok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2595"/>
        <location filename="../texmaker.cpp" line="8156"/>
        <location filename="../texmaker.cpp" line="8212"/>
        <source>Edit User &amp;Tags</source>
        <translation>Felhasználói sa&amp;blonok szerkesztése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2598"/>
        <source>User &amp;Commands</source>
        <translation>Felhasználói &amp;parancsok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2620"/>
        <location filename="../texmaker.cpp" line="9717"/>
        <location filename="../texmaker.cpp" line="9753"/>
        <source>Edit User &amp;Commands</source>
        <translation>Felhasználói para&amp;ncsok szerkesztése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2624"/>
        <source>Customize Completion</source>
        <translation>Kiegészítés testreszabása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2629"/>
        <source>Run script</source>
        <translation>Szkript futtatása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2640"/>
        <source>Other script</source>
        <translation>Egyéb szkript</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2645"/>
        <source>&amp;View</source>
        <translation>&amp;Nézet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2749"/>
        <source>Manage Settings File</source>
        <translation>Beállításfájl kezelése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2796"/>
        <source>Check for Update</source>
        <translation>Frissítés keresése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4774"/>
        <source>Browse script</source>
        <translation>Szkript böngészése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8441"/>
        <source>A document must be saved with an extension (and without spaces or accents in the name) before being used by a command.</source>
        <translation>A parancs használata előtt a dokumentumot kiterjesztéssel együtt (ékezetek és szóközök nélkül) kell menteni.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Make a copy of the %1.pdf/ps document in the &quot;build&quot; subdirectory and delete all the others %1.* files?</source>
        <translation>Készüljön egy másolat a %1.pd/ps dokumentumról a &quot;build&quot;alkönyvtárba és törölve legyen az összes többi %1.* fájl?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete the output files generated by LaTeX ?</source>
        <translation>Törli a LaTeX által előállított kimeneti fájlokat?</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre,.nlg,.nlo,.nls)</source>
        <translation type="obsolete">Törli a LaTeX által előállított kimeneti fájlokat?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre,.nlg,.nlo,.nls)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1224"/>
        <location filename="../texmaker.cpp" line="2646"/>
        <source>Next Document</source>
        <translation>Következő dokumentum</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1220"/>
        <location filename="../texmaker.cpp" line="2651"/>
        <source>Previous Document</source>
        <translation>Előző dokumentum</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2701"/>
        <source>&amp;Options</source>
        <translation>&amp;Beállítások</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2702"/>
        <source>Configure Texmaker</source>
        <translation>A Texmaker beállítása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2707"/>
        <location filename="../texmaker.cpp" line="11055"/>
        <source>Define Current Document as &apos;Master Document&apos;</source>
        <translation>A jelenlegi dokumentum megadása fődokumentumként</translation>
    </message>
    <message>
        <source>Interface Appearance</source>
        <translation type="vanished">Felület megjelenése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2727"/>
        <source>Change Interface Font</source>
        <translation>Felület betűtípusának megváltoztatása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2732"/>
        <source>Interface Language</source>
        <translation>Felület nyelve</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2763"/>
        <source>&amp;Help</source>
        <translation>Sú&amp;gó</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2764"/>
        <location filename="../texmaker.cpp" line="2765"/>
        <source>LaTeX Reference</source>
        <translation>LaTeX segédlet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2768"/>
        <location filename="../texmaker.cpp" line="2769"/>
        <source>User Manual</source>
        <translation>Felhasználói kézikönyv</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2801"/>
        <source>About Texmaker</source>
        <translation>A Texmaker névjegye</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2991"/>
        <source>Run</source>
        <translation>Futtatás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3004"/>
        <source>View</source>
        <translation>Nézet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3026"/>
        <source>Stop Process</source>
        <translation>Folyamat leállítása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1261"/>
        <location filename="../texmaker.cpp" line="1264"/>
        <location filename="../texmaker.cpp" line="1267"/>
        <source>Click to jump to the bookmark</source>
        <translation>Ugrás a könyvjelzőhöz</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2669"/>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Pdf Viewer</source>
        <translation>PDF megjelenítő</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2686"/>
        <source>List of opened files</source>
        <translation>Megnyitott fájlok listája</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2693"/>
        <source>Full Screen</source>
        <translation>Teljes képernyő</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2751"/>
        <source>Settings File</source>
        <translation>Beállításfájl</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2753"/>
        <source>Reset Settings</source>
        <translation>Beállítások visszaállítása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2756"/>
        <source>Save a copy of the settings file</source>
        <translation>Beállítások mentése fájlba</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2759"/>
        <source>Replace the settings file by a new one</source>
        <translation>Beállításfájl cseréje</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="4753"/>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="8441"/>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10020"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <source>You do not have read permission to this file.</source>
        <translation>Nincs olvasási jogosultsága a fájlhoz.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3595"/>
        <location filename="../texmaker.cpp" line="3655"/>
        <location filename="../texmaker.cpp" line="12312"/>
        <source>Open File</source>
        <translation>Fájl megnyitása</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3751"/>
        <location filename="../texmaker.cpp" line="3900"/>
        <location filename="../texmaker.cpp" line="3959"/>
        <source>The document has been changed outside Texmaker.Do you want to reload it (and discard your changes) or save it (and overwrite the file)?</source>
        <translation>A dokumentum a Texmakeren kívül lett módosítva. Újra be kívánja tölteni (ezzel elveszítve a módosításokat) vagy menti (és felülírja a fájlt)? </translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <source>Reload the file</source>
        <translation>Fájl újratöltése</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>A fájl nem menthető. Ellenőrizze, hogy rendelkezik-e írási jogosultsággal.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4130"/>
        <location filename="../texmaker.cpp" line="4194"/>
        <location filename="../texmaker.cpp" line="4264"/>
        <location filename="../texmaker.cpp" line="4370"/>
        <source>The document contains unsaved work. Do you want to save it before closing?</source>
        <translation>A dokumentum nem mentett változtatásokat tartalmaz. Szeretné menteni kilépés előtt?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Save and Close</source>
        <translation>Mentés és bezárás</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Don&apos;t Save and Close</source>
        <translation>Mentés kihagyása és bezárás</translation>
    </message>
    <message>
        <source>The document contains unsaved work. Do you want to save it before exiting?</source>
        <translation type="obsolete">A dokumentum nincs elmentve. Menti a kilépés előtt?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4539"/>
        <source>The document contains unsaved work.You will lose changes by reloading the document.</source>
        <translation>A dokumentum nincs elmentve. Ha újratölti, elveszti változtatásait.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4753"/>
        <source>Error : Can&apos;t open the dictionary</source>
        <translation>Hiba: A szótárt nem lehet megnyitni</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5710"/>
        <source>Delete settings file?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>Beállításfájl törlése?
(A Texmaker bezárul, újra kell indítania)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Ok</source>
        <translation>Rendben</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5753"/>
        <source>Replace settings file by a new one?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>Beállításfájl cseréje?
(A Texmaker bezárul, újra kell indítania)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5778"/>
        <source>Opened Files</source>
        <translation>Megnyitott fájlok</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="6667"/>
        <source>Select an image File</source>
        <translation>Válasszon egy képfájlt</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4311"/>
        <location filename="../texmaker.cpp" line="6698"/>
        <location filename="../texmaker.cpp" line="6723"/>
        <source>Select a File</source>
        <translation>Válasszon egy fájlt</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <source>Can&apos;t detect the file name</source>
        <translation>A fájlnév ismeretlen</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Could not start the command.</source>
        <translation>A parancs nem futtatható.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete Files</source>
        <translation>Fájlok törlése</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre)</source>
        <translation type="obsolete">Törli a LaTeX által előállított kimeneti fájlokat?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PdfLaTeX</source>
        <translation>PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvi Viewer</source>
        <translation>Dvi megjelenítő</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PS Viewer</source>
        <translation>PS megjelenítő</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Bibtex</source>
        <translation>Bibtex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>metapost</source>
        <translation>metapost</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ghostscript</source>
        <translation>ghostscript</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Latexmk</source>
        <translation>Latexmk</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>R Sweave</source>
        <translation>R Sweave</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>XeLaTex</source>
        <translation>XeLaTex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LuaLaTex</source>
        <translation>LuaLaTex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10020"/>
        <source>Log File not found !</source>
        <translation>A naplófájl nem található!</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10360"/>
        <location filename="../texmaker.cpp" line="10400"/>
        <source>Click to jump to the line</source>
        <translation>Kattintson az adott sorba ugráshoz</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10474"/>
        <location filename="../texmaker.cpp" line="10509"/>
        <source>No LaTeX errors detected !</source>
        <translation>Nem található LaTeX hiba!</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <source>File not found</source>
        <translation>A fájl nem található</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11079"/>
        <location filename="../texmaker.cpp" line="12629"/>
        <source>Normal Mode (current master document :</source>
        <translation>Normál mód (jelenlegi fődokumentum):</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11081"/>
        <location filename="../texmaker.cpp" line="12631"/>
        <source>Master Document :</source>
        <translation>Fődokumentum:</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11623"/>
        <source>The language setting will take effect after restarting the application.</source>
        <translation>Az új nyelvi beállítás az alkalmazás újraindítása után lép életbe.</translation>
    </message>
    <message>
        <source>The appearance setting will take effect after restarting the application.</source>
        <translation type="vanished">Az új megjelenés az alkalmazás újraindítása után lép életbe.</translation>
    </message>
</context>
<context>
    <name>UnicodeDialog</name>
    <message>
        <location filename="../unicodedialog.ui" line="14"/>
        <location filename="../unicodedialog.ui" line="98"/>
        <source>Convert to unicode</source>
        <translation>Kódolás unicode-ra</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="28"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="75"/>
        <source>Original Encoding</source>
        <translation>Eredeti kódolás</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="145"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="56"/>
        <source>Select a File</source>
        <translation>Válasszon egy fáljt</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>You do not have read permission to this file.</source>
        <translation>Nincs olvasási jogosultsága a fájlhoz.</translation>
    </message>
</context>
<context>
    <name>UnicodeView</name>
    <message>
        <location filename="../unicodeview.cpp" line="56"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="152"/>
        <source>Save As</source>
        <translation>Mentés másként</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>A fájl nem menthető. Ellenőrizze, hogy rendelkezik-e írási jogosultsággal.</translation>
    </message>
</context>
<context>
    <name>UserCompletionDialog</name>
    <message>
        <location filename="../usercompletiondialog.ui" line="14"/>
        <source>Completion</source>
        <translation>Kiegészítés</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="25"/>
        <source>Add</source>
        <translation>Hozzáadás</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="48"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="55"/>
        <source>( @ : placeholder )</source>
        <translation>(@: helykitöltő)</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="62"/>
        <source>( #bib# : bibliography item )</source>
        <translation>(#bib#: irodalomjegyzék eleme)</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="69"/>
        <source>( #label# : label item )</source>
        <translation>(#label#: címke eleme)</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="76"/>
        <source>Replace</source>
        <translation>Csere</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="85"/>
        <source>Items already supplied by Texmaker</source>
        <translation>A Texmaker által felajánlott elemek</translation>
    </message>
</context>
<context>
    <name>UserMenuDialog</name>
    <message>
        <location filename="../usermenudialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>Felhasználói sablonok szerkesztése</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="45"/>
        <source>Menu Item</source>
        <translation>Menüelem</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="55"/>
        <source>LaTeX Content</source>
        <translation>LaTeX tartalom</translation>
    </message>
</context>
<context>
    <name>UserQuickDialog</name>
    <message>
        <location filename="../userquickdialog.ui" line="14"/>
        <source>Quick Build Command</source>
        <translation>Gyorsfordítás parancs</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="42"/>
        <source>Add</source>
        <translation>Hozzáadás</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="70"/>
        <source>Ordered list of commands :</source>
        <translation>Parancsok rendezett listája:</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="80"/>
        <source>Up</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="87"/>
        <source>Down</source>
        <translation>Le</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="94"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
</context>
<context>
    <name>UserTagsListWidget</name>
    <message>
        <location filename="../usertagslistwidget.cpp" line="30"/>
        <source>Add tag</source>
        <translation>Sablon hozzáadása</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="32"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="34"/>
        <source>Modify</source>
        <translation>Módosítás</translation>
    </message>
</context>
<context>
    <name>UserToolDialog</name>
    <message>
        <location filename="../usertooldialog.ui" line="14"/>
        <source>Edit User Commands</source>
        <translation>Felhasználói parancs szerkesztése</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="51"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(a parancsokat &apos;|&apos; jellel kell elválasztani)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="70"/>
        <source>Menu Item</source>
        <translation>Menüelem</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="77"/>
        <source>Command (% : filename without extension)</source>
        <translation>Parancs (% : fájlnév kiterjesztés nélkül)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="103"/>
        <source>wizard</source>
        <translation>varázsló</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../versiondialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="20"/>
        <source>Check for available version</source>
        <translation>Elérhető verzió keresése</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="34"/>
        <source>Available version</source>
        <translation>Elérhető verzió</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="48"/>
        <source>Go to the download page</source>
        <translation>Ugrás a letöltéshez</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="68"/>
        <source>You&apos;re using the version</source>
        <translation>Ezt a verziót használja</translation>
    </message>
    <message>
        <location filename="../versiondialog.cpp" line="53"/>
        <location filename="../versiondialog.cpp" line="60"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
</context>
<context>
    <name>X11FontDialog</name>
    <message>
        <location filename="../x11fontdialog.ui" line="14"/>
        <source>Select a Font</source>
        <translation>Válasszon egy betűtípust</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="34"/>
        <source>Font Family</source>
        <translation>Betűtípus</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="44"/>
        <source>Font Size</source>
        <translation>Betűméret</translation>
    </message>
</context>
</TS>
