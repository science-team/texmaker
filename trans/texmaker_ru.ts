<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.ui" line="14"/>
        <source>About Texmaker</source>
        <translation>О программе Texmaker</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="76"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="86"/>
        <source>Authors</source>
        <translation>Авторы</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="96"/>
        <source>Thanks</source>
        <translation>Благодарности</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="106"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
</context>
<context>
    <name>AddOptionDialog</name>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../addoptiondialog.ui" line="14"/>
        <source>New</source>
        <translation>Создать</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
</context>
<context>
    <name>AddTagDialog</name>
    <message>
        <location filename="../addtagdialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>Редактировать метки пользователя</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="22"/>
        <source>Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="32"/>
        <source>LaTeX Content</source>
        <translation>Содержимое LaTeX</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="55"/>
        <source>Keyboard Trigger</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArrayDialog</name>
    <message>
        <location filename="../arraydialog.cpp" line="43"/>
        <source>Quick Array</source>
        <translation>Быстрый массив</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="45"/>
        <source>Num of Columns</source>
        <translation>Кол-во колонок</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="59"/>
        <source>Columns Alignment</source>
        <translation>Выравнивание в колонке</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="76"/>
        <source>Environment</source>
        <translation>Окружение</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="35"/>
        <source>Num of Rows</source>
        <translation>Кол-во столбцов</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../browser.cpp" line="78"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="79"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="81"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="89"/>
        <source>Index</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="107"/>
        <source>Find</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../configdialog.cpp" line="174"/>
        <location filename="../configdialog.cpp" line="197"/>
        <location filename="../configdialog.cpp" line="198"/>
        <source>Commands</source>
        <translation>Команды</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="181"/>
        <location filename="../configdialog.cpp" line="199"/>
        <source>Quick Build</source>
        <translation>Быстрая сборка</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1140"/>
        <location filename="../configdialog.cpp" line="187"/>
        <location filename="../configdialog.cpp" line="200"/>
        <source>Editor</source>
        <translation>Редактор</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="53"/>
        <source>Get more dictionaries at: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="245"/>
        <source>Browse dictionary</source>
        <translation>Просмотр словаря</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>svn command directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <location filename="../configdialog.cpp" line="391"/>
        <location filename="../configdialog.cpp" line="402"/>
        <location filename="../configdialog.cpp" line="414"/>
        <location filename="../configdialog.cpp" line="425"/>
        <location filename="../configdialog.cpp" line="436"/>
        <location filename="../configdialog.cpp" line="447"/>
        <location filename="../configdialog.cpp" line="458"/>
        <location filename="../configdialog.cpp" line="469"/>
        <location filename="../configdialog.cpp" line="480"/>
        <location filename="../configdialog.cpp" line="491"/>
        <location filename="../configdialog.cpp" line="502"/>
        <location filename="../configdialog.cpp" line="513"/>
        <location filename="../configdialog.cpp" line="524"/>
        <location filename="../configdialog.cpp" line="535"/>
        <location filename="../configdialog.cpp" line="546"/>
        <source>Browse program</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="14"/>
        <source>Configure Texmaker</source>
        <translation>Настроить Texmaker</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="130"/>
        <source>Commands (% : filename without extension - @ : line number)</source>
        <translation>Команды (% : имя файла без расширения - @ : номер строки)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="755"/>
        <location filename="../configdialog.cpp" line="619"/>
        <location filename="../configdialog.cpp" line="682"/>
        <source>Dvi Viewer</source>
        <translation>Просмотр DVI</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="820"/>
        <location filename="../configdialog.cpp" line="622"/>
        <location filename="../configdialog.cpp" line="685"/>
        <source>PS Viewer</source>
        <translation>Просмотр PS</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="278"/>
        <location filename="../configdialog.cpp" line="643"/>
        <location filename="../configdialog.cpp" line="706"/>
        <source>ghostscript</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="631"/>
        <location filename="../configdialog.cpp" line="694"/>
        <source>Bibtex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="383"/>
        <location filename="../configdialog.cpp" line="625"/>
        <location filename="../configdialog.cpp" line="688"/>
        <source>Dvipdfm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="155"/>
        <location filename="../configdialog.cpp" line="613"/>
        <location filename="../configdialog.cpp" line="676"/>
        <source>PdfLaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="366"/>
        <location filename="../configdialog.cpp" line="634"/>
        <location filename="../configdialog.cpp" line="697"/>
        <source>Makeindex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="399"/>
        <location filename="../configdialog.cpp" line="628"/>
        <location filename="../configdialog.cpp" line="691"/>
        <source>ps2pdf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="600"/>
        <location filename="../configdialog.cpp" line="637"/>
        <location filename="../configdialog.cpp" line="700"/>
        <source>Pdf Viewer</source>
        <translation>Просмотр PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="309"/>
        <location filename="../configdialog.cpp" line="640"/>
        <location filename="../configdialog.cpp" line="703"/>
        <source>metapost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="141"/>
        <location filename="../configdialog.cpp" line="610"/>
        <location filename="../configdialog.cpp" line="673"/>
        <source>LaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="376"/>
        <location filename="../configdialog.cpp" line="616"/>
        <location filename="../configdialog.cpp" line="679"/>
        <source>dvips</source>
        <translation></translation>
    </message>
    <message>
        <source>Built-in Viewer</source>
        <translation type="vanished">Встроенная программа</translation>
    </message>
    <message>
        <source>External Viewer</source>
        <translation type="vanished">Внешняя программа</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="271"/>
        <location filename="../configdialog.cpp" line="646"/>
        <location filename="../configdialog.cpp" line="709"/>
        <source>Asymptote</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="319"/>
        <location filename="../configdialog.cpp" line="649"/>
        <location filename="../configdialog.cpp" line="712"/>
        <source>Latexmk</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="629"/>
        <source>Embed</source>
        <translation>В том же окне</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="446"/>
        <location filename="../configdialog.cpp" line="652"/>
        <location filename="../configdialog.cpp" line="715"/>
        <source>R Sweave</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="247"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &amp;quot;--output-directory=build&amp;quot; option will be automatically added to the (pdf)latex command while the compilation.&lt;/p&gt;&lt;p&gt;For the others commands like dvips, ps2pdf, bibtex,... you will have to manually replaced &amp;quot;%&amp;quot; by &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;При компиляции к команде (pdf)latex будет автоматически добавлена опция &amp;quot;--output-directory=build&amp;quot;.&lt;/p&gt;&lt;p&gt;Для других команд, таких как dvips, ps2pdf, bibtex,..., вам нужно будет вручную заменить &amp;quot;%&amp;quot; на &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="250"/>
        <source>Use a &quot;build&quot; subdirectory for output files</source>
        <translation>Использовать каталог &quot;build&quot; для выходных файлов</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="148"/>
        <location filename="../configdialog.cpp" line="655"/>
        <location filename="../configdialog.cpp" line="718"/>
        <source>XeLaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="285"/>
        <source>Add to PATH</source>
        <translation>Добавить в PATH</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="704"/>
        <source>lp options for the printer</source>
        <translation>Настройки для принтера (lp)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="896"/>
        <source>Quick Build Command</source>
        <translation>Команда быстрой сборки</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="918"/>
        <source>LaTeX + dvips + View PS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="904"/>
        <source>PdfLaTeX + View PDF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="946"/>
        <source>LaTeX + dvipdfm + View PDF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="183"/>
        <location filename="../configdialog.cpp" line="658"/>
        <location filename="../configdialog.cpp" line="721"/>
        <source>LuaLaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="356"/>
        <source>Bib(la)tex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="559"/>
        <source>Launch the &quot;Clean&quot; tool when exiting Texmaker</source>
        <translation>Запустить &quot;Clean&quot; при выходе из Texmaker</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="925"/>
        <source>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + View Pdf</source>
        <translation>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + Просмотр Pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="953"/>
        <source>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + View Pdf</source>
        <translation>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + Просмотр Pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="960"/>
        <source>Sweave + PdfLaTeX + View Pdf</source>
        <translation>Sweave + PdfLaTeX + Просмотр Pdf</translation>
    </message>
    <message>
        <source>LaTeX + Asymptote + LaTeX + dvips + View PS</source>
        <oldsource>LaTeX + Asymptote + LaTeX + View PS</oldsource>
        <translation type="vanished">LaTeX + Asymptote + LaTeX + dvips + Просмотр PS</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="974"/>
        <source>PdfLaTeX + Asymptote + PdfLaTeX + View Pdf</source>
        <translation>PdfLaTeX + Asymptote + PdfLaTeX + Просмотр Pdf</translation>
    </message>
    <message>
        <source>LatexMk + View PDF</source>
        <translation type="vanished">LatexMk + Просмотр PDF</translation>
    </message>
    <message>
        <source>XeLaTeX + View PDF</source>
        <translation type="vanished">XeLaTeX + Просмотр PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="995"/>
        <source>LuaLaTeX + View PDF</source>
        <translation>LuaLaTeX + Просмотр PDF</translation>
    </message>
    <message>
        <source>User : (% : filename without extension)</source>
        <translation type="vanished">Пользовательская : (% : имя файла без расширения)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1030"/>
        <location filename="../configdialog.ui" line="1069"/>
        <source>wizard</source>
        <translation>мастер</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1043"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(разделяйте команды символом &apos;|&apos;)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1059"/>
        <source>For .asy files</source>
        <translation>Для файлов .asy</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1107"/>
        <source>Don&apos;t launch a new instance of the viewer if the dvi/ps/pdf file is already opened</source>
        <translation>Не запускать новую программу просмотра, если файл dvi/ps/pdf уже открыт</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1338"/>
        <source>Show Line Numbers</source>
        <translation>Показывать номера строк</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1232"/>
        <source>Tab width (num of spaces)</source>
        <translation>Отступ табуляции (число пробелов)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1153"/>
        <source>Replace tab with spaces</source>
        <translation>Заменять табуляцию пробелами</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1183"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1188"/>
        <source>Color</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1160"/>
        <source>Colors</source>
        <translation>Цвета</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1146"/>
        <source>Default Theme</source>
        <translation>Стиль по умолчанию</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1209"/>
        <source>Dark theme</source>
        <translation>Темный стиль</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1324"/>
        <source>Word Wrap</source>
        <translation>Перенос по словам</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1269"/>
        <source>Editor Font Family</source>
        <translation>Стиль шрифта</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1283"/>
        <source>Editor Font Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1331"/>
        <source>Completion</source>
        <translation>Автозавершение</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1300"/>
        <source>Editor Font Encoding</source>
        <translation>Кодировка</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="616"/>
        <source>Built-in &amp;Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="649"/>
        <source>E&amp;xternal Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="932"/>
        <source>LaTeX + &amp;View DVI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="939"/>
        <source>LaTeX + dvips + ps&amp;2pdf + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="967"/>
        <source>LaTeX + As&amp;ymptote + LaTeX + dvips + View PS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="981"/>
        <source>Latex&amp;Mk + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="988"/>
        <source>&amp;XeLaTeX + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1011"/>
        <source>User : (% : filename &amp;without extension)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1310"/>
        <source>Check for external changes</source>
        <translation>Проверять внешние изменения</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1317"/>
        <source>Backup documents every 10 min</source>
        <translation>Сохранять документ каждые 10 мин</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1362"/>
        <source>Spelling dictionary</source>
        <translation>Словарь</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1396"/>
        <source>Inline Spell Checking</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1399"/>
        <source>Inline</source>
        <translation>В редакторе</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1485"/>
        <location filename="../configdialog.cpp" line="193"/>
        <location filename="../configdialog.cpp" line="201"/>
        <source>Shortcuts</source>
        <translation>Быстрые клавиши</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1521"/>
        <source>Toggle focus editor/pdf viewer</source>
        <translation>Переключение фокуса между редактором и программой просмотра</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1528"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1495"/>
        <source>Action</source>
        <translation>Действие</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1444"/>
        <source>Svn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1450"/>
        <source>Detect uncommitted lines at the opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1457"/>
        <source>Path to the svn command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1500"/>
        <source>Shortcut</source>
        <translation>Быстрая клавиша</translation>
    </message>
</context>
<context>
    <name>DocumentView</name>
    <message>
        <source>Unlock %1</source>
        <translation type="vanished">Разблокировать %1</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="vanished">Пароль:</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="513"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation>Печать &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1052"/>
        <source>Loading pdf...</source>
        <translation>Загрузка pdf...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1358"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1374"/>
        <source>Number of words in the document</source>
        <translation>Количество слов в документе</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1384"/>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1419"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1473"/>
        <source>Number of words in the page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncodingDialog</name>
    <message>
        <location filename="../encodingdialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="20"/>
        <source>It seems that this file cannot be correctly decoded
with the default encoding setting</source>
        <translation>Не удалось раскодировать файл
с использованием кодировки по умолчанию</translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="31"/>
        <source>Use this encoding :</source>
        <translation>Использовать кодировку:</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../exportdialog.ui" line="14"/>
        <source>Export via TeX4ht</source>
        <translation>Экспорт через TeX4ht</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="22"/>
        <source>Path to htlatex</source>
        <translation>Путь к htlatex</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="45"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="51"/>
        <source>export to Html</source>
        <translation>экспорт в Html</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="61"/>
        <source>export to Html (new page for each section)</source>
        <translation>экспорт в Html (каждый раздел на новой странице)</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="68"/>
        <source>export to OpenDocumentFormat</source>
        <translation>экспорт в OpenDocumentFormat</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="75"/>
        <source>export to MathML</source>
        <translation>экспорт в MathML</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="82"/>
        <source>User</source>
        <translation>пользовательский</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="91"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="125"/>
        <source>Run</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="../exportdialog.cpp" line="49"/>
        <source>Browse program</source>
        <translation>Выбор программы</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../filechooser.cpp" line="44"/>
        <source>Select a File</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <location filename="../filechooser.ui" line="31"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <source>Find Text</source>
        <translation type="obsolete">Найти текст</translation>
    </message>
    <message>
        <source>Find</source>
        <translation type="obsolete">Искать</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="obsolete">Направление</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Вперед</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="obsolete">Назад</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Настройки</translation>
    </message>
    <message>
        <source>Whole words only</source>
        <translation type="obsolete">Слова целиком</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation type="obsolete">Различать регистр</translation>
    </message>
    <message>
        <source>Start at Beginning</source>
        <translation type="obsolete">Начать с начала</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <location filename="../findwidget.ui" line="23"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="47"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="57"/>
        <source>Forward</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="67"/>
        <source>Backward</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="92"/>
        <source>Whole words only</source>
        <translation>Слова целиком</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="99"/>
        <source>Case sensitive</source>
        <translation>Различать регистр</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="106"/>
        <source>Start at Beginning</source>
        <translation>Начать с начала</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="163"/>
        <source>Regular Expression</source>
        <translation>Регулярное выражение</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="170"/>
        <location filename="../findwidget.cpp" line="158"/>
        <source>Text must be selected before checking this option.</source>
        <translation>Для использования этой опции выделите текст.</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="173"/>
        <source>In selection only</source>
        <translation>В выбранном</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Invalid regular expression.</source>
        <translation>Неверное регулярное выражение.</translation>
    </message>
</context>
<context>
    <name>GotoLineDialog</name>
    <message>
        <source>Goto Line</source>
        <translation type="obsolete">Перейти к строке</translation>
    </message>
    <message>
        <source>Line</source>
        <translation type="obsolete">Строка</translation>
    </message>
    <message>
        <source>Goto</source>
        <translation type="obsolete">Перейти</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
</context>
<context>
    <name>GotoLineWidget</name>
    <message>
        <location filename="../gotolinewidget.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="59"/>
        <source>Line</source>
        <translation>Строка</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="37"/>
        <source>Goto</source>
        <translation>Перейти</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрыть</translation>
    </message>
</context>
<context>
    <name>GraphicFileChooser</name>
    <message>
        <location filename="../graphicfilechooser.ui" line="36"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="95"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="221"/>
        <source>Use &quot;figure&quot; environment</source>
        <translation>Использовать окружение &quot;figure&quot;</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="152"/>
        <source>Caption</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="176"/>
        <source>Above</source>
        <translation>Сверху</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="181"/>
        <source>Below</source>
        <translation>Снизу</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="195"/>
        <source>Placement</source>
        <translation>Расположение</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="208"/>
        <source>hbtp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="233"/>
        <source>Center</source>
        <translation>По центру</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.cpp" line="49"/>
        <source>Select a File</source>
        <translation>Выберите файл</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Выход</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="obsolete">Назад</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Вперед</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="obsolete">Домой</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Go</source>
        <translation type="obsolete">Перейти</translation>
    </message>
</context>
<context>
    <name>KeySequenceDialog</name>
    <message>
        <location filename="../keysequencedialog.ui" line="37"/>
        <location filename="../keysequencedialog.cpp" line="43"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../keysequencedialog.ui" line="14"/>
        <source>Shortcut</source>
        <translation>Быстрая клавиша</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>LatexEditor</name>
    <message>
        <location filename="../latexeditor.cpp" line="914"/>
        <source>Undo</source>
        <translation>Отменить действие</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="917"/>
        <source>Redo</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="921"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="924"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="927"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="932"/>
        <source>Select All</source>
        <translation>Выделить все</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="938"/>
        <source>Check Spelling Word</source>
        <translation>Проверить правописание слова</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="943"/>
        <source>Check Spelling Selection</source>
        <translation>Проверить правописание в выделении</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="946"/>
        <source>Check Spelling Document</source>
        <translation>Проверить правописание в документе</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="953"/>
        <source>Jump to the end of this block</source>
        <translation>Перейти к концу этого блока</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="959"/>
        <source>Jump to pdf</source>
        <translation>Перейти к pdf</translation>
    </message>
</context>
<context>
    <name>LetterDialog</name>
    <message>
        <location filename="../letterdialog.cpp" line="56"/>
        <source>Quick Letter</source>
        <translation>Быстрое письмо</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="40"/>
        <source>Typeface Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="53"/>
        <source>Encoding</source>
        <translation>Кодировка</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="60"/>
        <source>AMS Packages</source>
        <translation>Пакеты AMS</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="79"/>
        <source>Paper Size</source>
        <translation>Размер бумаги</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>LightFindWidget</name>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Invalid regular expression.</source>
        <translation>Неверное регулярное выражение.</translation>
    </message>
</context>
<context>
    <name>LightLatexEditor</name>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>You do not have read permission to this file.</source>
        <translation>У Вас нет прав на чтение этого файла.</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="900"/>
        <location filename="../lightlatexeditor.cpp" line="903"/>
        <location filename="../lightlatexeditor.cpp" line="906"/>
        <source>Click to jump to the bookmark</source>
        <translation>Нажмите для перехода к закладке</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="910"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="913"/>
        <source>Goto Line</source>
        <translation>Перейти к строке</translation>
    </message>
</context>
<context>
    <name>LightLineNumberWidget</name>
    <message>
        <location filename="../lightlinenumberwidget.cpp" line="181"/>
        <source>Click to add or remove a bookmark</source>
        <translation>Нажмите для добавления или удаления закладки</translation>
    </message>
</context>
<context>
    <name>LineNumberWidget</name>
    <message>
        <location filename="../linenumberwidget.cpp" line="286"/>
        <source>Click to add or remove a bookmark</source>
        <translation>Нажмите для добавления или удаления закладки</translation>
    </message>
</context>
<context>
    <name>LogEditor</name>
    <message>
        <source>Click to jump to the line</source>
        <translation type="obsolete">Нажмите для перехода к строке</translation>
    </message>
</context>
<context>
    <name>PageItem</name>
    <message>
        <location filename="../pageitem.cpp" line="137"/>
        <location filename="../pageitem.cpp" line="139"/>
        <source>Click to jump to the line</source>
        <translation>Нажмите для перехода к строке</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="146"/>
        <source>Number of words in the document</source>
        <translation>Количество слов в документе</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="150"/>
        <source>Number of words in the page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="154"/>
        <source>Convert page to png image</source>
        <translation>Преобразовать страницу в изображение png</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="158"/>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="unfinished">Проверить правописание на этой странице</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="162"/>
        <source>Open the file browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="547"/>
        <source>Copy text</source>
        <translation>Копировать текст</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="548"/>
        <source>Copy image</source>
        <translation>Копировать изображение</translation>
    </message>
</context>
<context>
    <name>PaperDialog</name>
    <message>
        <source>Paper Size</source>
        <translation type="obsolete">Размер бумаги</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>PdfDocumentWidget</name>
    <message>
        <source>Click to jump to the line</source>
        <translation type="obsolete">Нажмите для перехода к строке</translation>
    </message>
</context>
<context>
    <name>PdfViewer</name>
    <message>
        <location filename="../pdfviewer.cpp" line="134"/>
        <location filename="../pdfviewer.cpp" line="1117"/>
        <source>Pages</source>
        <translation>Страницы</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="184"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="110"/>
        <location filename="../pdfviewer.cpp" line="153"/>
        <location filename="../pdfviewer.cpp" line="1110"/>
        <source>Structure</source>
        <translation>Структура</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="188"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="190"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="201"/>
        <source>Previous</source>
        <translation>Предыдущая</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="205"/>
        <source>Next</source>
        <translation>Следующая</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="211"/>
        <source>&amp;View</source>
        <translation>П&amp;росмотр</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="212"/>
        <source>Fit Width</source>
        <translation>По ширине страницы</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="215"/>
        <source>Fit Page</source>
        <translation>По размеру страницы</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="218"/>
        <source>Zoom In</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="223"/>
        <source>Zoom Out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="228"/>
        <source>Continuous</source>
        <translation>Непрерывно</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="233"/>
        <source>Two pages</source>
        <translation>Две страницы</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="238"/>
        <source>Rotate left</source>
        <translation>Повернуть влево</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="242"/>
        <source>Rotate right</source>
        <translation>Повернуть вправо</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="246"/>
        <source>Presentation...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="255"/>
        <source>Previous Position</source>
        <translation>Предыдущее положение</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="260"/>
        <source>Next Position</source>
        <translation>Следующее положение</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">Проверить правописание на этой странице</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Сохранить как</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="191"/>
        <location filename="../pdfviewer.cpp" line="288"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="299"/>
        <location filename="../pdfviewer.cpp" line="992"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="302"/>
        <source>External Viewer</source>
        <translation>Внешняя программа просмотра</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="441"/>
        <location filename="../pdfviewer.cpp" line="669"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>File not found</source>
        <translation>Файл не найден</translation>
    </message>
</context>
<context>
    <name>PdfViewerWidget</name>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="104"/>
        <source>Show/Hide Table of contents</source>
        <translation>Показать/скрыть содержание</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="110"/>
        <source>Previous</source>
        <translation>Предыдущая</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="114"/>
        <source>Next</source>
        <translation>Следующая</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="120"/>
        <source>Fit Width</source>
        <translation>По ширине страницы</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="123"/>
        <source>Fit Page</source>
        <translation>По размеру страницы</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="126"/>
        <source>Zoom In</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="131"/>
        <source>Zoom Out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="144"/>
        <source>Continuous</source>
        <translation>Непрерывно</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="149"/>
        <source>Two pages</source>
        <translation>Две страницы</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="154"/>
        <source>Rotate left</source>
        <translation>Повернуть влево</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="158"/>
        <source>Rotate right</source>
        <translation>Повернуть вправо</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="162"/>
        <source>Presentation...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="195"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="206"/>
        <source>Previous Position</source>
        <translation>Предыдущее положение</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="211"/>
        <source>Next Position</source>
        <translation>Следующее положение</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="220"/>
        <location filename="../pdfviewerwidget.cpp" line="928"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="223"/>
        <source>External Viewer</source>
        <translation>Внешняя программа просмотра</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">Проверить правописание на этой странице</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>File not found</source>
        <translation>Файл не найден</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Сохранить как</translation>
    </message>
</context>
<context>
    <name>QuickBeamerDialog</name>
    <message>
        <location filename="../quickbeamerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="22"/>
        <source>AMS Packages</source>
        <translation>Пакеты AMS</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="29"/>
        <source>Encoding</source>
        <translation>Кодировка</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="39"/>
        <source>Typeface Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="46"/>
        <source>babel Package</source>
        <translation>Пакет babel</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="59"/>
        <source>graphicx Package</source>
        <translation>Пакет graphicx</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="66"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="79"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="86"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.cpp" line="23"/>
        <source>Quick Start</source>
        <translation>Быстрый старт</translation>
    </message>
</context>
<context>
    <name>QuickDocumentDialog</name>
    <message>
        <location filename="../quickdocumentdialog.cpp" line="32"/>
        <source>Quick Start</source>
        <translation>Быстрый старт</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="157"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="41"/>
        <location filename="../quickdocumentdialog.ui" line="70"/>
        <location filename="../quickdocumentdialog.ui" line="115"/>
        <location filename="../quickdocumentdialog.ui" line="144"/>
        <location filename="../quickdocumentdialog.ui" line="199"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="25"/>
        <source>Document Class</source>
        <translation>Класс документа</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="173"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="183"/>
        <source>babel Package</source>
        <translation>Пакет babel</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="206"/>
        <source>geometry Package</source>
        <translation>Пакет geometry</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="213"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="220"/>
        <source>AMS Packages</source>
        <translation>Пакеты AMS</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="237"/>
        <source>graphicx Package</source>
        <translation>Пакет graphicx</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="244"/>
        <source>lmodern Package</source>
        <translation>Пакет lmodern</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="251"/>
        <source>Kpfonts Package</source>
        <translation>Пакет Kpfonts</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="258"/>
        <source>Fourier Package</source>
        <translation>Пакет Fourier</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="83"/>
        <source>Typeface Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="99"/>
        <source>Paper Size</source>
        <translation>Размер бумаги</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="54"/>
        <source>Other Options</source>
        <translation>Другие настройки</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="128"/>
        <source>Encoding</source>
        <translation>Кодировка</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="230"/>
        <source>makeidx Package</source>
        <translation>Пакет makeidx</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>QuickXelatexDialog</name>
    <message>
        <location filename="../quickxelatexdialog.ui" line="25"/>
        <location filename="../quickxelatexdialog.ui" line="83"/>
        <location filename="../quickxelatexdialog.ui" line="115"/>
        <location filename="../quickxelatexdialog.ui" line="170"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="38"/>
        <source>Other Options</source>
        <translation type="unfinished">Другие настройки</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="51"/>
        <source>Document Class</source>
        <translation type="unfinished">Класс документа</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="70"/>
        <source>Paper Size</source>
        <translation type="unfinished">Размер бумаги</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="96"/>
        <source>Typeface Size</source>
        <translation type="unfinished">Размер шрифта</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="128"/>
        <source>Author</source>
        <translation type="unfinished">Автор</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="147"/>
        <source>Title</source>
        <translation type="unfinished">Заголовок</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="154"/>
        <source>polyglossia Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="177"/>
        <source>geometry Package</source>
        <translation type="unfinished">Пакет geometry</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="184"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="191"/>
        <source>AMS Packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="201"/>
        <source>graphicx Package</source>
        <translation type="unfinished">Пакет graphicx</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.cpp" line="31"/>
        <source>Quick Start</source>
        <translation type="unfinished">Быстрый старт</translation>
    </message>
</context>
<context>
    <name>RefDialog</name>
    <message>
        <location filename="../refdialog.ui" line="14"/>
        <location filename="../refdialog.ui" line="40"/>
        <source>Labels</source>
        <translation>Метки</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>ReplaceDialog</name>
    <message>
        <source>Replace Text</source>
        <translation type="obsolete">Заменить текст</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation type="obsolete">Заменить</translation>
    </message>
    <message>
        <source>Find</source>
        <translation type="obsolete">Искать</translation>
    </message>
    <message>
        <source>Replace All</source>
        <translation type="obsolete">Заменить все</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Настройки</translation>
    </message>
    <message>
        <source>Whole words only</source>
        <translation type="obsolete">Слова целиком</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation type="obsolete">Различать регистр</translation>
    </message>
    <message>
        <source>Start at Beginning</source>
        <translation type="obsolete">Начать с начала</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="obsolete">Направление</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Вперед</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="obsolete">Назад</translation>
    </message>
    <message>
        <source>Replace this occurrence ? </source>
        <translation type="obsolete">Заменить это вхождение? </translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>ReplaceWidget</name>
    <message>
        <location filename="../replacewidget.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="34"/>
        <location filename="../replacewidget.ui" line="60"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="70"/>
        <source>Forward</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="222"/>
        <location filename="../replacewidget.cpp" line="251"/>
        <source>Text must be selected before checking this option.</source>
        <translation>Для использования этой опции выделите текст.</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="225"/>
        <source>In selection only</source>
        <translation>В выбранном</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="130"/>
        <source>Backward</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="97"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="123"/>
        <source>Replace All</source>
        <translation>Заменить все</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="158"/>
        <source>Whole words only</source>
        <translation>Слова целиком</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="165"/>
        <source>Case sensitive</source>
        <translation>Различать регистр</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="172"/>
        <source>Start at Beginning</source>
        <translation>Начать с начала</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="215"/>
        <source>Regular Expression</source>
        <translation>Регулярное выражение</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Invalid regular expression.</source>
        <translation>Неверное регулярное выражение.</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Replace this occurrence ? </source>
        <translation>Заменить это вхождение? </translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../scandialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="20"/>
        <source>In directory :</source>
        <translation>В каталоге:</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="57"/>
        <source>Text :</source>
        <translation>Текст:</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="67"/>
        <location filename="../scandialog.cpp" line="43"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="74"/>
        <source>Include subdirectories</source>
        <translation>Включая подкаталоги</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="81"/>
        <source>Abort</source>
        <translation>Прекратить</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="100"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>SourceView</name>
    <message>
        <location filename="../sourceview.cpp" line="50"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="61"/>
        <source>Check differences</source>
        <translation>Просмотр различий</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="167"/>
        <source>Open File</source>
        <translation>Открыть файл</translation>
    </message>
</context>
<context>
    <name>SpellerDialog</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>Could not start the command.</source>
        <translation type="obsolete">Не могу запустить команду.</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="13"/>
        <source>Check Spelling</source>
        <translation>Проверка правописания</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="80"/>
        <source>Suggested words :</source>
        <translation>Предложения:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="33"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="19"/>
        <source>Unknown word:</source>
        <translation>Неизвестное слово:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="73"/>
        <source>Always ignore</source>
        <translation>Пропустить все</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="130"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="43"/>
        <source>Replace with:</source>
        <translation>Заменить на:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="53"/>
        <source>Ignore</source>
        <translation>Пропустить</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="121"/>
        <source>Check spelling selection...</source>
        <translation>Проверить правописание в выделении...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="131"/>
        <source>Check spelling from cursor...</source>
        <translation>Проверить правописание с позиции курсора...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="152"/>
        <location filename="../spellerdialog.cpp" line="169"/>
        <location filename="../spellerdialog.cpp" line="193"/>
        <location filename="../spellerdialog.cpp" line="319"/>
        <source>No more misspelled words</source>
        <translation>Слова с ошибками отсутствуют</translation>
    </message>
</context>
<context>
    <name>StructDialog</name>
    <message>
        <location filename="../structdialog.ui" line="14"/>
        <source>Structure</source>
        <translation>Структура</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="37"/>
        <source>Numeration</source>
        <translation>Перечисление</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="53"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>SymbolListWidget</name>
    <message>
        <location filename="../symbollistwidget.cpp" line="52"/>
        <source>Add to favorites</source>
        <translation>Добавить в избранное</translation>
    </message>
    <message>
        <location filename="../symbollistwidget.cpp" line="53"/>
        <source>Remove from favorites</source>
        <translation>Удалить из избранного</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <location filename="../tabdialog.cpp" line="108"/>
        <source>Quick Tabular</source>
        <translation>Быстрая таблица</translation>
    </message>
    <message>
        <source>Horizontal Separator</source>
        <translation type="obsolete">Горизонтальный разделитель</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="37"/>
        <source>Num of Columns</source>
        <translation>Кол-во столбцов</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="78"/>
        <source>Columns</source>
        <translation>Столбцы</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="86"/>
        <source>Column :</source>
        <translation>Столбец:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="113"/>
        <source>Alignment :</source>
        <translation>Выравнивание:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="127"/>
        <source>Left Border :</source>
        <translation>Левая граница:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="167"/>
        <source>Apply to all columns</source>
        <translation>Применить ко всем столбцам</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="194"/>
        <source>Right Border (last column) :</source>
        <translation>Правая граница (последний столбец):</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="263"/>
        <source>Rows</source>
        <translation>Строки</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="271"/>
        <source>Row :</source>
        <translation>Строка:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="296"/>
        <source>Top Border</source>
        <translation>Верхняя граница</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="305"/>
        <source>Merge columns :</source>
        <translation>Объединить столбцы:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="321"/>
        <source>-&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="364"/>
        <source>Apply to all rows</source>
        <translation>Применить ко всем строкам</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="389"/>
        <source>Bottom Border (last row)</source>
        <translation>Нижняя граница (последняя строка)</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="396"/>
        <source>Add vertical margin for each row</source>
        <translation>Добавить вертикальный отступ для всех строк</translation>
    </message>
    <message>
        <source>Columns Alignment</source>
        <translation type="obsolete">Выравнивание в колонке</translation>
    </message>
    <message>
        <source>Vertical Separator</source>
        <translation type="obsolete">Вертикальный разделитель</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="232"/>
        <source>Num of Rows</source>
        <translation>Кол-во строк</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>TabbingDialog</name>
    <message>
        <location filename="../tabbingdialog.cpp" line="27"/>
        <source>Quick Tabbing</source>
        <translation>Быстрый разделитель</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="67"/>
        <source>Spacing</source>
        <translation>Промежутки</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="80"/>
        <source>Num of Rows</source>
        <translation>Кол-во строк</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="93"/>
        <source>Num of Columns</source>
        <translation>Кол-во столбцов</translation>
    </message>
</context>
<context>
    <name>TexdocDialog</name>
    <message>
        <location filename="../texdocdialog.ui" line="20"/>
        <source>Search documentation about :</source>
        <translation>Поиск документации о:</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="59"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="116"/>
        <source>Texdoc command :</source>
        <translation>Команда Texdoc:</translation>
    </message>
    <message>
        <location filename="../texdocdialog.cpp" line="32"/>
        <source>Browse program</source>
        <translation>Выбрать программу</translation>
    </message>
</context>
<context>
    <name>Texmaker</name>
    <message>
        <location filename="../texmaker.cpp" line="667"/>
        <location filename="../texmaker.cpp" line="837"/>
        <location filename="../texmaker.cpp" line="2658"/>
        <location filename="../texmaker.cpp" line="5806"/>
        <source>Structure</source>
        <translation>Структура</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10360"/>
        <location filename="../texmaker.cpp" line="10400"/>
        <source>Click to jump to the line</source>
        <translation>Нажмите для перехода к строке</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="686"/>
        <location filename="../texmaker.cpp" line="5811"/>
        <source>Relation symbols</source>
        <translation>Символы отношений</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="696"/>
        <location filename="../texmaker.cpp" line="5816"/>
        <source>Arrow symbols</source>
        <translation>Символы стрелок</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="705"/>
        <location filename="../texmaker.cpp" line="5821"/>
        <source>Miscellaneous symbols</source>
        <translation>Прочие символы</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="714"/>
        <location filename="../texmaker.cpp" line="5826"/>
        <source>Delimiters</source>
        <translation>Разделители</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="723"/>
        <location filename="../texmaker.cpp" line="5831"/>
        <source>Greek letters</source>
        <translation>Греческие буквы</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="741"/>
        <location filename="../texmaker.cpp" line="5841"/>
        <source>Favorites symbols</source>
        <translation>Избранные символы</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="788"/>
        <location filename="../texmaker.cpp" line="816"/>
        <location filename="../texmaker.cpp" line="5856"/>
        <source>MetaPost Commands</source>
        <translation>Команды MetaPost</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2663"/>
        <source>Messages / Log File</source>
        <translation>Сообщения / Лог</translation>
    </message>
    <message>
        <source>Line : Col :</source>
        <translation type="obsolete">Строка : Кол :</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1383"/>
        <location filename="../texmaker.cpp" line="11064"/>
        <source>Normal Mode</source>
        <translation>Нормальный режим</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="obsolete">Готов</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1407"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1408"/>
        <location filename="../texmaker.cpp" line="1409"/>
        <location filename="../texmaker.cpp" line="2914"/>
        <location filename="../texmaker.cpp" line="2915"/>
        <source>New</source>
        <translation>Создать</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="obsolete">Созд&amp;ать...</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1418"/>
        <location filename="../texmaker.cpp" line="1419"/>
        <location filename="../texmaker.cpp" line="2919"/>
        <location filename="../texmaker.cpp" line="2920"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation type="obsolete">&amp;Открыть...</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1424"/>
        <source>Open Recent</source>
        <translation>Открыть недавние</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1450"/>
        <location filename="../texmaker.cpp" line="1451"/>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="12260"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>&amp;Save...</source>
        <translation type="obsolete">&amp;Сохранить...</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1458"/>
        <location filename="../texmaker.cpp" line="4020"/>
        <location filename="../texmaker.cpp" line="4093"/>
        <location filename="../texmaker.cpp" line="5732"/>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation type="obsolete">Сохранить &amp;как...</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1462"/>
        <source>Save All</source>
        <translation>Сохранить все</translation>
    </message>
    <message>
        <source>Print Source</source>
        <translation type="obsolete">Печать искодного текста</translation>
    </message>
    <message>
        <source>&amp;Print Source</source>
        <translation type="obsolete">&amp;Печать искодного текста</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1242"/>
        <location filename="../texmaker.cpp" line="1470"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>C&amp;lose</source>
        <translation type="obsolete">&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1476"/>
        <source>Close All</source>
        <translation>Закрыть все</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1494"/>
        <location filename="../texmaker.cpp" line="1495"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation type="obsolete">В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1501"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1502"/>
        <location filename="../texmaker.cpp" line="1503"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation type="obsolete">О&amp;тменить действие</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1508"/>
        <location filename="../texmaker.cpp" line="1509"/>
        <source>Redo</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="obsolete">&amp;Повторить отменённое действие</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1515"/>
        <location filename="../texmaker.cpp" line="1516"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="obsolete">&amp;Копировать</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1521"/>
        <location filename="../texmaker.cpp" line="1522"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="obsolete">Вы&amp;резать</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1527"/>
        <location filename="../texmaker.cpp" line="1528"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="obsolete">&amp;Вставить</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1533"/>
        <source>Select All</source>
        <translation>Выделить все</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="obsolete">Выделить &amp;все</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1539"/>
        <source>Comment</source>
        <translation>Закомментировать</translation>
    </message>
    <message>
        <source>C&amp;omment Selection</source>
        <translation type="obsolete">За&amp;комментировать секцию</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1545"/>
        <source>Uncomment</source>
        <translation>Раскомментировать</translation>
    </message>
    <message>
        <source>Unco&amp;mment Selection</source>
        <translation type="obsolete">&amp;Раскомментировать секцию</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1551"/>
        <source>Indent</source>
        <translation>Увеличить отступ</translation>
    </message>
    <message>
        <source>&amp;Indent Selection</source>
        <translation type="obsolete">От&amp;ступ секции</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1663"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <source>&amp;Find...</source>
        <translation type="obsolete">&amp;Найти...</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1669"/>
        <source>FindNext</source>
        <translation>Продолжить поиск</translation>
    </message>
    <message>
        <source>Find &amp;Next</source>
        <translation type="obsolete">Пр&amp;одолжить поиск</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1680"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <source>&amp;Replace...</source>
        <translation type="obsolete">&amp;Заменить...</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1686"/>
        <source>Goto Line</source>
        <translation>Перейти к строке</translation>
    </message>
    <message>
        <source>&amp;Goto Line...</source>
        <translation type="obsolete">П&amp;ерейти к строке...</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1700"/>
        <source>Refresh Structure</source>
        <translation>Обновить структуру</translation>
    </message>
    <message>
        <source>Tool Actions</source>
        <translation type="obsolete">Действия инструментов</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1712"/>
        <location filename="../texmaker.cpp" line="2969"/>
        <location filename="../texmaker.cpp" line="9757"/>
        <source>Quick Build</source>
        <translation>Быстрая сборка</translation>
    </message>
    <message>
        <source>View Log File</source>
        <translation type="obsolete">Просмотр лога</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1818"/>
        <location filename="../texmaker.cpp" line="3020"/>
        <source>Previous LaTeX Error</source>
        <translation>Предыдущая ошибка LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1820"/>
        <location filename="../texmaker.cpp" line="3014"/>
        <source>Next LaTeX Error</source>
        <translation>Следующая ошибка LaTeX</translation>
    </message>
    <message>
        <source>View DVI</source>
        <translation type="obsolete">Просмотр DVI</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1733"/>
        <location filename="../texmaker.cpp" line="2996"/>
        <source>View PS</source>
        <translation>Просмотр PS</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1743"/>
        <location filename="../texmaker.cpp" line="2997"/>
        <source>View PDF</source>
        <translation>Просмотр PDF</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1711"/>
        <source>&amp;Tools</source>
        <translation>&amp;Инструменты</translation>
    </message>
    <message>
        <source>&amp;Quick Build</source>
        <translation type="obsolete">&amp;Быстрая сборка</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1723"/>
        <location filename="../texmaker.cpp" line="2995"/>
        <source>View Dvi</source>
        <translation>Просмотр DVI</translation>
    </message>
    <message>
        <source>View &amp;Dvi</source>
        <translation type="obsolete">Просмотр &amp;DVI</translation>
    </message>
    <message>
        <source>Dvi-&gt;PS</source>
        <translation type="obsolete">DVI-&gt;PS</translation>
    </message>
    <message>
        <source>View &amp;PS</source>
        <translation type="obsolete">Просмотр &amp;PS</translation>
    </message>
    <message>
        <source>&amp;View PDF</source>
        <translation type="obsolete">Просмотр PD&amp;F</translation>
    </message>
    <message>
        <source>View Lo&amp;g</source>
        <translation type="obsolete">Просмотр &amp;лога</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1433"/>
        <location filename="../texmaker.cpp" line="1799"/>
        <location filename="../texmaker.cpp" line="2547"/>
        <source>Clean</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <source>Convert to Html</source>
        <translation type="obsolete">Преобразовать в HTML</translation>
    </message>
    <message>
        <source>Sectionning</source>
        <translation type="obsolete">Секции</translation>
    </message>
    <message>
        <source>Environment</source>
        <translation type="obsolete">Окружение</translation>
    </message>
    <message>
        <source>List Environment</source>
        <translation type="obsolete">Списки</translation>
    </message>
    <message>
        <source>Font Styles</source>
        <translation type="obsolete">Стили шрифтов</translation>
    </message>
    <message>
        <source>Tabular Environment</source>
        <translation type="obsolete">Таблицы</translation>
    </message>
    <message>
        <source>Spacing</source>
        <translation type="obsolete">Разделители</translation>
    </message>
    <message>
        <source>Math Font Styles</source>
        <translation type="obsolete">Стиль шрифта</translation>
    </message>
    <message>
        <source>Math Accents</source>
        <translation type="obsolete">Акцент</translation>
    </message>
    <message>
        <source>Math Spaces</source>
        <translation type="obsolete">Промежутки</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2138"/>
        <source>&amp;Math</source>
        <translation>&amp;Математика</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2423"/>
        <source>Quick Start</source>
        <translation>Быстрый старт</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2432"/>
        <source>Quick Letter</source>
        <translation>Быстрое письмо</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2436"/>
        <source>Quick Tabular</source>
        <translation>Быстрая таблица (tabular)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2439"/>
        <source>Quick Tabbing</source>
        <translation>Быстрая таблица (tabbing)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2442"/>
        <source>Quick Array</source>
        <translation>Быстрый массив</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2422"/>
        <source>&amp;Wizard</source>
        <translation>П&amp;омощник</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2446"/>
        <source>&amp;Bibliography</source>
        <translation>&amp;Библиография</translation>
    </message>
    <message>
        <source>Edit User Tags</source>
        <translation type="obsolete">Редактировать теги пользователя</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2553"/>
        <source>User &amp;Tags</source>
        <translation>&amp;Метки пользователя</translation>
    </message>
    <message>
        <source>Edit User Commands</source>
        <translation type="obsolete">Редактировать команды пользователя</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2598"/>
        <source>User &amp;Commands</source>
        <translation>&amp;Команды пользователя</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2552"/>
        <source>&amp;User</source>
        <translation>По&amp;льзователь</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1224"/>
        <location filename="../texmaker.cpp" line="2646"/>
        <source>Next Document</source>
        <translation>Следующий документ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1220"/>
        <location filename="../texmaker.cpp" line="2651"/>
        <source>Previous Document</source>
        <translation>Предыдущий документ</translation>
    </message>
    <message>
        <source>Show Structure View</source>
        <translation type="obsolete">Показывать структуру</translation>
    </message>
    <message>
        <source>Show Output View</source>
        <translation type="obsolete">Показывать вывод</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2645"/>
        <source>&amp;View</source>
        <translation>П&amp;росмотр</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2702"/>
        <source>Configure Texmaker</source>
        <translation>Настроить Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2707"/>
        <location filename="../texmaker.cpp" line="11055"/>
        <source>Define Current Document as &apos;Master Document&apos;</source>
        <translation>Установить текущий документ как основной</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2701"/>
        <source>&amp;Options</source>
        <translation>&amp;Настройка</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1103"/>
        <source>Bold</source>
        <translation>Жирный</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1108"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="obsolete">Подчерк</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1118"/>
        <source>Left</source>
        <translation>Влево</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1123"/>
        <source>Center</source>
        <translation>По центру</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1128"/>
        <source>Right</source>
        <translation>Вправо</translation>
    </message>
    <message>
        <source>New Line</source>
        <translation type="obsolete">Следующая строка</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2763"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2764"/>
        <location filename="../texmaker.cpp" line="2765"/>
        <source>LaTeX Reference</source>
        <translation>Справка по LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2768"/>
        <location filename="../texmaker.cpp" line="2769"/>
        <source>User Manual</source>
        <translation>Руководство пользователя</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2801"/>
        <source>About Texmaker</source>
        <translation>О программе Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2991"/>
        <source>Run</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3004"/>
        <source>View</source>
        <translation>Просмотр</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="4753"/>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="8441"/>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10020"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <source>You do not have read permission to this file.</source>
        <translation>У Вас нет прав на чтение этого файла.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3595"/>
        <location filename="../texmaker.cpp" line="3655"/>
        <location filename="../texmaker.cpp" line="12312"/>
        <source>Open File</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>Файл не может быть сохранен. Пожауйста, проверьте, есть ли у Вас права на запись в этот файл.</translation>
    </message>
    <message>
        <source>A Document with this name already exists.
Do you want to overwrite it? </source>
        <translation type="obsolete">Документ с таким названием уже существует. Хотите его перезаписать?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4130"/>
        <location filename="../texmaker.cpp" line="4194"/>
        <location filename="../texmaker.cpp" line="4264"/>
        <location filename="../texmaker.cpp" line="4370"/>
        <source>The document contains unsaved work. Do you want to save it before closing?</source>
        <translation>В документе содержатся несохраненные данные. Хотите сохранить документ перед закрытием?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2732"/>
        <source>Interface Language</source>
        <translation>Язык интерфейса</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Save and Close</source>
        <translation>Сохранить и закрыть</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Don&apos;t Save and Close</source>
        <translation>Закрыть не сохраняя</translation>
    </message>
    <message>
        <source>The document contains unsaved work. Do you want to save it before exiting?</source>
        <translation type="obsolete">В документе содержатся несохраненные данные. Хотите сохранить документ перед выходом?</translation>
    </message>
    <message>
        <source>Line: %d Col: %d</source>
        <translation type="obsolete">Строка: %d Кол: %d</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <source>Can&apos;t detect the file name</source>
        <translation>Не могу определить имя файла</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Could not start the command.</source>
        <translation>Не могу запустить команду.</translation>
    </message>
    <message>
        <source>View Dvi file</source>
        <translation type="obsolete">Просмотр файла DVI</translation>
    </message>
    <message>
        <source>View PS file</source>
        <translation type="obsolete">Просмотр файла PS</translation>
    </message>
    <message>
        <source>View Pdf file</source>
        <translation type="obsolete">Просмотр файла PDF</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind)</source>
        <translation type="obsolete">Удалить выходные файлы созданные LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete Files</source>
        <translation>Удалить файлы</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre,.nlg,.nlo,.nls)</source>
        <translation type="obsolete">Удалить выходные файлы созданные LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre,.nlg,.nlo,.nls)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10020"/>
        <source>Log File not found !</source>
        <translation>Файл с логом не найден!</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10474"/>
        <location filename="../texmaker.cpp" line="10509"/>
        <source>No LaTeX errors detected !</source>
        <translation>Ошибок LaTeX не обнаружено!</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="6667"/>
        <source>Select an image File</source>
        <translation>Выберите файл с изображением</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4753"/>
        <source>Error : Can&apos;t open the dictionary</source>
        <translation>Ошибка: Не могу открыть словарь</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4311"/>
        <location filename="../texmaker.cpp" line="6698"/>
        <location filename="../texmaker.cpp" line="6723"/>
        <source>Select a File</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4774"/>
        <source>Browse script</source>
        <translation>Просмотр скрипта</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5710"/>
        <source>Delete settings file?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>Удалить файл настроек?
(Texmaker будет закрыт, вы будете должны перезапустить его.)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Ok</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5753"/>
        <source>Replace settings file by a new one?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>Заменить файл настроек новым?
(Texmaker будет закрыт, вы будете должны перезапустить его.)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5778"/>
        <source>Opened Files</source>
        <translation>Открытые файлы</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8441"/>
        <source>A document must be saved with an extension (and without spaces or accents in the name) before being used by a command.</source>
        <translation>Перед запуском команды требуется сохранить документ с расширением (без пробелов и специальных символов в имени).</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete the output files generated by LaTeX ?</source>
        <translation>Удалить выходные файлы, созданные LaTeX?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PdfLaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>dvips</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvi Viewer</source>
        <translation>Просмотр DVI</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PS Viewer</source>
        <translation>Просмотр PS</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvipdfm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ps2pdf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Bibtex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Makeindex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>metapost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ghostscript</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Asymptote</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Latexmk</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>R Sweave</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>XeLaTex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LuaLaTex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <source>File not found</source>
        <translation>Файл не найден</translation>
    </message>
    <message>
        <source>Texmaker : User Manual</source>
        <translation type="obsolete">Texmaker: Руководство Пользователя</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11079"/>
        <location filename="../texmaker.cpp" line="12629"/>
        <source>Normal Mode (current master document :</source>
        <translation>Нормальный режим (текущий основной документ :</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11081"/>
        <location filename="../texmaker.cpp" line="12631"/>
        <source>Master Document :</source>
        <translation>Основной документ:</translation>
    </message>
    <message>
        <source>TeX files (*.tex *.bib *.sty *.cls *.mp);;All files (*.*)</source>
        <translation type="obsolete">Файлы TeX (*.tex *.bib *.sty *.cls *.mp);;Все файлы (*.*)</translation>
    </message>
    <message>
        <source>Process launched</source>
        <translation type="obsolete">Процесс запущен</translation>
    </message>
    <message>
        <source>Process failed</source>
        <translation type="obsolete">Процесс завершился с ошибкой</translation>
    </message>
    <message>
        <source>Process exited normally</source>
        <translation type="obsolete">Процесс завершился успешно</translation>
    </message>
    <message>
        <source>Process exited with error(s)</source>
        <translation type="obsolete">Процесс завершился с ошибкой</translation>
    </message>
    <message>
        <source>Graphic files (*.eps *.pdf *.png);;All files (*.*)</source>
        <translation type="obsolete">Графические файлы (*.eps *.pdf *.png);;Все файлы (*.*)</translation>
    </message>
    <message>
        <source>TeX files (*.tex);;All files (*.*)</source>
        <translation type="obsolete">Файлы TeX (*.tex);;Все файлы (*.*)</translation>
    </message>
    <message>
        <source>Texmaker : LaTeX Reference</source>
        <translation type="obsolete">Texmaker : Руководство LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="732"/>
        <location filename="../texmaker.cpp" line="5836"/>
        <source>Most used symbols</source>
        <translation>Часто используемые символы</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="764"/>
        <location filename="../texmaker.cpp" line="5872"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="780"/>
        <location filename="../texmaker.cpp" line="813"/>
        <location filename="../texmaker.cpp" line="5846"/>
        <source>Pstricks Commands</source>
        <translation>Команды Pstricks</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="796"/>
        <location filename="../texmaker.cpp" line="819"/>
        <location filename="../texmaker.cpp" line="5861"/>
        <source>Tikz Commands</source>
        <translation>Команды Tikz</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="804"/>
        <location filename="../texmaker.cpp" line="822"/>
        <location filename="../texmaker.cpp" line="5866"/>
        <source>Asymptote Commands</source>
        <translation>Команды Asymptote</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1215"/>
        <source>Toggle between the master document and the current document</source>
        <translation>Переключиться между основным и текущим документами</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1414"/>
        <source>New by copying an existing file</source>
        <translation>Создать из существующего файла</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1437"/>
        <source>Session</source>
        <translation>Сеанс</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1438"/>
        <source>Restore previous session</source>
        <translation>Восстановить предыдущий сеанс</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1443"/>
        <source>Save session</source>
        <translation>Сохранить сеанс</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1446"/>
        <source>Load session</source>
        <translation>Загрузить сеанс</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1466"/>
        <source>Save A Copy</source>
        <translation>Сохранить копию</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1480"/>
        <source>Reload document from file</source>
        <translation>Восстановить с диска</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1484"/>
        <source>Reload all documents from file</source>
        <translation>Восстановить все документы с диска</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1488"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1557"/>
        <source>Unindent</source>
        <translation>Уменьшить отступ</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1564"/>
        <source>Fold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1568"/>
        <source>Unfold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1573"/>
        <source>&amp;Fold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1616"/>
        <source>&amp;Unfold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1675"/>
        <source>Find In Directory</source>
        <translation>Поиск в каталоге</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1693"/>
        <source>Check Spelling</source>
        <translation>Проверка правописания</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1706"/>
        <source>Refresh Bibliography</source>
        <translation>Обновить библиографию</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1758"/>
        <location filename="../texmaker.cpp" line="3010"/>
        <source>View Log</source>
        <translation>Просмотр лога</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1804"/>
        <source>Open Terminal</source>
        <translation>Открыть терминал</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1808"/>
        <source>Export via TeX4ht</source>
        <translation>Экспорт через TeX4ht</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1813"/>
        <source>Convert to unicode</source>
        <translation>Преобразовать в юникод</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1823"/>
        <source>&amp;LaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1857"/>
        <source>&amp;Sectioning</source>
        <translation>&amp;Секции</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1887"/>
        <source>&amp;Environment</source>
        <translation>&amp;Окружение</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1933"/>
        <source>&amp;List Environment</source>
        <translation>С&amp;писки</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1957"/>
        <source>Font St&amp;yles</source>
        <translation>Стили &amp;шрифтов</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1994"/>
        <source>&amp;Tabular Environment</source>
        <translation>&amp;Таблицы</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2020"/>
        <source>S&amp;pacing</source>
        <translation>П&amp;ромежутки</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2047"/>
        <source>International &amp;Accents</source>
        <translation>&amp;Диакритические знаки</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2089"/>
        <source>International &amp;Quotes</source>
        <translation>Иностранные &amp;кавычки</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2139"/>
        <source>Inline math mode $...$</source>
        <translation>Встроенная формула $...$</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2144"/>
        <source>Display math mode \[...\]</source>
        <translation>Выносная формула \[...\]</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2149"/>
        <source>Numbered equations \begin{equation}</source>
        <translation>Нумерованные уравнения \begin{equation}</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2202"/>
        <source>Math &amp;Functions</source>
        <translation>Математические &amp;функции</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2324"/>
        <source>Math Font St&amp;yles</source>
        <translation>Математические &amp;шрифты</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2358"/>
        <source>Math &amp;Accents</source>
        <translation>Математические &amp;значки над буквами</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2400"/>
        <source>Math S&amp;paces</source>
        <translation>Математические &amp;промежутки</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2426"/>
        <source>Quick Beamer Presentation</source>
        <translation>Быстрая презентация Beamer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2429"/>
        <source>Quick Xelatex Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2595"/>
        <location filename="../texmaker.cpp" line="8156"/>
        <location filename="../texmaker.cpp" line="8212"/>
        <source>Edit User &amp;Tags</source>
        <translation>Редактировать &amp;теги пользователя</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2620"/>
        <location filename="../texmaker.cpp" line="9717"/>
        <location filename="../texmaker.cpp" line="9753"/>
        <source>Edit User &amp;Commands</source>
        <translation>Редактировать &amp;команды пользователя</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2624"/>
        <source>Customize Completion</source>
        <translation>Настройка автозавершения</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2629"/>
        <source>Run script</source>
        <translation>Запуск скрипта</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2640"/>
        <source>Other script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2669"/>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Pdf Viewer</source>
        <translation>Просмотр PDF</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2686"/>
        <source>List of opened files</source>
        <translation>Список открытых файлов</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2693"/>
        <source>Full Screen</source>
        <translation>Во весь экран</translation>
    </message>
    <message>
        <source>Interface Appearance</source>
        <translation type="vanished">Вид интерфейса</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2727"/>
        <source>Change Interface Font</source>
        <translation>Сменить шрифт интерфейса</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2749"/>
        <source>Manage Settings File</source>
        <translation>Управление файлом настроек</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2751"/>
        <source>Settings File</source>
        <translation>Файл настроек</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2753"/>
        <source>Reset Settings</source>
        <translation>Сбросить настройки</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2756"/>
        <source>Save a copy of the settings file</source>
        <translation>Сохранить копию файла настроек</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2759"/>
        <source>Replace the settings file by a new one</source>
        <translation>Заменить файл настроек на новый</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2796"/>
        <source>Check for Update</source>
        <translation>Проверка обновлений</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3026"/>
        <source>Stop Process</source>
        <translation>Остановить процесс</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3751"/>
        <location filename="../texmaker.cpp" line="3900"/>
        <location filename="../texmaker.cpp" line="3959"/>
        <source>The document has been changed outside Texmaker.Do you want to reload it (and discard your changes) or save it (and overwrite the file)?</source>
        <translation>Документ был изменен внешним процессом. Вы хотите перезагрузить его (отменив свои изменения) или сохранить его (перезаписав файл)?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4539"/>
        <source>The document contains unsaved work.You will lose changes by reloading the document.</source>
        <oldsource>The document contains unsaved work. you will lose changes by reloading the file</oldsource>
        <translation>В документе есть несохраненные изменения. При восстановлении с диска они будут утеряны.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <source>Reload the file</source>
        <translation>Восстановить файл</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Make a copy of the %1.pdf/ps document in the &quot;build&quot; subdirectory and delete all the others %1.* files?</source>
        <translation>Создать копию документа %1.pdf/ps в каталоге &quot;build&quot; и удалить остальные файлы %1.*?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11623"/>
        <source>The language setting will take effect after restarting the application.</source>
        <translation>Настройки языка будут применены после перезапуска приложения.</translation>
    </message>
    <message>
        <source>The appearance setting will take effect after restarting the application.</source>
        <translation type="vanished">Настройки внешнего вида будут применены после перезапуска приложения.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1134"/>
        <location filename="../texmaker.cpp" line="1174"/>
        <source>New line</source>
        <translation>Новая строка</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1261"/>
        <location filename="../texmaker.cpp" line="1264"/>
        <location filename="../texmaker.cpp" line="1267"/>
        <source>Click to jump to the bookmark</source>
        <translation>Нажмите для перехода к закладке</translation>
    </message>
</context>
<context>
    <name>UnicodeDialog</name>
    <message>
        <location filename="../unicodedialog.ui" line="14"/>
        <location filename="../unicodedialog.ui" line="98"/>
        <source>Convert to unicode</source>
        <translation>Преобразовать в юникод</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="28"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="75"/>
        <source>Original Encoding</source>
        <translation>Исходная кодировка</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="145"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="56"/>
        <source>Select a File</source>
        <translation>Выберите файл</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>You do not have read permission to this file.</source>
        <translation>У Вас нет прав на чтение этого файла.</translation>
    </message>
</context>
<context>
    <name>UnicodeView</name>
    <message>
        <location filename="../unicodeview.cpp" line="56"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="152"/>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>Файл не может быть сохранен. Пожауйста, проверьте, есть ли у Вас права на запись в этот файл.</translation>
    </message>
</context>
<context>
    <name>UserCompletionDialog</name>
    <message>
        <location filename="../usercompletiondialog.ui" line="14"/>
        <source>Completion</source>
        <translation>Автозавершение</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="25"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="48"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="55"/>
        <source>( @ : placeholder )</source>
        <translation>( @ : заполнитель )</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="62"/>
        <source>( #bib# : bibliography item )</source>
        <translation>( #bib# : элемент библиографии )</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="69"/>
        <source>( #label# : label item )</source>
        <translation>( #label# : метка )</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="76"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="85"/>
        <source>Items already supplied by Texmaker</source>
        <translation>Элементы, которые предоставляет Texmaker</translation>
    </message>
</context>
<context>
    <name>UserMenuDialog</name>
    <message>
        <location filename="../usermenudialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>Редактировать метки пользователя</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="45"/>
        <source>Menu Item</source>
        <translation>Элемент меню</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="55"/>
        <source>LaTeX Content</source>
        <translation>Содержимое LaTeX</translation>
    </message>
</context>
<context>
    <name>UserQuickDialog</name>
    <message>
        <location filename="../userquickdialog.ui" line="14"/>
        <source>Quick Build Command</source>
        <translation>Команда быстрой сборки</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="42"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="70"/>
        <source>Ordered list of commands :</source>
        <translation>Упорядоченный список команд:</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="80"/>
        <source>Up</source>
        <translation>Вверх</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="87"/>
        <source>Down</source>
        <translation>Вниз</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="94"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>UserTagsListWidget</name>
    <message>
        <location filename="../usertagslistwidget.cpp" line="30"/>
        <source>Add tag</source>
        <translation>Добавить метку</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="32"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="34"/>
        <source>Modify</source>
        <translation>Изменить</translation>
    </message>
</context>
<context>
    <name>UserToolDialog</name>
    <message>
        <location filename="../usertooldialog.ui" line="14"/>
        <source>Edit User Commands</source>
        <translation>Редактировать команды пользователя</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="51"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(разделяйте команды символом &apos;|&apos;)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="70"/>
        <source>Menu Item</source>
        <translation>Элемент меню</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="77"/>
        <source>Command (% : filename without extension)</source>
        <translation>Команда (%s : имя файла без расширения)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="103"/>
        <source>wizard</source>
        <translation>мастер</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../versiondialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="20"/>
        <source>Check for available version</source>
        <translation>Проверить наличие новой версии</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="34"/>
        <source>Available version</source>
        <translation>Доступная версия</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="48"/>
        <source>Go to the download page</source>
        <translation>Перейти к странице загрузки</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="68"/>
        <source>You&apos;re using the version</source>
        <translation>Вы используете версию</translation>
    </message>
    <message>
        <location filename="../versiondialog.cpp" line="53"/>
        <location filename="../versiondialog.cpp" line="60"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>WebPublishDialog</name>
    <message>
        <source>Left</source>
        <translation type="obsolete">Влево</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">По центру</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="obsolete">Вправо</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="obsolete">Открыть файл</translation>
    </message>
    <message>
        <source>Convert to Html</source>
        <translation type="obsolete">Преобразовать в HTML</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
</context>
<context>
    <name>X11FontDialog</name>
    <message>
        <location filename="../x11fontdialog.ui" line="14"/>
        <source>Select a Font</source>
        <translation>Выбор шрифта</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="34"/>
        <source>Font Family</source>
        <translation>Семейство шрифтов</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="44"/>
        <source>Font Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>arraydialog</name>
    <message>
        <source>Num of Rows</source>
        <translation type="obsolete">Кол-во столбцов</translation>
    </message>
    <message>
        <source>Num of Columns</source>
        <translation type="obsolete">Кол-во колонок</translation>
    </message>
    <message>
        <source>Columns Alignment</source>
        <translation type="obsolete">Выравнивание в колонке</translation>
    </message>
    <message>
        <source>Environment</source>
        <translation type="obsolete">Окружение</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">По центру</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="obsolete">Влево</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="obsolete">Вправо</translation>
    </message>
</context>
<context>
    <name>letterdialog</name>
    <message>
        <source>Typeface Size</source>
        <translation type="obsolete">Размер шрифта</translation>
    </message>
    <message>
        <source>Paper Size</source>
        <translation type="obsolete">Размер бумаги</translation>
    </message>
    <message>
        <source>Encoding</source>
        <translation type="obsolete">Кодировка</translation>
    </message>
    <message>
        <source>AMS Packages</source>
        <translation type="obsolete">Пакет AMS</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>quickdocumentdialog</name>
    <message>
        <source>Document Class</source>
        <translation type="obsolete">Класс документа</translation>
    </message>
    <message>
        <source>Typeface Size</source>
        <translation type="obsolete">Размер шрифта</translation>
    </message>
    <message>
        <source>Paper Size</source>
        <translation type="obsolete">Размер бумаги</translation>
    </message>
    <message>
        <source>Encoding</source>
        <translation type="obsolete">Кодировка</translation>
    </message>
    <message>
        <source>AMS Packages</source>
        <translation type="obsolete">Пакеты AMS</translation>
    </message>
    <message>
        <source>makeidx Package</source>
        <translation type="obsolete">Пакеты makeidx</translation>
    </message>
    <message>
        <source>Other Options</source>
        <translation type="obsolete">Другие настройки</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="obsolete">Автор</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="obsolete">Заголовок</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>refdialog</name>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>structdialog</name>
    <message>
        <source>Title</source>
        <translation type="obsolete">Заголовок</translation>
    </message>
    <message>
        <source>Numeration</source>
        <translation type="obsolete">Перечисление</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>tabbingdialog</name>
    <message>
        <source>Num of Columns</source>
        <translation type="obsolete">Кол-во колонок</translation>
    </message>
    <message>
        <source>Num of Rows</source>
        <translation type="obsolete">Кол-во столбцов</translation>
    </message>
    <message>
        <source>Spacing</source>
        <translation type="obsolete">Промежутки</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>tabdialog</name>
    <message>
        <source>Num of Rows</source>
        <translation type="obsolete">Кол-во столбцов</translation>
    </message>
    <message>
        <source>Num of Columns</source>
        <translation type="obsolete">Кол-во колонок</translation>
    </message>
    <message>
        <source>Columns Alignment</source>
        <translation type="obsolete">Выравнивание в колонке</translation>
    </message>
    <message>
        <source>Vertical Separator</source>
        <translation type="obsolete">Вертикальный разделитель</translation>
    </message>
    <message>
        <source>Horizontal Separator</source>
        <translation type="obsolete">Горизонтальный разделитель</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">По центру</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="obsolete">Влево</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="obsolete">Вправо</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="obsolete">нет</translation>
    </message>
</context>
<context>
    <name>toolsoptionsdialog</name>
    <message>
        <source>Configure Texmaker</source>
        <translation type="obsolete">Настроить Texmaker</translation>
    </message>
    <message>
        <source>Quick Build Command</source>
        <translation type="obsolete">Команда быстрой сборки</translation>
    </message>
    <message>
        <source>Commands (% : filename without extension)</source>
        <translation type="obsolete">Команды (%s : имя файла без расширения)</translation>
    </message>
    <message>
        <source>PS Viewer</source>
        <translation type="obsolete">Просмотр PS</translation>
    </message>
    <message>
        <source>Dvi Viewer</source>
        <translation type="obsolete">Просмотр DVI</translation>
    </message>
    <message>
        <source>Pdf Viewer</source>
        <translation type="obsolete">Просмотр PDF</translation>
    </message>
    <message>
        <source>Commands</source>
        <translation type="obsolete">Команды</translation>
    </message>
    <message>
        <source>Editor Font Family</source>
        <translation type="obsolete">Стиль шрифта</translation>
    </message>
    <message>
        <source>Editor Font Size</source>
        <translation type="obsolete">Размер шрифта</translation>
    </message>
    <message>
        <source>Editor Font Encoding</source>
        <translation type="obsolete">Кодировка</translation>
    </message>
    <message>
        <source>Word Wrap</source>
        <translation type="obsolete">Перенос по словам</translation>
    </message>
    <message>
        <source>Show Line Numbers</source>
        <translation type="obsolete">Показывать номера строк</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation type="obsolete">Редактор</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>usermenudialog</name>
    <message>
        <source>Menu Item</source>
        <translation type="obsolete">Элемент меню</translation>
    </message>
    <message>
        <source>LaTeX Content</source>
        <translation type="obsolete">Содержимое LaTeX</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>usertooldialog</name>
    <message>
        <source>Menu Item</source>
        <translation type="obsolete">Элемент меню</translation>
    </message>
    <message>
        <source>Command (% : filename without extension)</source>
        <translation type="obsolete">Команда (%s : имя файла без расширения)</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
</TS>
