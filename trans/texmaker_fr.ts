<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.ui" line="14"/>
        <source>About Texmaker</source>
        <translation>À propos de Texmaker</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="76"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="86"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="96"/>
        <source>Thanks</source>
        <translation>Remerciements</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="106"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>AddOptionDialog</name>
    <message>
        <location filename="../addoptiondialog.ui" line="14"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
</context>
<context>
    <name>AddTagDialog</name>
    <message>
        <location filename="../addtagdialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>Editer balises</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="22"/>
        <source>Item</source>
        <translation>Item</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="32"/>
        <source>LaTeX Content</source>
        <translation>Code LaTeX</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="55"/>
        <source>Keyboard Trigger</source>
        <translation>Déclencheur clavier</translation>
    </message>
</context>
<context>
    <name>ArrayDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="45"/>
        <source>Num of Columns</source>
        <translation>Nb de colonnes</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="59"/>
        <source>Columns Alignment</source>
        <translation>Alignement Colonnes</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="76"/>
        <source>Environment</source>
        <translation>Environnement</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="35"/>
        <source>Num of Rows</source>
        <translation>Nb Lignes</translation>
    </message>
    <message>
        <location filename="../arraydialog.cpp" line="43"/>
        <source>Quick Array</source>
        <translation>Assistant Matrice</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../browser.cpp" line="78"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="79"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="81"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="89"/>
        <source>Index</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="107"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="53"/>
        <source>Get more dictionaries at: %1</source>
        <translation>Plus de dictionnaires disponibles ici: %1</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="174"/>
        <location filename="../configdialog.cpp" line="197"/>
        <location filename="../configdialog.cpp" line="198"/>
        <source>Commands</source>
        <translation>Commandes</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="181"/>
        <location filename="../configdialog.cpp" line="199"/>
        <source>Quick Build</source>
        <translation>Compil rapide</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>svn command directory</source>
        <translation>répertoire de l&apos;exécutable svn</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1140"/>
        <location filename="../configdialog.cpp" line="187"/>
        <location filename="../configdialog.cpp" line="200"/>
        <source>Editor</source>
        <translation>Editeur</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="14"/>
        <source>Configure Texmaker</source>
        <translation>Configurer Texmaker</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <location filename="../configdialog.cpp" line="391"/>
        <location filename="../configdialog.cpp" line="402"/>
        <location filename="../configdialog.cpp" line="414"/>
        <location filename="../configdialog.cpp" line="425"/>
        <location filename="../configdialog.cpp" line="436"/>
        <location filename="../configdialog.cpp" line="447"/>
        <location filename="../configdialog.cpp" line="458"/>
        <location filename="../configdialog.cpp" line="469"/>
        <location filename="../configdialog.cpp" line="480"/>
        <location filename="../configdialog.cpp" line="491"/>
        <location filename="../configdialog.cpp" line="502"/>
        <location filename="../configdialog.cpp" line="513"/>
        <location filename="../configdialog.cpp" line="524"/>
        <location filename="../configdialog.cpp" line="535"/>
        <location filename="../configdialog.cpp" line="546"/>
        <source>Browse program</source>
        <translation>Chercher programme</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="130"/>
        <source>Commands (% : filename without extension - @ : line number)</source>
        <translation>Commandes (% : nom fichier sans extension - @ : numéro ligne)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="755"/>
        <location filename="../configdialog.cpp" line="619"/>
        <location filename="../configdialog.cpp" line="682"/>
        <source>Dvi Viewer</source>
        <translation>Afficheur Dvi</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="820"/>
        <location filename="../configdialog.cpp" line="622"/>
        <location filename="../configdialog.cpp" line="685"/>
        <source>PS Viewer</source>
        <translation>Afficheur Ps</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="278"/>
        <location filename="../configdialog.cpp" line="643"/>
        <location filename="../configdialog.cpp" line="706"/>
        <source>ghostscript</source>
        <translation>ghostscript</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="631"/>
        <location filename="../configdialog.cpp" line="694"/>
        <source>Bibtex</source>
        <translation>Bibtex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="383"/>
        <location filename="../configdialog.cpp" line="625"/>
        <location filename="../configdialog.cpp" line="688"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="155"/>
        <location filename="../configdialog.cpp" line="613"/>
        <location filename="../configdialog.cpp" line="676"/>
        <source>PdfLaTeX</source>
        <translation>PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="366"/>
        <location filename="../configdialog.cpp" line="634"/>
        <location filename="../configdialog.cpp" line="697"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="399"/>
        <location filename="../configdialog.cpp" line="628"/>
        <location filename="../configdialog.cpp" line="691"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="600"/>
        <location filename="../configdialog.cpp" line="637"/>
        <location filename="../configdialog.cpp" line="700"/>
        <source>Pdf Viewer</source>
        <translation>Afficheur Pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="309"/>
        <location filename="../configdialog.cpp" line="640"/>
        <location filename="../configdialog.cpp" line="703"/>
        <source>metapost</source>
        <translation>metapost</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="141"/>
        <location filename="../configdialog.cpp" line="610"/>
        <location filename="../configdialog.cpp" line="673"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="376"/>
        <location filename="../configdialog.cpp" line="616"/>
        <location filename="../configdialog.cpp" line="679"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <source>Built-in Viewer</source>
        <translation type="vanished">Afficheur Pdf interne</translation>
    </message>
    <message>
        <source>External Viewer</source>
        <translation type="vanished">Afficheur Pdf externe</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="271"/>
        <location filename="../configdialog.cpp" line="646"/>
        <location filename="../configdialog.cpp" line="709"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="319"/>
        <location filename="../configdialog.cpp" line="649"/>
        <location filename="../configdialog.cpp" line="712"/>
        <source>Latexmk</source>
        <translation>Latexmk</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="629"/>
        <source>Embed</source>
        <translation>Intégré à la fenêtre</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="446"/>
        <location filename="../configdialog.cpp" line="652"/>
        <location filename="../configdialog.cpp" line="715"/>
        <source>R Sweave</source>
        <translation>R.Sweave</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="247"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &amp;quot;--output-directory=build&amp;quot; option will be automatically added to the (pdf)latex command while the compilation.&lt;/p&gt;&lt;p&gt;For the others commands like dvips, ps2pdf, bibtex,... you will have to manually replaced &amp;quot;%&amp;quot; by &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;L&apos;option &amp;quot;--output-directory=build&amp;quot; sera automatiquement ajouté à la commande (pdf)latex pour la compilation.&lt;/p&gt;&lt;p&gt;Pour les autres commandes (dvips, ps2pdf, bibtex,...) il faut remplacer manuellement &amp;quot;%&amp;quot; par &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="250"/>
        <source>Use a &quot;build&quot; subdirectory for output files</source>
        <translation>Utiliser un sous-répertoire &quot;build&quot; pour les fichiers de sortie</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="148"/>
        <location filename="../configdialog.cpp" line="655"/>
        <location filename="../configdialog.cpp" line="718"/>
        <source>XeLaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="285"/>
        <source>Add to PATH</source>
        <translation>Ajouter au PATH</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="704"/>
        <source>lp options for the printer</source>
        <translation>Options pour lp (impression)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="896"/>
        <source>Quick Build Command</source>
        <translation>Commande compilation rapide</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="918"/>
        <source>LaTeX + dvips + View PS</source>
        <translation>LaTeX + dvips + Voir PS</translation>
    </message>
    <message>
        <source>LaTeX + View DVI</source>
        <translation type="vanished">LaTeX + Voir DVI</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="904"/>
        <source>PdfLaTeX + View PDF</source>
        <translation>PdfLaTeX + Voir PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="946"/>
        <source>LaTeX + dvipdfm + View PDF</source>
        <translation>LaTeX + dvipdfm + Voir PDF</translation>
    </message>
    <message>
        <source>LaTeX + dvips + ps2pdf + View PDF</source>
        <translation type="vanished">LaTeX + dvips + ps2pdf + Voir PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="183"/>
        <location filename="../configdialog.cpp" line="658"/>
        <location filename="../configdialog.cpp" line="721"/>
        <source>LuaLaTeX</source>
        <translation>LuaLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="356"/>
        <source>Bib(la)tex</source>
        <translation>Bib(la)tex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="559"/>
        <source>Launch the &quot;Clean&quot; tool when exiting Texmaker</source>
        <translation>Nettoyer les fichiers auxiliaires en quittant</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="925"/>
        <source>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + View Pdf</source>
        <translation>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + Voir Pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="953"/>
        <source>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + View Pdf</source>
        <translation>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + Voir Pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="960"/>
        <source>Sweave + PdfLaTeX + View Pdf</source>
        <translation>Sweave + PdfLaTeX + Voir Pdf</translation>
    </message>
    <message>
        <source>LaTeX + Asymptote + LaTeX + dvips + View PS</source>
        <oldsource>LaTeX + Asymptote + LaTeX + View PS</oldsource>
        <translation type="vanished">LaTeX + Asymptote + LaTeX + dvips + Voir PS</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="974"/>
        <source>PdfLaTeX + Asymptote + PdfLaTeX + View Pdf</source>
        <translation>PdfLaTeX + Asymptote + PdfLaTeX + Voir Pdf</translation>
    </message>
    <message>
        <source>LatexMk + View PDF</source>
        <translation type="vanished">LatexMk + Voir PDF</translation>
    </message>
    <message>
        <source>XeLaTeX + View PDF</source>
        <translation type="vanished">XeLaTeX + Voir PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="995"/>
        <source>LuaLaTeX + View PDF</source>
        <translation>LuaLaTeX + Voir PDF</translation>
    </message>
    <message>
        <source>User : (% : filename without extension)</source>
        <translation type="vanished">Utilisateur : (% nom fichier sans extension)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1030"/>
        <location filename="../configdialog.ui" line="1069"/>
        <source>wizard</source>
        <translation>assistant</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1043"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(les commandes sont séparées par &apos;|&apos;)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1059"/>
        <source>For .asy files</source>
        <translation>Pour les fichiers .asy</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1183"/>
        <source>Item</source>
        <translation>Item</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1188"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1160"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1146"/>
        <source>Default Theme</source>
        <translation>Thème par défaut</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1209"/>
        <source>Dark theme</source>
        <translation>Thème sombre</translation>
    </message>
    <message>
        <source>Aspell Lang</source>
        <translation type="obsolete">Langue Aspell</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1338"/>
        <source>Show Line Numbers</source>
        <translation>Numéros de ligne</translation>
    </message>
    <message>
        <source>Click here to set this color</source>
        <translation type="obsolete">Cliquer ici pour définir la couleur</translation>
    </message>
    <message>
        <source>&quot;Keyword&quot; color</source>
        <translation type="obsolete">Couleur &quot;Mots clés&quot;</translation>
    </message>
    <message>
        <source>Aspell Command</source>
        <translation type="obsolete">Commande Aspell</translation>
    </message>
    <message>
        <source>Aspell Encoding</source>
        <translation type="obsolete">Encodage Aspell</translation>
    </message>
    <message>
        <source>&quot;Command&quot; color</source>
        <translation type="obsolete">Couleur &quot;Commande&quot;</translation>
    </message>
    <message>
        <source>&quot;Math&quot; color</source>
        <translation type="obsolete">Couleur &quot;Math&quot;</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1324"/>
        <source>Word Wrap</source>
        <translation>Retour à la ligne</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1269"/>
        <source>Editor Font Family</source>
        <translation>Police</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="616"/>
        <source>Built-in &amp;Viewer</source>
        <translation>Afficheur Pdf interne</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="649"/>
        <source>E&amp;xternal Viewer</source>
        <translation>Afficheur externe</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="932"/>
        <source>LaTeX + &amp;View DVI</source>
        <translation>LaTeX + Voir DVI</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="939"/>
        <source>LaTeX + dvips + ps&amp;2pdf + View PDF</source>
        <translation>LaTeX + dvips + ps2pdf + Voir PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="967"/>
        <source>LaTeX + As&amp;ymptote + LaTeX + dvips + View PS</source>
        <translation>LaTeX + Asymptote + LaTeX + dvips + Voir PS</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="981"/>
        <source>Latex&amp;Mk + View PDF</source>
        <translation>LatexMk + Voir PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="988"/>
        <source>&amp;XeLaTeX + View PDF</source>
        <translation>XeLaTeX + Voir PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1011"/>
        <source>User : (% : filename &amp;without extension)</source>
        <translation>Utilisateur : (% nom fichier sans extension)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1107"/>
        <source>Don&apos;t launch a new instance of the viewer if the dvi/ps/pdf file is already opened</source>
        <translation>Ne pas lancer une nouvelle instance de l&apos;afficheur si le fichier dvi/ps/pdf est déjà ouvert</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1283"/>
        <source>Editor Font Size</source>
        <translation>Taille police</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1300"/>
        <source>Editor Font Encoding</source>
        <translation>Encodage</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1310"/>
        <source>Check for external changes</source>
        <translation>Détecter modifications externes</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1317"/>
        <source>Backup documents every 10 min</source>
        <translation>Sauvegarder documents toutes les 10 min</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1232"/>
        <source>Tab width (num of spaces)</source>
        <translation>Largeur tabulation (nb d&apos;espaces)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1153"/>
        <source>Replace tab with spaces</source>
        <translation>Remplacer tabulation par des espaces</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1444"/>
        <source>Svn</source>
        <translation>Svn</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1450"/>
        <source>Detect uncommitted lines at the opening</source>
        <translation>Détecter les lignes non mises à jour depuis le dernier commit à l&apos;ouverture</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1457"/>
        <source>Path to the svn command</source>
        <translation>Chemin vers l&apos;exécutbale svn</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1485"/>
        <location filename="../configdialog.cpp" line="193"/>
        <location filename="../configdialog.cpp" line="201"/>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1521"/>
        <source>Toggle focus editor/pdf viewer</source>
        <translation>Basculer focus éditeur/afficheur pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1528"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1495"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1500"/>
        <source>Shortcut</source>
        <translation>Raccourci</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1362"/>
        <source>Spelling dictionary</source>
        <translation>Dictionnaire</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1331"/>
        <source>Completion</source>
        <translation>Complétion</translation>
    </message>
    <message>
        <source>Get dictionary at: %1</source>
        <translation type="obsolete">Téléchargement dictionnaire: %1</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="245"/>
        <source>Browse dictionary</source>
        <translation>Sélectionner dictionnaire</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1396"/>
        <source>Inline Spell Checking</source>
        <translation>Vérification orthographique à la volée</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1399"/>
        <source>Inline</source>
        <translation>A la volée</translation>
    </message>
</context>
<context>
    <name>DocumentView</name>
    <message>
        <source>Unlock %1</source>
        <translation type="vanished">Déverouiller %1</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="vanished">Mot de passe:</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="513"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation>Impression de &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1052"/>
        <source>Loading pdf...</source>
        <translation>Chargement du pdf...</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1358"/>
        <source>Cancel</source>
        <translation>Abandon</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1374"/>
        <source>Number of words in the document</source>
        <translation>Nombre de mots dans le document</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1384"/>
        <source>Save As</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1419"/>
        <source>Language:</source>
        <translation>Langue:</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1473"/>
        <source>Number of words in the page</source>
        <translation>Nombre de mots dans la page</translation>
    </message>
</context>
<context>
    <name>EncodingDialog</name>
    <message>
        <location filename="../encodingdialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="20"/>
        <source>It seems that this file cannot be correctly decoded
with the default encoding setting</source>
        <translation>Il semble que ce document ne puisse pas être décodé correctement
avec l&apos;encodage défini par défaut</translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="31"/>
        <source>Use this encoding :</source>
        <translation>Utiliser cet encodage :</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../exportdialog.ui" line="14"/>
        <source>Export via TeX4ht</source>
        <translation>Exporter via TeX4ht</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="22"/>
        <source>Path to htlatex</source>
        <translation>Chemin vers htlatex</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="45"/>
        <source>Mode</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="51"/>
        <source>export to Html</source>
        <translation>exporter en html</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="61"/>
        <source>export to Html (new page for each section)</source>
        <translation>exporter en html (une page par section)</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="68"/>
        <source>export to OpenDocumentFormat</source>
        <translation>exporter au format OpenDocument</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="75"/>
        <source>export to MathML</source>
        <translation>exporter en MathML</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="82"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="91"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="125"/>
        <source>Run</source>
        <translation>Exécuter</translation>
    </message>
    <message>
        <location filename="../exportdialog.cpp" line="49"/>
        <source>Browse program</source>
        <translation>Chercher programme</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../filechooser.ui" line="31"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../filechooser.cpp" line="44"/>
        <source>Select a File</source>
        <translation>Sélectionner un fichier</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <source>Find Text</source>
        <translation type="obsolete">Chercher Texte</translation>
    </message>
    <message>
        <source>Find</source>
        <translation type="obsolete">Chercher</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Options</translation>
    </message>
    <message>
        <source>Whole words only</source>
        <translation type="obsolete">Mots entiers</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation type="obsolete">Sensible à la casse</translation>
    </message>
    <message>
        <source>Start at Beginning</source>
        <translation type="obsolete">Commencer au début</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="obsolete">Direction</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">En avant</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="obsolete">En arrière</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <location filename="../findwidget.ui" line="23"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="47"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="57"/>
        <source>Forward</source>
        <translation>En avant</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="67"/>
        <source>Backward</source>
        <translation>En arrière</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="92"/>
        <source>Whole words only</source>
        <translation>Mots entiers</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="99"/>
        <source>Case sensitive</source>
        <translation>Sensible à la casse</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="106"/>
        <source>Start at Beginning</source>
        <translation>Commencer au début</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="163"/>
        <source>Regular Expression</source>
        <translation>Expression régulière</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="170"/>
        <location filename="../findwidget.cpp" line="158"/>
        <source>Text must be selected before checking this option.</source>
        <translation>Le texte doit être sélectionné avant de cocher cette option.</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="173"/>
        <source>In selection only</source>
        <translation>Dans sélection uniquement</translation>
    </message>
    <message>
        <source>RegularExpression</source>
        <translation type="obsolete">Expression régulière</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Invalid regular expression.</source>
        <translation>Expression régulière non valide.</translation>
    </message>
</context>
<context>
    <name>GotoLineDialog</name>
    <message>
        <source>Goto Line</source>
        <translation type="obsolete">Aller à la ligne</translation>
    </message>
    <message>
        <source>Line</source>
        <translation type="obsolete">Ligne</translation>
    </message>
    <message>
        <source>Goto</source>
        <translation type="obsolete">Aller</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
</context>
<context>
    <name>GotoLineWidget</name>
    <message>
        <location filename="../gotolinewidget.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="59"/>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="37"/>
        <source>Goto</source>
        <translation>Aller</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Fermer</translation>
    </message>
</context>
<context>
    <name>GraphicFileChooser</name>
    <message>
        <location filename="../graphicfilechooser.ui" line="36"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="95"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="221"/>
        <source>Use &quot;figure&quot; environment</source>
        <translation>Utiliser l&apos;environnement &quot;figure&quot;</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="152"/>
        <source>Caption</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="176"/>
        <source>Above</source>
        <translation>Au dessus</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="181"/>
        <source>Below</source>
        <translation>En dessous</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="195"/>
        <source>Placement</source>
        <translation>Placement</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="208"/>
        <source>hbtp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="233"/>
        <source>Center</source>
        <translation>Centrer</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.cpp" line="49"/>
        <source>Select a File</source>
        <translation>Sélectionner un fichier</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <source>Home</source>
        <translation type="obsolete">Index</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="obsolete">Précédent</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Suivant</translation>
    </message>
</context>
<context>
    <name>KeySequenceDialog</name>
    <message>
        <location filename="../keysequencedialog.ui" line="37"/>
        <location filename="../keysequencedialog.cpp" line="43"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../keysequencedialog.ui" line="14"/>
        <source>Shortcut</source>
        <translation>Raccourci</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
</context>
<context>
    <name>LatexEditor</name>
    <message>
        <location filename="../latexeditor.cpp" line="914"/>
        <source>Undo</source>
        <translation>Défaire</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="917"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="921"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="924"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="927"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="932"/>
        <source>Select All</source>
        <translation>Sélectionner tout</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="938"/>
        <source>Check Spelling Word</source>
        <translation>Vérifier l&apos;orthographe du mot</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="943"/>
        <source>Check Spelling Selection</source>
        <translation>Vérifier l&apos;orthographe de la sélection</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="946"/>
        <source>Check Spelling Document</source>
        <translation>Vérifier l&apos;orthographe du document</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="953"/>
        <source>Jump to the end of this block</source>
        <translation>Aller à la fin du bloc</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="959"/>
        <source>Jump to pdf</source>
        <translation>Aller au document Pdf</translation>
    </message>
    <message>
        <source>Add LRM</source>
        <translation type="obsolete">Ajouter LRM</translation>
    </message>
    <message>
        <source>Remove LRM from Selection</source>
        <translation type="obsolete">Supprimer LRM de la sélection</translation>
    </message>
</context>
<context>
    <name>LetterDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="40"/>
        <source>Typeface Size</source>
        <translation>Taille police</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="53"/>
        <source>Encoding</source>
        <translation>Encodage</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="60"/>
        <source>AMS Packages</source>
        <translation>Paquets AMS</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="79"/>
        <source>Paper Size</source>
        <translation>Format papier</translation>
    </message>
    <message>
        <location filename="../letterdialog.cpp" line="56"/>
        <source>Quick Letter</source>
        <translation>Assistant lettre</translation>
    </message>
</context>
<context>
    <name>LightFindWidget</name>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Invalid regular expression.</source>
        <translation>Expression régulière non valide.</translation>
    </message>
</context>
<context>
    <name>LightLatexEditor</name>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>You do not have read permission to this file.</source>
        <translation>Vous n&apos;avez pas les droits nécessaires pour lire ce fichier.</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="900"/>
        <location filename="../lightlatexeditor.cpp" line="903"/>
        <location filename="../lightlatexeditor.cpp" line="906"/>
        <source>Click to jump to the bookmark</source>
        <translation>Cliquer pour atteindre le signet</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="910"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="913"/>
        <source>Goto Line</source>
        <translation>Aller à la ligne</translation>
    </message>
</context>
<context>
    <name>LightLineNumberWidget</name>
    <message>
        <location filename="../lightlinenumberwidget.cpp" line="181"/>
        <source>Click to add or remove a bookmark</source>
        <translation>Cliquer pour ajouter ou supprimer un signet</translation>
    </message>
</context>
<context>
    <name>LineNumberWidget</name>
    <message>
        <location filename="../linenumberwidget.cpp" line="286"/>
        <source>Click to add or remove a bookmark</source>
        <translation>Cliquer pour ajouter ou supprimer un signet</translation>
    </message>
</context>
<context>
    <name>LogEditor</name>
    <message>
        <source>Click to jump to the line</source>
        <translation type="obsolete">Cliquer pour aller à la ligne correspondante</translation>
    </message>
</context>
<context>
    <name>PageItem</name>
    <message>
        <location filename="../pageitem.cpp" line="137"/>
        <location filename="../pageitem.cpp" line="139"/>
        <source>Click to jump to the line</source>
        <translation>Cliquer pour aller à la ligne correspondante</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="146"/>
        <source>Number of words in the document</source>
        <translation>Nombre de mots dans le document</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="150"/>
        <source>Number of words in the page</source>
        <translation>Nombre de mots dans la page</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="154"/>
        <source>Convert page to png image</source>
        <translation>Convertir page en image png</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="158"/>
        <source>Check Spelling and Grammar on this page</source>
        <translation>Analyser orthographe/grammaire de cette page</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="162"/>
        <source>Open the file browser</source>
        <translation>Ouvrir le gestionnaire de fichiers</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="547"/>
        <source>Copy text</source>
        <translation>Copier texte</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="548"/>
        <source>Copy image</source>
        <translation>Copier image</translation>
    </message>
</context>
<context>
    <name>PaperDialog</name>
    <message>
        <source>Print</source>
        <translation type="obsolete">Imprimer</translation>
    </message>
    <message>
        <source>Paper Size</source>
        <translation type="obsolete">Format papier</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="obsolete">Plage d&apos;impression</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="obsolete">Tout imprimer</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="obsolete">Pages de</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">à</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
</context>
<context>
    <name>PdfDocumentWidget</name>
    <message>
        <source>Click to jump to the line</source>
        <translation type="obsolete">Cliquer pour aller à la ligne correspondante</translation>
    </message>
    <message>
        <source>Number of words in the document</source>
        <translation type="obsolete">Nombre de mots dans le document</translation>
    </message>
    <message>
        <source>Convert page to png image</source>
        <translation type="obsolete">Convertir page en image png</translation>
    </message>
</context>
<context>
    <name>PdfViewer</name>
    <message>
        <location filename="../pdfviewer.cpp" line="134"/>
        <location filename="../pdfviewer.cpp" line="1117"/>
        <source>Pages</source>
        <translation>Pages</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="184"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="110"/>
        <location filename="../pdfviewer.cpp" line="153"/>
        <location filename="../pdfviewer.cpp" line="1110"/>
        <source>Structure</source>
        <translation>Structure</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="188"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="190"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="201"/>
        <source>Previous</source>
        <translation>Page précédente</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="205"/>
        <source>Next</source>
        <translation>Page suivante</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="211"/>
        <source>&amp;View</source>
        <translation>Afficha&amp;ge</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="212"/>
        <source>Fit Width</source>
        <translation>Adapter à la largeur</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="215"/>
        <source>Fit Page</source>
        <translation>Adapter à la page</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="218"/>
        <source>Zoom In</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="223"/>
        <source>Zoom Out</source>
        <translation>Zoom arrière</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="228"/>
        <source>Continuous</source>
        <translation>Mode continu</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="233"/>
        <source>Two pages</source>
        <translation>Deux pages</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="238"/>
        <source>Rotate left</source>
        <translation>Rotation gauche</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="242"/>
        <source>Rotate right</source>
        <translation>Rotation droite</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="246"/>
        <source>Presentation...</source>
        <translation>Mode présentation...</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="255"/>
        <source>Previous Position</source>
        <translation>Position Précédente</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="260"/>
        <source>Next Position</source>
        <translation>Position Suivante</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">Analyser orthographe/grammaire de cette page</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <source>Number of words in the document</source>
        <translation type="obsolete">Nombre de mots dans le document</translation>
    </message>
    <message>
        <source>Can&apos;t print : the ghostscript command (gswin32c.exe) was not found on your system.</source>
        <translation type="obsolete">Impossible d&apos;imprimer : ghostscript n&apos;a pas été trouvé.</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="191"/>
        <location filename="../pdfviewer.cpp" line="288"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="299"/>
        <location filename="../pdfviewer.cpp" line="992"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="302"/>
        <source>External Viewer</source>
        <translation>Afficheur externe</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="441"/>
        <location filename="../pdfviewer.cpp" line="669"/>
        <source>Page</source>
        <translation>Page</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>File not found</source>
        <translation>Fichier non trouvé</translation>
    </message>
</context>
<context>
    <name>PdfViewerWidget</name>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="104"/>
        <source>Show/Hide Table of contents</source>
        <translation>Montrer/Cacher la table des matières</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="110"/>
        <source>Previous</source>
        <translation>Page précédente</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="114"/>
        <source>Next</source>
        <translation>Page suivante</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="120"/>
        <source>Fit Width</source>
        <translation>Adapter à la largeur</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="123"/>
        <source>Fit Page</source>
        <translation>Adapter à la page</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="126"/>
        <source>Zoom In</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="131"/>
        <source>Zoom Out</source>
        <translation>Zoom arrière</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="144"/>
        <source>Continuous</source>
        <translation>Mode continu</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="149"/>
        <source>Two pages</source>
        <translation>Deux pages</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="154"/>
        <source>Rotate left</source>
        <translation>Rotation gauche</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="158"/>
        <source>Rotate right</source>
        <translation>Rotation droite</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="162"/>
        <source>Presentation...</source>
        <translation>Mode présentation...</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="195"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="206"/>
        <source>Previous Position</source>
        <translation>Position Précédente</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="211"/>
        <source>Next Position</source>
        <translation>Position Suivante</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="220"/>
        <location filename="../pdfviewerwidget.cpp" line="928"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="223"/>
        <source>External Viewer</source>
        <translation>Afficheur externe</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">Analyser orthographe/grammaire de cette page</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>File not found</source>
        <translation>Fichier non trouvé</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="obsolete">Page</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <source>Number of words in the document</source>
        <translation type="obsolete">Nombre de mots dans le document</translation>
    </message>
    <message>
        <source>Can&apos;t print : the ghostscript command (gswin32c.exe) was not found on your system.</source>
        <translation type="obsolete">Impossible d&apos;imprimer : ghostscript n&apos;a pas été trouvé.</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Enregistrer sous</translation>
    </message>
</context>
<context>
    <name>QuickBeamerDialog</name>
    <message>
        <location filename="../quickbeamerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="22"/>
        <source>AMS Packages</source>
        <translation>Paquets AMS</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="29"/>
        <source>Encoding</source>
        <translation>Encodage</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="39"/>
        <source>Typeface Size</source>
        <translation>Taille police</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="46"/>
        <source>babel Package</source>
        <translation>Paquet babel</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="59"/>
        <source>graphicx Package</source>
        <translation>Paquet graphicx</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="66"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="79"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="86"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.cpp" line="23"/>
        <source>Quick Start</source>
        <translation>Assistant nouveau document</translation>
    </message>
</context>
<context>
    <name>QuickDocumentDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="157"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="41"/>
        <location filename="../quickdocumentdialog.ui" line="70"/>
        <location filename="../quickdocumentdialog.ui" line="115"/>
        <location filename="../quickdocumentdialog.ui" line="144"/>
        <location filename="../quickdocumentdialog.ui" line="199"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="25"/>
        <source>Document Class</source>
        <translation>Classe document</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="173"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="183"/>
        <source>babel Package</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="206"/>
        <source>geometry Package</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="213"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="220"/>
        <source>AMS Packages</source>
        <translation>Paquets AMS</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="237"/>
        <source>graphicx Package</source>
        <translation>Paquet graphicx</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="244"/>
        <source>lmodern Package</source>
        <translation>Paquet lmodern</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="251"/>
        <source>Kpfonts Package</source>
        <translation>Paquet Kpfonts</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="258"/>
        <source>Fourier Package</source>
        <translation>Paquet Fourier</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="83"/>
        <source>Typeface Size</source>
        <translation>Taille police</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="99"/>
        <source>Paper Size</source>
        <translation>Format papier</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="54"/>
        <source>Other Options</source>
        <translation>Autres options</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="128"/>
        <source>Encoding</source>
        <translation>Encodage</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="230"/>
        <source>makeidx Package</source>
        <translation>Paquet makeidx</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.cpp" line="32"/>
        <source>Quick Start</source>
        <translation>Assistant nouveau document</translation>
    </message>
</context>
<context>
    <name>QuickXelatexDialog</name>
    <message>
        <location filename="../quickxelatexdialog.ui" line="25"/>
        <location filename="../quickxelatexdialog.ui" line="83"/>
        <location filename="../quickxelatexdialog.ui" line="115"/>
        <location filename="../quickxelatexdialog.ui" line="170"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="38"/>
        <source>Other Options</source>
        <translation>Autres options</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="51"/>
        <source>Document Class</source>
        <translation>Classe document</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="70"/>
        <source>Paper Size</source>
        <translation>Format papier</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="96"/>
        <source>Typeface Size</source>
        <translation>Taille police</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="128"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="147"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="154"/>
        <source>polyglossia Package</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="177"/>
        <source>geometry Package</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="184"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="191"/>
        <source>AMS Packages</source>
        <translation>Paquets AMS</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="201"/>
        <source>graphicx Package</source>
        <translation>Paquet graphicx</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.cpp" line="31"/>
        <source>Quick Start</source>
        <translation>Assistant nouveau document</translation>
    </message>
</context>
<context>
    <name>RefDialog</name>
    <message>
        <location filename="../refdialog.ui" line="14"/>
        <location filename="../refdialog.ui" line="40"/>
        <source>Labels</source>
        <translation>Labels</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
</context>
<context>
    <name>ReplaceDialog</name>
    <message>
        <source>Replace Text</source>
        <translation type="obsolete">Remplacer Texte</translation>
    </message>
    <message>
        <source>Find</source>
        <translation type="obsolete">Chercher</translation>
    </message>
    <message>
        <source>Replace All</source>
        <translation type="obsolete">Remplacer Tout</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation type="obsolete">Remplacer</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Options</translation>
    </message>
    <message>
        <source>Whole words only</source>
        <translation type="obsolete">Mots entiers</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation type="obsolete">Sensible à la casse</translation>
    </message>
    <message>
        <source>Start at Beginning</source>
        <translation type="obsolete">Commencer au début</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="obsolete">Direction</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">En avant</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="obsolete">En arrière</translation>
    </message>
    <message>
        <source>Replace this occurrence ? </source>
        <translation type="obsolete">Remplacer cette occurrence ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
</context>
<context>
    <name>ReplaceWidget</name>
    <message>
        <location filename="../replacewidget.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="34"/>
        <location filename="../replacewidget.ui" line="60"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="70"/>
        <source>Forward</source>
        <translation>En avant</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <source>RegularExpression</source>
        <translation type="obsolete">Expression régulière</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="130"/>
        <source>Backward</source>
        <translation>En arrière</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="97"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="123"/>
        <source>Replace All</source>
        <translation>Remplacer Tout</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="158"/>
        <source>Whole words only</source>
        <translation>Mots entiers</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="165"/>
        <source>Case sensitive</source>
        <translation>Sensible à la casse</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="172"/>
        <source>Start at Beginning</source>
        <translation>Commencer au début</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="215"/>
        <source>Regular Expression</source>
        <translation>Expression régulière</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="222"/>
        <location filename="../replacewidget.cpp" line="251"/>
        <source>Text must be selected before checking this option.</source>
        <translation>Le texte doit être sélectionné avant de cocher cette option.</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="225"/>
        <source>In selection only</source>
        <translation>Dans sélection uniquement</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Invalid regular expression.</source>
        <translation>Expression régulière non valide.</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Replace this occurrence ? </source>
        <translation>Remplacer cette occurence ?</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Cancel</source>
        <translation>Abandon</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../scandialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="20"/>
        <source>In directory :</source>
        <translation>Dans le dossier :</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="57"/>
        <source>Text :</source>
        <translation>Texte :</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="67"/>
        <location filename="../scandialog.cpp" line="43"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="74"/>
        <source>Include subdirectories</source>
        <translation>Inclure sous dossiers</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="81"/>
        <source>Abort</source>
        <translation>Abandon</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="100"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>SourceView</name>
    <message>
        <location filename="../sourceview.cpp" line="50"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="61"/>
        <source>Check differences</source>
        <translation>Rechercher les différences</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="167"/>
        <source>Open File</source>
        <translation>Ouvrir fichier</translation>
    </message>
</context>
<context>
    <name>SpellerDialog</name>
    <message>
        <location filename="../spellerdialog.ui" line="13"/>
        <source>Check Spelling</source>
        <translation>Vérifier orthographe</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="80"/>
        <source>Suggested words :</source>
        <translation>Mots suggérés :</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="33"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="19"/>
        <source>Unknown word:</source>
        <translation>Mot inconnu:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="130"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="43"/>
        <source>Replace with:</source>
        <translation>Remplacer par:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="53"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>Could not start the command.</source>
        <translation type="obsolete">Impossible de lancer la commande.</translation>
    </message>
    <message>
        <source>Scanning document...</source>
        <translation type="obsolete">Analyse du document...</translation>
    </message>
    <message>
        <source>No misspelled words were found.</source>
        <translation type="obsolete">Aucun mot inconnu n&apos;a été détecté.</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="73"/>
        <source>Always ignore</source>
        <translation>Toujours ignorer</translation>
    </message>
    <message>
        <source>Error : Can&apos;t open the dictionary</source>
        <translation type="obsolete">Erreur : impossible d&apos;ouvrir le dictionnaire</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="121"/>
        <source>Check spelling selection...</source>
        <translation>Vérifier l&apos;orthographe de la sélection...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="131"/>
        <source>Check spelling from cursor...</source>
        <translation>Vérifier l&apos;orthographe à partir du curseur...</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="152"/>
        <location filename="../spellerdialog.cpp" line="169"/>
        <location filename="../spellerdialog.cpp" line="193"/>
        <location filename="../spellerdialog.cpp" line="319"/>
        <source>No more misspelled words</source>
        <translation>Plus de mots inconnus</translation>
    </message>
</context>
<context>
    <name>StructDialog</name>
    <message>
        <location filename="../structdialog.ui" line="14"/>
        <source>Structure</source>
        <translation>Structure</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="37"/>
        <source>Numeration</source>
        <translation>Numérotation</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="53"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
</context>
<context>
    <name>SymbolListWidget</name>
    <message>
        <location filename="../symbollistwidget.cpp" line="52"/>
        <source>Add to favorites</source>
        <translation>Ajouter aux favoris</translation>
    </message>
    <message>
        <location filename="../symbollistwidget.cpp" line="53"/>
        <source>Remove from favorites</source>
        <translation>Retirer des favoris</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <source>Horizontal Separator</source>
        <translation type="obsolete">Séparateur horizontal</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="37"/>
        <source>Num of Columns</source>
        <translation>Nb colonnes</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="78"/>
        <source>Columns</source>
        <translation>Colonnes</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="86"/>
        <source>Column :</source>
        <translation>Colonne :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="113"/>
        <source>Alignment :</source>
        <translation>Alignement :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="127"/>
        <source>Left Border :</source>
        <translation>Bordure Gauche :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="167"/>
        <source>Apply to all columns</source>
        <translation>Appliquer à toutes les colonnes</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="194"/>
        <source>Right Border (last column) :</source>
        <translation>Bordure droite (dernière colonne)</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="263"/>
        <source>Rows</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="271"/>
        <source>Row :</source>
        <translation>Ligne :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="296"/>
        <source>Top Border</source>
        <translation>Bordure supérieure</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="305"/>
        <source>Merge columns :</source>
        <translation>Fusionner colonnes :</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="321"/>
        <source>-&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="364"/>
        <source>Apply to all rows</source>
        <translation>Appliquer à toutes les lignes</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="389"/>
        <source>Bottom Border (last row)</source>
        <translation>Bordure inférieure (dernière ligne)</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="396"/>
        <source>Add vertical margin for each row</source>
        <translation>Ajouter marge verticale pour chaque ligne</translation>
    </message>
    <message>
        <source>Columns Alignment</source>
        <translation type="obsolete">Alignement colonnes</translation>
    </message>
    <message>
        <source>Vertical Separator</source>
        <translation type="obsolete">Séparateur vertical</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="232"/>
        <source>Num of Rows</source>
        <translation>Nb lignes</translation>
    </message>
    <message>
        <source>add a margin with the horizontal line</source>
        <translation type="obsolete">Ajouter une marge avec la ligne horizontale</translation>
    </message>
    <message>
        <location filename="../tabdialog.cpp" line="108"/>
        <source>Quick Tabular</source>
        <translation>Assistant tableau</translation>
    </message>
</context>
<context>
    <name>TabbingDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="67"/>
        <source>Spacing</source>
        <translation>Espacement</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="80"/>
        <source>Num of Rows</source>
        <translation>Nb lignes</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="93"/>
        <source>Num of Columns</source>
        <translation>Nb colonnes</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.cpp" line="27"/>
        <source>Quick Tabbing</source>
        <translation>Assistant tabulation</translation>
    </message>
</context>
<context>
    <name>TexdocDialog</name>
    <message>
        <location filename="../texdocdialog.ui" line="20"/>
        <source>Search documentation about :</source>
        <translation>Chercher documentation à propos de :</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="59"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="116"/>
        <source>Texdoc command :</source>
        <translation>Commande Texdoc :</translation>
    </message>
    <message>
        <location filename="../texdocdialog.cpp" line="32"/>
        <source>Browse program</source>
        <translation>Chercher programme</translation>
    </message>
</context>
<context>
    <name>Texmaker</name>
    <message>
        <location filename="../texmaker.cpp" line="667"/>
        <location filename="../texmaker.cpp" line="837"/>
        <location filename="../texmaker.cpp" line="2658"/>
        <location filename="../texmaker.cpp" line="5806"/>
        <source>Structure</source>
        <translation>Structure</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8441"/>
        <source>A document must be saved with an extension (and without spaces or accents in the name) before being used by a command.</source>
        <translation>Un document doit être sauvegardé (sans espaces, ni accents dans le nom du fichier) avant de pouvoir être utilisé par une commande.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Make a copy of the %1.pdf/ps document in the &quot;build&quot; subdirectory and delete all the others %1.* files?</source>
        <translation>Effectuer une copie du document %1.pdf/ps dans le sous-répertoire &quot;build&quot; et effacer tous les autres fichiers %1.* ?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete the output files generated by LaTeX ?</source>
        <translation>Effacer les fichiers auxiliaires générés par LaTeX ?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10360"/>
        <location filename="../texmaker.cpp" line="10400"/>
        <source>Click to jump to the line</source>
        <translation>Cliquer pour aller à la ligne correspondante</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="686"/>
        <location filename="../texmaker.cpp" line="5811"/>
        <source>Relation symbols</source>
        <translation>Symboles et opérateurs</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="696"/>
        <location filename="../texmaker.cpp" line="5816"/>
        <source>Arrow symbols</source>
        <translation>Flèches</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="705"/>
        <location filename="../texmaker.cpp" line="5821"/>
        <source>Miscellaneous symbols</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="714"/>
        <location filename="../texmaker.cpp" line="5826"/>
        <source>Delimiters</source>
        <translation>Délimiteurs</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="723"/>
        <location filename="../texmaker.cpp" line="5831"/>
        <source>Greek letters</source>
        <translation>Lettres grecques</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="741"/>
        <location filename="../texmaker.cpp" line="5841"/>
        <source>Favorites symbols</source>
        <translation>Symboles favoris</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="788"/>
        <location filename="../texmaker.cpp" line="816"/>
        <location filename="../texmaker.cpp" line="5856"/>
        <source>MetaPost Commands</source>
        <translation>Commandes metapost</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="796"/>
        <location filename="../texmaker.cpp" line="819"/>
        <location filename="../texmaker.cpp" line="5861"/>
        <source>Tikz Commands</source>
        <translation>Commandes Tikz</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="804"/>
        <location filename="../texmaker.cpp" line="822"/>
        <location filename="../texmaker.cpp" line="5866"/>
        <source>Asymptote Commands</source>
        <translation>Commandes Asymptote</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2663"/>
        <source>Messages / Log File</source>
        <translation>Messages / Fichier log</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1215"/>
        <source>Toggle between the master document and the current document</source>
        <translation>Basculer entre le document maître et le document courant</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1383"/>
        <location filename="../texmaker.cpp" line="11064"/>
        <source>Normal Mode</source>
        <translation>Mode normal</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="obsolete">Prêt</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1407"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1408"/>
        <location filename="../texmaker.cpp" line="1409"/>
        <location filename="../texmaker.cpp" line="2914"/>
        <location filename="../texmaker.cpp" line="2915"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1414"/>
        <source>New by copying an existing file</source>
        <translation>Nouveau en copiant un document existant</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1418"/>
        <location filename="../texmaker.cpp" line="1419"/>
        <location filename="../texmaker.cpp" line="2919"/>
        <location filename="../texmaker.cpp" line="2920"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1424"/>
        <source>Open Recent</source>
        <translation>Récemment ouverts</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1438"/>
        <source>Restore previous session</source>
        <translation>Restaurer la session précédente</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1450"/>
        <location filename="../texmaker.cpp" line="1451"/>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="12260"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1458"/>
        <location filename="../texmaker.cpp" line="4020"/>
        <location filename="../texmaker.cpp" line="4093"/>
        <location filename="../texmaker.cpp" line="5732"/>
        <source>Save As</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1462"/>
        <source>Save All</source>
        <translation>Enregistrer tout</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1242"/>
        <location filename="../texmaker.cpp" line="1470"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1466"/>
        <source>Save A Copy</source>
        <translation>Enregistrer une copie</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1476"/>
        <source>Close All</source>
        <translation>Fermer tout</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1480"/>
        <source>Reload document from file</source>
        <translation>Recharger le document depuis le fichier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1484"/>
        <source>Reload all documents from file</source>
        <translation>Recharger tous les documents depuis le disque</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1494"/>
        <location filename="../texmaker.cpp" line="1495"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1501"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1502"/>
        <location filename="../texmaker.cpp" line="1503"/>
        <source>Undo</source>
        <translation>Défaire</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1508"/>
        <location filename="../texmaker.cpp" line="1509"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1515"/>
        <location filename="../texmaker.cpp" line="1516"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1521"/>
        <location filename="../texmaker.cpp" line="1522"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1527"/>
        <location filename="../texmaker.cpp" line="1528"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1533"/>
        <source>Select All</source>
        <translation>Sélectionner tout</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1539"/>
        <source>Comment</source>
        <translation>Commenter</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1545"/>
        <source>Uncomment</source>
        <translation>Décommenter</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1551"/>
        <source>Indent</source>
        <translation>Indenter</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1564"/>
        <source>Fold</source>
        <translation>Replier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1568"/>
        <source>Unfold</source>
        <translation>Déplier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1573"/>
        <source>&amp;Fold All</source>
        <translation>Tout replier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1616"/>
        <source>&amp;Unfold All</source>
        <translation>Tout déplier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1663"/>
        <source>Find</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1669"/>
        <source>FindNext</source>
        <translation>Chercher à nouveau</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1675"/>
        <source>Find In Directory</source>
        <translation>Chercher dans dossiers</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1680"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1686"/>
        <source>Goto Line</source>
        <translation>Aller à la ligne</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1700"/>
        <source>Refresh Structure</source>
        <translation>Rafraîchir structure</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1706"/>
        <source>Refresh Bibliography</source>
        <translation>Rafraîchir la bibliographie</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1711"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1712"/>
        <location filename="../texmaker.cpp" line="2969"/>
        <location filename="../texmaker.cpp" line="9757"/>
        <source>Quick Build</source>
        <translation>Compilation rapide</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1723"/>
        <location filename="../texmaker.cpp" line="2995"/>
        <source>View Dvi</source>
        <translation>Voir Dvi</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1733"/>
        <location filename="../texmaker.cpp" line="2996"/>
        <source>View PS</source>
        <translation>Voir PS</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1743"/>
        <location filename="../texmaker.cpp" line="2997"/>
        <source>View PDF</source>
        <translation>Voir PDF</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1758"/>
        <location filename="../texmaker.cpp" line="3010"/>
        <source>View Log</source>
        <translation>Voir Log</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1433"/>
        <location filename="../texmaker.cpp" line="1799"/>
        <location filename="../texmaker.cpp" line="2547"/>
        <source>Clean</source>
        <translation>Nettoyer</translation>
    </message>
    <message>
        <source>Convert to Html</source>
        <translation type="obsolete">Exporter vers Html</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1818"/>
        <location filename="../texmaker.cpp" line="3020"/>
        <source>Previous LaTeX Error</source>
        <translation>Erreur LaTeX précédente</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1820"/>
        <location filename="../texmaker.cpp" line="3014"/>
        <source>Next LaTeX Error</source>
        <translation>Erreur LaTeX suivante</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1823"/>
        <source>&amp;LaTeX</source>
        <translation>&amp;LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2138"/>
        <source>&amp;Math</source>
        <translation>&amp;Math</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2422"/>
        <source>&amp;Wizard</source>
        <translation>&amp;Assistants</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2423"/>
        <source>Quick Start</source>
        <translation>Assistant nouveau document</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2432"/>
        <source>Quick Letter</source>
        <translation>Assistant lettre</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2436"/>
        <source>Quick Tabular</source>
        <translation>Assistant tableau</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2439"/>
        <source>Quick Tabbing</source>
        <translation>Assistant tabulation</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2442"/>
        <source>Quick Array</source>
        <translation>Assistant matrice</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2446"/>
        <source>&amp;Bibliography</source>
        <translation>&amp;Bibliographie</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2552"/>
        <source>&amp;User</source>
        <translation>&amp;Utilisateur</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2553"/>
        <source>User &amp;Tags</source>
        <translation>&amp;Balises Utilisateur</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2598"/>
        <source>User &amp;Commands</source>
        <translation>&amp;Commandes Utilisateur</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2624"/>
        <source>Customize Completion</source>
        <translation>Personnaliser Completion</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2645"/>
        <source>&amp;View</source>
        <translation>Afficha&amp;ge</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1224"/>
        <location filename="../texmaker.cpp" line="2646"/>
        <source>Next Document</source>
        <translation>Document suivant</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1220"/>
        <location filename="../texmaker.cpp" line="2651"/>
        <source>Previous Document</source>
        <translation>Document précédent</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1804"/>
        <source>Open Terminal</source>
        <translation>Ouvrir un terminal</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1808"/>
        <source>Export via TeX4ht</source>
        <translation>Exporter via TeX4ht</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1813"/>
        <source>Convert to unicode</source>
        <translation>Convertir en unicode</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2426"/>
        <source>Quick Beamer Presentation</source>
        <translation>Assistant Présentation Beamer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2429"/>
        <source>Quick Xelatex Document</source>
        <translation>Assistant Xelatex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2629"/>
        <source>Run script</source>
        <translation>Éxecuter un script</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2640"/>
        <source>Other script</source>
        <translation>Autre script</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2669"/>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Pdf Viewer</source>
        <translation>Afficheur Pdf</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2693"/>
        <source>Full Screen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2701"/>
        <source>&amp;Options</source>
        <translation>O&amp;ptions</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2702"/>
        <source>Configure Texmaker</source>
        <translation>Configurer Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2707"/>
        <location filename="../texmaker.cpp" line="11055"/>
        <source>Define Current Document as &apos;Master Document&apos;</source>
        <translation>Définir le document courant comme document &apos;maître&apos;</translation>
    </message>
    <message>
        <source>Interface Appearance</source>
        <translation type="vanished">Apparence Interface</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2749"/>
        <source>Manage Settings File</source>
        <translation>Fichier de configuration</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2763"/>
        <source>&amp;Help</source>
        <translation>A&amp;ide</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2764"/>
        <location filename="../texmaker.cpp" line="2765"/>
        <source>LaTeX Reference</source>
        <translation>Référence LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2768"/>
        <location filename="../texmaker.cpp" line="2769"/>
        <source>User Manual</source>
        <translation>Manuel de Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2796"/>
        <source>Check for Update</source>
        <translation>Vérifier mise à jour disponible</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2801"/>
        <source>About Texmaker</source>
        <translation>A propos de Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3026"/>
        <source>Stop Process</source>
        <translation>Stopper le processus</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3751"/>
        <location filename="../texmaker.cpp" line="3900"/>
        <location filename="../texmaker.cpp" line="3959"/>
        <source>The document has been changed outside Texmaker.Do you want to reload it (and discard your changes) or save it (and overwrite the file)?</source>
        <translation>Le document a été modifié en dehors de Texmaker. Voulez-vous le recharger (et perdre les modifications effectuées par vous) ou l&apos;enregistrer (et écraser le fichier sur le disque)?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4539"/>
        <source>The document contains unsaved work.You will lose changes by reloading the document.</source>
        <oldsource>The document contains unsaved work. you will lose changes by reloading the file</oldsource>
        <translation>Le document.a subi des modifications non sauvegardées. Ces modifications seront perdues en rechargeant le document.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <source>Reload the file</source>
        <translation>Recharger le document</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg)</source>
        <translation type="obsolete">Effacer les fichiers générés par LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg)</translation>
    </message>
    <message>
        <source>The appearance setting will take effect after restarting the application.</source>
        <translation type="vanished">Le changement d&apos;apparence prendra effet au prochain démarrage du programme.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1103"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1108"/>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="obsolete">Souligné</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1118"/>
        <source>Left</source>
        <translation>Justifier à gauche</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1123"/>
        <source>Center</source>
        <translation>Centrer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1128"/>
        <source>Right</source>
        <translation>Justifier à droite</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1134"/>
        <location filename="../texmaker.cpp" line="1174"/>
        <source>New line</source>
        <translation>Nouvelle ligne</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="4753"/>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="8441"/>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10020"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <source>You do not have read permission to this file.</source>
        <translation>Vous n&apos;avez pas les droits nécessaires pour lire ce fichier.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3595"/>
        <location filename="../texmaker.cpp" line="3655"/>
        <location filename="../texmaker.cpp" line="12312"/>
        <source>Open File</source>
        <translation>Ouvrir fichier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>Impossible d&apos;enregistrer le fichier. Droits insuffisants ?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Cancel</source>
        <translation>Abandon</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4130"/>
        <location filename="../texmaker.cpp" line="4194"/>
        <location filename="../texmaker.cpp" line="4264"/>
        <location filename="../texmaker.cpp" line="4370"/>
        <source>The document contains unsaved work. Do you want to save it before closing?</source>
        <translation>Le document a été modifié. Voulez-vous l&apos;enregistrer avant de fermer ?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Save and Close</source>
        <translation>Enregistrer et Fermer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Don&apos;t Save and Close</source>
        <translation>Fermer sans enregistrer</translation>
    </message>
    <message>
        <source>The document contains unsaved work. Do you want to save it before exiting?</source>
        <translation type="obsolete">Le document a été modifié. Voulez-vous l&apos;enregistrer avant de fermer ?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="6667"/>
        <source>Select an image File</source>
        <translation>Sélectionner un fichier image</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4311"/>
        <location filename="../texmaker.cpp" line="6698"/>
        <location filename="../texmaker.cpp" line="6723"/>
        <source>Select a File</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <source>Can&apos;t detect the file name</source>
        <translation>Nom de fichier inconnu</translation>
    </message>
    <message>
        <source>View Dvi file</source>
        <translation type="obsolete">Voir fichier Dvi</translation>
    </message>
    <message>
        <source>View PS file</source>
        <translation type="obsolete">Voir fichier PS</translation>
    </message>
    <message>
        <source>View Pdf file</source>
        <translation type="obsolete">Voir fichier Pdf</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind)</source>
        <translation type="obsolete">Effacer les fichiers générés par LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete Files</source>
        <translation>Effacer Fichiers</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Could not start the command.</source>
        <translation>Impossible de lancer la commande.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="764"/>
        <location filename="../texmaker.cpp" line="5872"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1437"/>
        <source>Session</source>
        <translation>Session</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1443"/>
        <source>Save session</source>
        <translation>Enregistrer session</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1446"/>
        <source>Load session</source>
        <translation>Charger session</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2089"/>
        <source>International &amp;Quotes</source>
        <translation>&amp;Guillemets internationaux</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2686"/>
        <source>List of opened files</source>
        <translation>Liste des fichiers ouverts</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2751"/>
        <source>Settings File</source>
        <translation>Fichier de configuration</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2753"/>
        <source>Reset Settings</source>
        <translation>Réinitialiser la configuration</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2756"/>
        <source>Save a copy of the settings file</source>
        <translation>Enregistrer une copie du fichier de configuration</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2759"/>
        <source>Replace the settings file by a new one</source>
        <translation>Remplacer le fichier de configuration par un nouveau</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4774"/>
        <source>Browse script</source>
        <translation>Chercher script</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5710"/>
        <source>Delete settings file?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>Effacer le fichier de configuration?
(Texmaker sera fermé et vous devrez le réouvrir)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5753"/>
        <source>Replace settings file by a new one?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>Remplacer le fichier de configuration par autre?
(Texmaker sera fermé et vous devrez le réouvrir)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5778"/>
        <source>Opened Files</source>
        <translation>Fichiers ouverts</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre)</source>
        <translation type="obsolete">Effacer les fichiers générés par LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PdfLaTeX</source>
        <translation>PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvi Viewer</source>
        <translation>Afficheur Dvi</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PS Viewer</source>
        <translation>Afficheur Ps</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Bibtex</source>
        <translation>Bibtex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>metapost</source>
        <translation>metapost</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ghostscript</source>
        <translation>ghostscript</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Latexmk</source>
        <translation>Latexmk</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>R Sweave</source>
        <translation>R Sweave</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>XeLaTex</source>
        <translation>XeLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LuaLaTex</source>
        <translation>LuaLaTex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10020"/>
        <source>Log File not found !</source>
        <translation>Fichier log non trouvé !</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10474"/>
        <location filename="../texmaker.cpp" line="10509"/>
        <source>No LaTeX errors detected !</source>
        <translation>Aucune erreur LaTeX détectée !</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <source>File not found</source>
        <translation>Fichier non trouvé</translation>
    </message>
    <message>
        <source>Texmaker : User Manual</source>
        <translation type="obsolete">Manuel de Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11079"/>
        <location filename="../texmaker.cpp" line="12629"/>
        <source>Normal Mode (current master document :</source>
        <translation>Mode normal (document maître actuel :</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11081"/>
        <location filename="../texmaker.cpp" line="12631"/>
        <source>Master Document :</source>
        <translation>Document maître :</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2727"/>
        <source>Change Interface Font</source>
        <translation>Changer Police Interface</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1261"/>
        <location filename="../texmaker.cpp" line="1264"/>
        <location filename="../texmaker.cpp" line="1267"/>
        <source>Click to jump to the bookmark</source>
        <translation>Cliquer pour atteindre le signet</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="780"/>
        <location filename="../texmaker.cpp" line="813"/>
        <location filename="../texmaker.cpp" line="5846"/>
        <source>Pstricks Commands</source>
        <translation>Commandes Pstricks</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1693"/>
        <source>Check Spelling</source>
        <translation>Vérifier orthographe</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="732"/>
        <location filename="../texmaker.cpp" line="5836"/>
        <source>Most used symbols</source>
        <translation>Symboles fréquents</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1857"/>
        <source>&amp;Sectioning</source>
        <translation>&amp;Structure</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1887"/>
        <source>&amp;Environment</source>
        <translation>&amp;Environnement</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1933"/>
        <source>&amp;List Environment</source>
        <translation>&amp;Listes</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1957"/>
        <source>Font St&amp;yles</source>
        <translation>St&amp;yles de caractères</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1994"/>
        <source>&amp;Tabular Environment</source>
        <translation>&amp;Tableaux</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2020"/>
        <source>S&amp;pacing</source>
        <translation>Es&amp;pacement</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2047"/>
        <source>International &amp;Accents</source>
        <translation>&amp;Accents internationaux</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2202"/>
        <source>Math &amp;Functions</source>
        <translation>&amp;Fonctions math</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2324"/>
        <source>Math Font St&amp;yles</source>
        <translation>St&amp;yles de caractères math</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2358"/>
        <source>Math &amp;Accents</source>
        <translation>&amp;Accents math</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2400"/>
        <source>Math S&amp;paces</source>
        <translation>Es&amp;pacement math</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2595"/>
        <location filename="../texmaker.cpp" line="8156"/>
        <location filename="../texmaker.cpp" line="8212"/>
        <source>Edit User &amp;Tags</source>
        <translation>Editer &amp;Balises Utilisateur</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2620"/>
        <location filename="../texmaker.cpp" line="9717"/>
        <location filename="../texmaker.cpp" line="9753"/>
        <source>Edit User &amp;Commands</source>
        <translation>Editer &amp;Commandes Utilisateur</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2139"/>
        <source>Inline math mode $...$</source>
        <translation>Math en ligne $...$</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2144"/>
        <source>Display math mode \[...\]</source>
        <translation>Math hors ligne \[...\]</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2149"/>
        <source>Numbered equations \begin{equation}</source>
        <translation>Equations numérotées \begin{equation}</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1488"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1557"/>
        <source>Unindent</source>
        <translation>Désindenter</translation>
    </message>
    <message>
        <source>Add LRM to Document</source>
        <translation type="obsolete">Ajouter LRM au document</translation>
    </message>
    <message>
        <source>Remove LRM from Document</source>
        <translation type="obsolete">Supprimer LRM du document</translation>
    </message>
    <message>
        <source>Bidirecting (All Line)</source>
        <translation type="obsolete">Double direction (toutes lignes)</translation>
    </message>
    <message>
        <source>Text Direction Right to Left</source>
        <translation type="obsolete">Direction du texte de Droite à Gauche</translation>
    </message>
    <message>
        <source>Text Direction Left to Right</source>
        <translation type="obsolete">Direction du texte de Gauche à Droite</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2732"/>
        <source>Interface Language</source>
        <translation>Langue de l&apos;interface</translation>
    </message>
    <message>
        <source>Compile</source>
        <translation type="obsolete">Exécuter</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3004"/>
        <source>View</source>
        <translation>Voir</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4753"/>
        <source>Error : Can&apos;t open the dictionary</source>
        <translation>Erreur : impossible d&apos;ouvrir le dictionnaire</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre,.nlg,.nlo,.nls)</source>
        <translation type="obsolete">Effacer les fichiers générés par LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind,.out,.synctex.gz,.blg,.thm,.pre,.nlg,.nlo,.nls)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11623"/>
        <source>The language setting will take effect after restarting the application.</source>
        <translation>Le changement de langue prendra effet au prochain démarrage du programme.</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2991"/>
        <source>Run</source>
        <translation>Exécuter</translation>
    </message>
</context>
<context>
    <name>UnicodeDialog</name>
    <message>
        <location filename="../unicodedialog.ui" line="14"/>
        <location filename="../unicodedialog.ui" line="98"/>
        <source>Convert to unicode</source>
        <translation>Convertir en unicode</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="28"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="75"/>
        <source>Original Encoding</source>
        <translation>Encodage d&apos;origine</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="145"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="56"/>
        <source>Select a File</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>You do not have read permission to this file.</source>
        <translation>Vous n&apos;avez pas les droits nécessaires pour lire ce fichier.</translation>
    </message>
</context>
<context>
    <name>UnicodeView</name>
    <message>
        <location filename="../unicodeview.cpp" line="56"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="152"/>
        <source>Save As</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>Impossible d&apos;enregistrer le fichier. Droits insuffisants ?</translation>
    </message>
</context>
<context>
    <name>UserCompletionDialog</name>
    <message>
        <location filename="../usercompletiondialog.ui" line="14"/>
        <source>Completion</source>
        <translation>Complétion</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="25"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="48"/>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="55"/>
        <source>( @ : placeholder )</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="62"/>
        <source>( #bib# : bibliography item )</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="69"/>
        <source>( #label# : label item )</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="76"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="85"/>
        <source>Items already supplied by Texmaker</source>
        <translation>Items déjà fournis par Texmaker</translation>
    </message>
</context>
<context>
    <name>UserMenuDialog</name>
    <message>
        <location filename="../usermenudialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>Editer balises</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="45"/>
        <source>Menu Item</source>
        <translation>Item menu</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="55"/>
        <source>LaTeX Content</source>
        <translation>Code LaTeX</translation>
    </message>
</context>
<context>
    <name>UserQuickDialog</name>
    <message>
        <location filename="../userquickdialog.ui" line="14"/>
        <source>Quick Build Command</source>
        <translation>Commande compilation rapide</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="42"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="70"/>
        <source>Ordered list of commands :</source>
        <translation>Liste ordonnée de commandes :</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="80"/>
        <source>Up</source>
        <translation>Au dessus</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="87"/>
        <source>Down</source>
        <translation>En dessous</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="94"/>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
</context>
<context>
    <name>UserTagsListWidget</name>
    <message>
        <location filename="../usertagslistwidget.cpp" line="30"/>
        <source>Add tag</source>
        <translation>Ajouter balise</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="32"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="34"/>
        <source>Modify</source>
        <translation>Modifier</translation>
    </message>
</context>
<context>
    <name>UserToolDialog</name>
    <message>
        <location filename="../usertooldialog.ui" line="14"/>
        <source>Edit User Commands</source>
        <translation>Editer commandes</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="70"/>
        <source>Menu Item</source>
        <translation>Item menu</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="77"/>
        <source>Command (% : filename without extension)</source>
        <translation>Commande (% : nom fichier sans extension)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="103"/>
        <source>wizard</source>
        <translation>assistant</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="51"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(les commandes sont séparées par &apos;|&apos;)</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../versiondialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="20"/>
        <source>Check for available version</source>
        <translation>Vérifer version disponible</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="34"/>
        <source>Available version</source>
        <translation>Version disponible</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="48"/>
        <source>Go to the download page</source>
        <translation>Aller à la page de téléchargement</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="68"/>
        <source>You&apos;re using the version</source>
        <translation>Vous utilisez la version</translation>
    </message>
    <message>
        <location filename="../versiondialog.cpp" line="53"/>
        <location filename="../versiondialog.cpp" line="60"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>WebPublishDialog</name>
    <message>
        <source>Convert to Html</source>
        <translation type="obsolete">Exporter vers Html</translation>
    </message>
    <message>
        <source>Html options</source>
        <translation type="obsolete">Options Html</translation>
    </message>
    <message>
        <source>Title :</source>
        <translation type="obsolete">Titre :</translation>
    </message>
    <message>
        <source>Title of the html files</source>
        <translation type="obsolete">Titre fichiers html</translation>
    </message>
    <message>
        <source>Navigation :</source>
        <translation type="obsolete">Navigation :</translation>
    </message>
    <message>
        <source>Footnote :</source>
        <translation type="obsolete">Bas de page :</translation>
    </message>
    <message>
        <source>Navigation mode</source>
        <translation type="obsolete">Mode navigation</translation>
    </message>
    <message>
        <source>Alignment :</source>
        <translation type="obsolete">Alignement :</translation>
    </message>
    <message>
        <source>Create an index page ?</source>
        <translation type="obsolete">Créer page index ?</translation>
    </message>
    <message>
        <source>Text displayed at the bottom of each html files </source>
        <translation type="obsolete">Texte affiché en bas de chaque page</translation>
    </message>
    <message>
        <source>Alignment in the html files</source>
        <translation type="obsolete">Alignement dans les pages html</translation>
    </message>
    <message>
        <source>Create index :</source>
        <translation type="obsolete">Créer index :</translation>
    </message>
    <message>
        <source>Images options</source>
        <translation type="obsolete">Options images</translation>
    </message>
    <message>
        <source>Width of the largest image</source>
        <translation type="obsolete">Largeur de la plus grande image</translation>
    </message>
    <message>
        <source>Images Width :</source>
        <translation type="obsolete">Largeur image :</translation>
    </message>
    <message>
        <source>LaTeX options</source>
        <translation type="obsolete">Options LaTeX</translation>
    </message>
    <message>
        <source>Number of latex compilations for the input file</source>
        <translation type="obsolete">Nombre de compilations latex</translation>
    </message>
    <message>
        <source>Content name :</source>
        <translation type="obsolete">Code sommaire :</translation>
    </message>
    <message>
        <source>Start Index :</source>
        <translation type="obsolete">Page de départ :</translation>
    </message>
    <message>
        <source>Tocdepth :</source>
        <translation type="obsolete">Tocdepth :</translation>
    </message>
    <message>
        <source>Latex code for the title of the table of contents</source>
        <translation type="obsolete">Code latex pour le titre du sommaire</translation>
    </message>
    <message>
        <source>The number of the ps page corresponding to the first numberline indexed in the toc file</source>
        <translation type="obsolete">Numéro de la première page postscript indexée dans le fichier toc</translation>
    </message>
    <message>
        <source>Set the value of the LaTeX tocdepth counter</source>
        <translation type="obsolete">Valeur du compteur tocdepth</translation>
    </message>
    <message>
        <source>Number of compilations :</source>
        <translation type="obsolete">Nb de compilations :</translation>
    </message>
    <message>
        <source>Browser command. Let&apos;s empty to not run the browser at the end of the conversion</source>
        <translation type="obsolete">Commande pour lancer le navigateur (laisser vide pour ne pas lancer de navigateur)</translation>
    </message>
    <message>
        <source>Launch</source>
        <translation type="obsolete">Lancer</translation>
    </message>
    <message>
        <source>Input File :</source>
        <translation type="obsolete">Fichier source :</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
    <message>
        <source>Browser :</source>
        <translation type="obsolete">Navigateur :</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="obsolete">Justifier à gauche</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">Centrer</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="obsolete">Justifier à droite</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="obsolete">Icones</translation>
    </message>
    <message>
        <source>Page numbers</source>
        <translation type="obsolete">Numéros page</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="obsolete">Ouvrir fichier</translation>
    </message>
</context>
<context>
    <name>X11FontDialog</name>
    <message>
        <location filename="../x11fontdialog.ui" line="14"/>
        <source>Select a Font</source>
        <translation>Sélectionner Police</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abandon</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="34"/>
        <source>Font Family</source>
        <translation>Police</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="44"/>
        <source>Font Size</source>
        <translation>Taille</translation>
    </message>
</context>
</TS>
