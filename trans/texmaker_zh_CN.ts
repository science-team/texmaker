<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.ui" line="14"/>
        <source>About Texmaker</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="76"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="86"/>
        <source>Authors</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="96"/>
        <source>Thanks</source>
        <translation>致谢</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="106"/>
        <source>License</source>
        <translation>许可</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
</context>
<context>
    <name>AddOptionDialog</name>
    <message>
        <location filename="../addoptiondialog.ui" line="14"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>AddTagDialog</name>
    <message>
        <location filename="../addtagdialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>编辑自定义标签</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="22"/>
        <source>Item</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="32"/>
        <source>LaTeX Content</source>
        <translation>LaTeX代码</translation>
    </message>
    <message>
        <location filename="../addtagdialog.ui" line="55"/>
        <source>Keyboard Trigger</source>
        <translation>按键</translation>
    </message>
</context>
<context>
    <name>ArrayDialog</name>
    <message>
        <location filename="../arraydialog.cpp" line="43"/>
        <source>Quick Array</source>
        <translation>数组向导</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="45"/>
        <source>Num of Columns</source>
        <translation>列数</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="59"/>
        <source>Columns Alignment</source>
        <translation>列对齐</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="76"/>
        <source>Environment</source>
        <translation>环境</translation>
    </message>
    <message>
        <location filename="../arraydialog.ui" line="35"/>
        <source>Num of Rows</source>
        <translation>行数</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../browser.cpp" line="78"/>
        <source>&amp;File</source>
        <translation>&amp;文件</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="79"/>
        <source>Print</source>
        <translation>打印</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="81"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="89"/>
        <source>Index</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../browser.cpp" line="107"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <source>Get dictionary at: %1</source>
        <translation type="obsolete">获取字典：%1</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="174"/>
        <location filename="../configdialog.cpp" line="197"/>
        <location filename="../configdialog.cpp" line="198"/>
        <source>Commands</source>
        <translation>命令</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="181"/>
        <location filename="../configdialog.cpp" line="199"/>
        <source>Quick Build</source>
        <translation>快速构建</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1140"/>
        <location filename="../configdialog.cpp" line="187"/>
        <location filename="../configdialog.cpp" line="200"/>
        <source>Editor</source>
        <translation>编辑器</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="53"/>
        <source>Get more dictionaries at: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="245"/>
        <source>Browse dictionary</source>
        <translation>浏览字典</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>svn command directory</source>
        <translation>SVN命令文件夹</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <location filename="../configdialog.cpp" line="391"/>
        <location filename="../configdialog.cpp" line="402"/>
        <location filename="../configdialog.cpp" line="414"/>
        <location filename="../configdialog.cpp" line="425"/>
        <location filename="../configdialog.cpp" line="436"/>
        <location filename="../configdialog.cpp" line="447"/>
        <location filename="../configdialog.cpp" line="458"/>
        <location filename="../configdialog.cpp" line="469"/>
        <location filename="../configdialog.cpp" line="480"/>
        <location filename="../configdialog.cpp" line="491"/>
        <location filename="../configdialog.cpp" line="502"/>
        <location filename="../configdialog.cpp" line="513"/>
        <location filename="../configdialog.cpp" line="524"/>
        <location filename="../configdialog.cpp" line="535"/>
        <location filename="../configdialog.cpp" line="546"/>
        <source>Browse program</source>
        <translation>浏览程序</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="14"/>
        <source>Configure Texmaker</source>
        <translation>配置Texmaker</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="130"/>
        <source>Commands (% : filename without extension - @ : line number)</source>
        <translation>命令(%:不带扩展名的文件名，@:行号)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="755"/>
        <location filename="../configdialog.cpp" line="619"/>
        <location filename="../configdialog.cpp" line="682"/>
        <source>Dvi Viewer</source>
        <translation>DVI查看器</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="820"/>
        <location filename="../configdialog.cpp" line="622"/>
        <location filename="../configdialog.cpp" line="685"/>
        <source>PS Viewer</source>
        <translation>PS查看器</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="278"/>
        <location filename="../configdialog.cpp" line="643"/>
        <location filename="../configdialog.cpp" line="706"/>
        <source>ghostscript</source>
        <translation>Ghostscript</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="631"/>
        <location filename="../configdialog.cpp" line="694"/>
        <source>Bibtex</source>
        <translation>BibTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="383"/>
        <location filename="../configdialog.cpp" line="625"/>
        <location filename="../configdialog.cpp" line="688"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="155"/>
        <location filename="../configdialog.cpp" line="613"/>
        <location filename="../configdialog.cpp" line="676"/>
        <source>PdfLaTeX</source>
        <translation>PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="366"/>
        <location filename="../configdialog.cpp" line="634"/>
        <location filename="../configdialog.cpp" line="697"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="399"/>
        <location filename="../configdialog.cpp" line="628"/>
        <location filename="../configdialog.cpp" line="691"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="600"/>
        <location filename="../configdialog.cpp" line="637"/>
        <location filename="../configdialog.cpp" line="700"/>
        <source>Pdf Viewer</source>
        <translation>PDF查看器</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="309"/>
        <location filename="../configdialog.cpp" line="640"/>
        <location filename="../configdialog.cpp" line="703"/>
        <source>metapost</source>
        <translation>MetaPost</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="141"/>
        <location filename="../configdialog.cpp" line="610"/>
        <location filename="../configdialog.cpp" line="673"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="376"/>
        <location filename="../configdialog.cpp" line="616"/>
        <location filename="../configdialog.cpp" line="679"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <source>Built-in Viewer</source>
        <translation type="vanished">内置查看器</translation>
    </message>
    <message>
        <source>External Viewer</source>
        <translation type="vanished">外部查看器</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="271"/>
        <location filename="../configdialog.cpp" line="646"/>
        <location filename="../configdialog.cpp" line="709"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="319"/>
        <location filename="../configdialog.cpp" line="649"/>
        <location filename="../configdialog.cpp" line="712"/>
        <source>Latexmk</source>
        <translation>LaTeX-Mk</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="629"/>
        <source>Embed</source>
        <translation>嵌入</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="446"/>
        <location filename="../configdialog.cpp" line="652"/>
        <location filename="../configdialog.cpp" line="715"/>
        <source>R Sweave</source>
        <translation>R Sweave</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="247"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &amp;quot;--output-directory=build&amp;quot; option will be automatically added to the (pdf)latex command while the compilation.&lt;/p&gt;&lt;p&gt;For the others commands like dvips, ps2pdf, bibtex,... you will have to manually replaced &amp;quot;%&amp;quot; by &amp;quot;build/%&amp;quot; .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;编译时，--output-directory=build选项将自动添加到(Pdf)LaTeX命令中。对于dvips, ps2pdf, bibtex等其他命令，需要将“%”替换为“build/%”。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="250"/>
        <source>Use a &quot;build&quot; subdirectory for output files</source>
        <translation>为输出文件建立build文件夹</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="148"/>
        <location filename="../configdialog.cpp" line="655"/>
        <location filename="../configdialog.cpp" line="718"/>
        <source>XeLaTeX</source>
        <translation>XeLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="285"/>
        <source>Add to PATH</source>
        <translation>添加到环境变量PATH</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="704"/>
        <source>lp options for the printer</source>
        <translation>打印机IP设置</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="896"/>
        <source>Quick Build Command</source>
        <translation>快速构建命令</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="918"/>
        <source>LaTeX + dvips + View PS</source>
        <translation>LaTeX + dvips + 查看PS</translation>
    </message>
    <message>
        <source>LaTeX + View DVI</source>
        <translation type="vanished">LaTeX + 查看DVI</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="904"/>
        <source>PdfLaTeX + View PDF</source>
        <translation>PdfLaTeX + 查看PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="946"/>
        <source>LaTeX + dvipdfm + View PDF</source>
        <translation>LaTeX + dvipdfm + 查看PDF</translation>
    </message>
    <message>
        <source>LaTeX + dvips + ps2pdf + View PDF</source>
        <translation type="vanished">LaTeX  +dvips + ps2pdf + 查看PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="183"/>
        <location filename="../configdialog.cpp" line="658"/>
        <location filename="../configdialog.cpp" line="721"/>
        <source>LuaLaTeX</source>
        <translation>LuaLaTeX</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="356"/>
        <source>Bib(la)tex</source>
        <translation>Bib(la)tex</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="559"/>
        <source>Launch the &quot;Clean&quot; tool when exiting Texmaker</source>
        <translation>退出Texmaker时加载“清理”工具</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="925"/>
        <source>PdfLaTeX + Bib(la)tex + PdfLaTeX (x2) + View Pdf</source>
        <translation>PdfLaTeX + Bib(la)tex + 两次PdfLaTeX + 查看PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="953"/>
        <source>LaTeX + Bib(la)tex + LaTeX (x2) + dvips + ps2pdf + View Pdf</source>
        <translation>LaTeX + Bib(la)tex + 两次LaTeX + dvips + ps2pdf + 查看PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="960"/>
        <source>Sweave + PdfLaTeX + View Pdf</source>
        <translation>Sweave + PdfLaTeX + 查看PDF</translation>
    </message>
    <message>
        <source>LaTeX + Asymptote + LaTeX + dvips + View PS</source>
        <oldsource>LaTeX + Asymptote + LaTeX + View PS</oldsource>
        <translation type="vanished">LaTeX + Asymptote + LaTeX + dvips + 查看PS</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="974"/>
        <source>PdfLaTeX + Asymptote + PdfLaTeX + View Pdf</source>
        <translation>PdfLaTeX + Asymptote + PdfLaTeX + 查看PDF</translation>
    </message>
    <message>
        <source>LatexMk + View PDF</source>
        <translation type="vanished">LaTeX-Mk + 查看PDF</translation>
    </message>
    <message>
        <source>XeLaTeX + View PDF</source>
        <translation type="vanished">XeLaTeX + 查看PDF</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="995"/>
        <source>LuaLaTeX + View PDF</source>
        <translation>LuaLaTeX + 查看PDF</translation>
    </message>
    <message>
        <source>User : (% : filename without extension)</source>
        <translation type="vanished">自定义:(%:不带扩展名的文件名)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1030"/>
        <location filename="../configdialog.ui" line="1069"/>
        <source>wizard</source>
        <translation>向导</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1043"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(命令必须用&apos;|&apos;分隔)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1059"/>
        <source>For .asy files</source>
        <translation>.asy文件命令</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1107"/>
        <source>Don&apos;t launch a new instance of the viewer if the dvi/ps/pdf file is already opened</source>
        <translation>当DVI/PS/PDF文档已经打开时，不打开新的查看器窗口</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1269"/>
        <source>Editor Font Family</source>
        <translation>编辑器字体</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1283"/>
        <source>Editor Font Size</source>
        <translation>编辑器字体大小</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1300"/>
        <source>Editor Font Encoding</source>
        <translation>编辑器字体编码</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1183"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1188"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1160"/>
        <source>Colors</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1146"/>
        <source>Default Theme</source>
        <translation>默认主题</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1209"/>
        <source>Dark theme</source>
        <translation>深色主题</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1324"/>
        <source>Word Wrap</source>
        <translation>换行</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1338"/>
        <source>Show Line Numbers</source>
        <translation>显示行号</translation>
    </message>
    <message>
        <source>&quot;Math&quot; color</source>
        <translation type="obsolete">&quot;Math&quot; 颜色</translation>
    </message>
    <message>
        <source>Click here to set this color</source>
        <translation type="obsolete">点击这里设置颜色</translation>
    </message>
    <message>
        <source>&quot;Command&quot; color</source>
        <translation type="obsolete">&quot;Command&quot; 颜色</translation>
    </message>
    <message>
        <source>&quot;Keyword&quot; color</source>
        <translation type="obsolete">&quot;Keyword&quot; 颜色</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1310"/>
        <source>Check for external changes</source>
        <translation>检查外部更改</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1317"/>
        <source>Backup documents every 10 min</source>
        <translation>每10分钟备份一次文档</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1362"/>
        <source>Spelling dictionary</source>
        <translation>拼写字典</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1396"/>
        <source>Inline Spell Checking</source>
        <translation>内联拼写字典</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1399"/>
        <source>Inline</source>
        <translation>内联</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1232"/>
        <source>Tab width (num of spaces)</source>
        <translation>Tab宽度(空格数)</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="616"/>
        <source>Built-in &amp;Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="649"/>
        <source>E&amp;xternal Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="932"/>
        <source>LaTeX + &amp;View DVI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="939"/>
        <source>LaTeX + dvips + ps&amp;2pdf + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="967"/>
        <source>LaTeX + As&amp;ymptote + LaTeX + dvips + View PS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="981"/>
        <source>Latex&amp;Mk + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="988"/>
        <source>&amp;XeLaTeX + View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1011"/>
        <source>User : (% : filename &amp;without extension)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1153"/>
        <source>Replace tab with spaces</source>
        <translation>用空格替换Tab</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1444"/>
        <source>Svn</source>
        <translation>SVN</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1450"/>
        <source>Detect uncommitted lines at the opening</source>
        <translation>检测文档开头空行</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1457"/>
        <source>Path to the svn command</source>
        <translation>SVN命令路径</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1485"/>
        <location filename="../configdialog.cpp" line="193"/>
        <location filename="../configdialog.cpp" line="201"/>
        <source>Shortcuts</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1521"/>
        <source>Toggle focus editor/pdf viewer</source>
        <translation>在编辑器和PDF查看器间切换</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1528"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1495"/>
        <source>Action</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1500"/>
        <source>Shortcut</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="1331"/>
        <source>Completion</source>
        <translation>自动补全</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>DocumentView</name>
    <message>
        <source>Unlock %1</source>
        <translation type="vanished">解锁%1</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="513"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation>打印%1中</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1052"/>
        <source>Loading pdf...</source>
        <translation>加载PDF中</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1358"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1374"/>
        <source>Number of words in the document</source>
        <translation>文档字数</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1384"/>
        <source>Save As</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1419"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../documentview.cpp" line="1473"/>
        <source>Number of words in the page</source>
        <translation>页内字数</translation>
    </message>
</context>
<context>
    <name>EncodingDialog</name>
    <message>
        <location filename="../encodingdialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="20"/>
        <source>It seems that this file cannot be correctly decoded
with the default encoding setting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../encodingdialog.ui" line="31"/>
        <source>Use this encoding :</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../exportdialog.ui" line="14"/>
        <source>Export via TeX4ht</source>
        <translation>通过TeX4ht导出</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="22"/>
        <source>Path to htlatex</source>
        <translation>htlatex路径</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="45"/>
        <source>Mode</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="51"/>
        <source>export to Html</source>
        <translation>导出到HTML</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="61"/>
        <source>export to Html (new page for each section)</source>
        <translation>导出到HTML(每个section均从新页开始)</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="68"/>
        <source>export to OpenDocumentFormat</source>
        <translation>导出到OpenDocumentFormat</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="75"/>
        <source>export to MathML</source>
        <translation>导出到MathML</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="82"/>
        <source>User</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="91"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../exportdialog.ui" line="125"/>
        <source>Run</source>
        <translation>运行</translation>
    </message>
    <message>
        <location filename="../exportdialog.cpp" line="49"/>
        <source>Browse program</source>
        <translation>浏览程序</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <location filename="../filechooser.cpp" line="44"/>
        <source>Select a File</source>
        <translation>选择一个文件</translation>
    </message>
    <message>
        <location filename="../filechooser.ui" line="31"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <location filename="../findwidget.ui" line="23"/>
        <source>Form</source>
        <translation>从</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="47"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="57"/>
        <source>Forward</source>
        <translation>向下</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="67"/>
        <source>Backward</source>
        <translation>向上</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="92"/>
        <source>Whole words only</source>
        <translation>全字匹配</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="99"/>
        <source>Case sensitive</source>
        <translation>区分大小写</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="106"/>
        <source>Start at Beginning</source>
        <translation>从头开始</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="163"/>
        <source>Regular Expression</source>
        <translation>正则表达式</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="170"/>
        <location filename="../findwidget.cpp" line="158"/>
        <source>Text must be selected before checking this option.</source>
        <translation>在检查这个选项之前必须选择文本。</translation>
    </message>
    <message>
        <location filename="../findwidget.ui" line="173"/>
        <source>In selection only</source>
        <translation>只在选中的部分中查找</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../findwidget.cpp" line="81"/>
        <source>Invalid regular expression.</source>
        <translation>无效的正则表达式。</translation>
    </message>
</context>
<context>
    <name>GotoLineDialog</name>
    <message>
        <source>Goto Line</source>
        <translation type="obsolete">跳转到行</translation>
    </message>
    <message>
        <source>Goto</source>
        <translation type="obsolete">跳转</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <source>Line</source>
        <translation type="obsolete">行</translation>
    </message>
</context>
<context>
    <name>GotoLineWidget</name>
    <message>
        <location filename="../gotolinewidget.ui" line="20"/>
        <source>Form</source>
        <translation>从</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="59"/>
        <source>Line</source>
        <translation>行</translation>
    </message>
    <message>
        <location filename="../gotolinewidget.ui" line="37"/>
        <source>Goto</source>
        <translation>跳转</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>GraphicFileChooser</name>
    <message>
        <location filename="../graphicfilechooser.ui" line="36"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="95"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="221"/>
        <source>Use &quot;figure&quot; environment</source>
        <translation>使用figure环境</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="152"/>
        <source>Caption</source>
        <translation>配图文字</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="176"/>
        <source>Above</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="181"/>
        <source>Below</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="195"/>
        <source>Placement</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="208"/>
        <source>hbtp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.ui" line="233"/>
        <source>Center</source>
        <translation>居中</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <location filename="../graphicfilechooser.cpp" line="49"/>
        <source>Select a File</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>KeySequenceDialog</name>
    <message>
        <location filename="../keysequencedialog.ui" line="37"/>
        <location filename="../keysequencedialog.cpp" line="43"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../keysequencedialog.ui" line="14"/>
        <source>Shortcut</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>LatexEditor</name>
    <message>
        <location filename="../latexeditor.cpp" line="914"/>
        <source>Undo</source>
        <translation>撤消</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="917"/>
        <source>Redo</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="921"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="924"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="927"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="932"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="938"/>
        <source>Check Spelling Word</source>
        <translation>检查拼写</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="943"/>
        <source>Check Spelling Selection</source>
        <translation>检查选中部分的拼写</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="946"/>
        <source>Check Spelling Document</source>
        <translation>检查文档拼写</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="953"/>
        <source>Jump to the end of this block</source>
        <translation>转到代码块末尾</translation>
    </message>
    <message>
        <location filename="../latexeditor.cpp" line="959"/>
        <source>Jump to pdf</source>
        <translation>转到PDF</translation>
    </message>
</context>
<context>
    <name>LetterDialog</name>
    <message>
        <location filename="../letterdialog.cpp" line="56"/>
        <source>Quick Letter</source>
        <translation>信件向导</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="40"/>
        <source>Typeface Size</source>
        <translation>字体大小</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="53"/>
        <source>Encoding</source>
        <translation>编码</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="60"/>
        <source>AMS Packages</source>
        <translation>AMS包</translation>
    </message>
    <message>
        <location filename="../letterdialog.ui" line="79"/>
        <source>Paper Size</source>
        <translation>纸张大小</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>LightFindWidget</name>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../lightfindwidget.cpp" line="79"/>
        <source>Invalid regular expression.</source>
        <translation>无效的正则表达式。</translation>
    </message>
</context>
<context>
    <name>LightLatexEditor</name>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="483"/>
        <source>You do not have read permission to this file.</source>
        <translation>你没有读这个文件的权限。</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="900"/>
        <location filename="../lightlatexeditor.cpp" line="903"/>
        <location filename="../lightlatexeditor.cpp" line="906"/>
        <source>Click to jump to the bookmark</source>
        <translation>点击以跳转到书签</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="910"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../lightlatexeditor.cpp" line="913"/>
        <source>Goto Line</source>
        <translation>跳转到行</translation>
    </message>
</context>
<context>
    <name>LightLineNumberWidget</name>
    <message>
        <location filename="../lightlinenumberwidget.cpp" line="181"/>
        <source>Click to add or remove a bookmark</source>
        <translation>点击添加或移除书签</translation>
    </message>
</context>
<context>
    <name>LineNumberWidget</name>
    <message>
        <location filename="../linenumberwidget.cpp" line="286"/>
        <source>Click to add or remove a bookmark</source>
        <translation>点击这里添加或移除书签</translation>
    </message>
</context>
<context>
    <name>PageItem</name>
    <message>
        <location filename="../pageitem.cpp" line="137"/>
        <location filename="../pageitem.cpp" line="139"/>
        <source>Click to jump to the line</source>
        <translation>点击跳转到行</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="146"/>
        <source>Number of words in the document</source>
        <translation>文档字数</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="150"/>
        <source>Number of words in the page</source>
        <translation>页内字数</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="154"/>
        <source>Convert page to png image</source>
        <translation>把页面转换为PNG图片</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="158"/>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="unfinished">拼写和语法检查</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="162"/>
        <source>Open the file browser</source>
        <translation>打开文档浏览器</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="547"/>
        <source>Copy text</source>
        <translation>复制文本</translation>
    </message>
    <message>
        <location filename="../pageitem.cpp" line="548"/>
        <source>Copy image</source>
        <translation>复制图像</translation>
    </message>
</context>
<context>
    <name>PaperDialog</name>
    <message>
        <source>Print</source>
        <translation type="obsolete">打印</translation>
    </message>
    <message>
        <source>Paper Size</source>
        <translation type="obsolete">纸张大小</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>PdfDocumentWidget</name>
    <message>
        <source>Click to jump to the line</source>
        <translation type="obsolete">点击以跳转到行</translation>
    </message>
</context>
<context>
    <name>PdfViewer</name>
    <message>
        <location filename="../pdfviewer.cpp" line="134"/>
        <location filename="../pdfviewer.cpp" line="1117"/>
        <source>Pages</source>
        <translation>页面</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="184"/>
        <source>&amp;File</source>
        <translation>&amp;文件</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="110"/>
        <location filename="../pdfviewer.cpp" line="153"/>
        <location filename="../pdfviewer.cpp" line="1110"/>
        <source>Structure</source>
        <translation>文档结构</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="188"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="190"/>
        <source>&amp;Edit</source>
        <translation>&amp;编辑</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="201"/>
        <source>Previous</source>
        <translation>上一页</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="205"/>
        <source>Next</source>
        <translation>下一页</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="211"/>
        <source>&amp;View</source>
        <translation>&amp;查看</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="212"/>
        <source>Fit Width</source>
        <translation>适合宽度</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="215"/>
        <source>Fit Page</source>
        <translation>适合页面</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="218"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="223"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="228"/>
        <source>Continuous</source>
        <translation>连续</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="233"/>
        <source>Two pages</source>
        <translation>双页</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="238"/>
        <source>Rotate left</source>
        <translation>向左旋转</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="242"/>
        <source>Rotate right</source>
        <translation>向右旋转</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="246"/>
        <source>Presentation...</source>
        <translation>放映</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="255"/>
        <source>Previous Position</source>
        <translation>上一位置</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="260"/>
        <source>Next Position</source>
        <translation>下一位置</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">拼写和语法检查</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">另存为</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="191"/>
        <location filename="../pdfviewer.cpp" line="288"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="299"/>
        <location filename="../pdfviewer.cpp" line="992"/>
        <source>Print</source>
        <translation>打印</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="302"/>
        <source>External Viewer</source>
        <translation>外部查看器</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="441"/>
        <location filename="../pdfviewer.cpp" line="669"/>
        <source>Page</source>
        <translation>页面</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../pdfviewer.cpp" line="460"/>
        <source>File not found</source>
        <translation>未找到文件</translation>
    </message>
</context>
<context>
    <name>PdfViewerWidget</name>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="104"/>
        <source>Show/Hide Table of contents</source>
        <translation>显示/隐藏目录</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="110"/>
        <source>Previous</source>
        <translation>上一页</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="114"/>
        <source>Next</source>
        <translation>下一页</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="120"/>
        <source>Fit Width</source>
        <translation>适合宽度</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="123"/>
        <source>Fit Page</source>
        <translation>适合页面</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="126"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="131"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="144"/>
        <source>Continuous</source>
        <translation>连续</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="149"/>
        <source>Two pages</source>
        <translation>双页</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="154"/>
        <source>Rotate left</source>
        <translation>向左旋转</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="158"/>
        <source>Rotate right</source>
        <translation>向右旋转</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="162"/>
        <source>Presentation...</source>
        <translation>放映</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="195"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="206"/>
        <source>Previous Position</source>
        <translation>上一位置</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="211"/>
        <source>Next Position</source>
        <translation>下一位置</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="220"/>
        <location filename="../pdfviewerwidget.cpp" line="928"/>
        <source>Print</source>
        <translation>打印</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="223"/>
        <source>External Viewer</source>
        <translation>外部查看器</translation>
    </message>
    <message>
        <source>Check Spelling and Grammar on this page</source>
        <translation type="vanished">拼写和语法检查</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../pdfviewerwidget.cpp" line="406"/>
        <source>File not found</source>
        <translation>未找到文件</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">另存为</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>unknown</source>
        <translation type="vanished">未知类型</translation>
    </message>
    <message>
        <source>Type 1</source>
        <translation type="vanished">Type 1</translation>
    </message>
    <message>
        <source>Type 1C</source>
        <translation type="vanished">Type 1C</translation>
    </message>
    <message>
        <source>Type 3</source>
        <translation type="vanished">Type 3</translation>
    </message>
    <message>
        <source>TrueType</source>
        <translation type="vanished">TrueType</translation>
    </message>
    <message>
        <source>CID Type 0</source>
        <translation type="vanished">CID Type 0</translation>
    </message>
    <message>
        <source>CID Type 0C</source>
        <translation type="vanished">CID Type 0C</translation>
    </message>
    <message>
        <source>CID TrueType</source>
        <translation type="vanished">CID TrueType</translation>
    </message>
    <message>
        <source>Type 1C (OpenType)</source>
        <translation type="vanished">Type 1C (OpenType)</translation>
    </message>
    <message>
        <source>TrueType (OpenType)</source>
        <translation type="vanished">TrueType (OpenType)</translation>
    </message>
    <message>
        <source>CID Type 0C (OpenType)</source>
        <translation type="vanished">CID Type 0C (OpenType)</translation>
    </message>
    <message>
        <source>CID TrueType (OpenType)</source>
        <translation type="vanished">CID TrueType (OpenType)</translation>
    </message>
    <message>
        <source>Bug: unexpected font type. Notify poppler mailing list!</source>
        <translation type="vanished">错误:未知字体类型。</translation>
    </message>
</context>
<context>
    <name>QuickBeamerDialog</name>
    <message>
        <location filename="../quickbeamerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>会话</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="22"/>
        <source>AMS Packages</source>
        <translation>AMS包</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="29"/>
        <source>Encoding</source>
        <translation>编码</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="39"/>
        <source>Typeface Size</source>
        <translation>字体大小</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="46"/>
        <source>babel Package</source>
        <translation>Babel包</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="59"/>
        <source>graphicx Package</source>
        <translation>graphicx包</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="66"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="79"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.ui" line="86"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../quickbeamerdialog.cpp" line="23"/>
        <source>Quick Start</source>
        <translation>文档向导</translation>
    </message>
</context>
<context>
    <name>QuickDocumentDialog</name>
    <message>
        <location filename="../quickdocumentdialog.cpp" line="32"/>
        <source>Quick Start</source>
        <translation>文档向导</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="157"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="41"/>
        <location filename="../quickdocumentdialog.ui" line="70"/>
        <location filename="../quickdocumentdialog.ui" line="115"/>
        <location filename="../quickdocumentdialog.ui" line="144"/>
        <location filename="../quickdocumentdialog.ui" line="199"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="25"/>
        <source>Document Class</source>
        <translation>文档类别</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="173"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="183"/>
        <source>babel Package</source>
        <translation>Babel包</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="206"/>
        <source>geometry Package</source>
        <translation>geometry包</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="213"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation>left=2cm,right=2cm,top=2cm,bottom=2cm</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="220"/>
        <source>AMS Packages</source>
        <translation>AMS包</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="237"/>
        <source>graphicx Package</source>
        <translation>graphicx包</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="244"/>
        <source>lmodern Package</source>
        <translation>lmodern包</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="251"/>
        <source>Kpfonts Package</source>
        <translation>Kpfonts包</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="258"/>
        <source>Fourier Package</source>
        <translation>Fourier包</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="83"/>
        <source>Typeface Size</source>
        <translation>字体大小</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="99"/>
        <source>Paper Size</source>
        <translation>纸张大小</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="54"/>
        <source>Other Options</source>
        <translation>其他选项</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="128"/>
        <source>Encoding</source>
        <translation>编码</translation>
    </message>
    <message>
        <location filename="../quickdocumentdialog.ui" line="230"/>
        <source>makeidx Package</source>
        <translation>makeidx包</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>QuickXelatexDialog</name>
    <message>
        <location filename="../quickxelatexdialog.ui" line="25"/>
        <location filename="../quickxelatexdialog.ui" line="83"/>
        <location filename="../quickxelatexdialog.ui" line="115"/>
        <location filename="../quickxelatexdialog.ui" line="170"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="38"/>
        <source>Other Options</source>
        <translation>其他选项</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="51"/>
        <source>Document Class</source>
        <translation>文档类别</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="70"/>
        <source>Paper Size</source>
        <translation>纸张大小</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="96"/>
        <source>Typeface Size</source>
        <translation>字体大小</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="128"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="147"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="154"/>
        <source>polyglossia Package</source>
        <translation>polyglossia包</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="177"/>
        <source>geometry Package</source>
        <translation>geometry包</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="184"/>
        <source>left=2cm,right=2cm,top=2cm,bottom=2cm</source>
        <translation>left=2cm,right=2cm,top=2cm,bottom=2cm</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="191"/>
        <source>AMS Packages</source>
        <translation>AMS包</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.ui" line="201"/>
        <source>graphicx Package</source>
        <translation>graphicx包</translation>
    </message>
    <message>
        <location filename="../quickxelatexdialog.cpp" line="31"/>
        <source>Quick Start</source>
        <translation>文档向导</translation>
    </message>
</context>
<context>
    <name>RefDialog</name>
    <message>
        <location filename="../refdialog.ui" line="14"/>
        <location filename="../refdialog.ui" line="40"/>
        <source>Labels</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>ReplaceDialog</name>
    <message>
        <source>Replace this occurrence ? </source>
        <translation type="obsolete">替换这个 ? </translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">否</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Replace Text</source>
        <translation type="obsolete">替换文本</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation type="obsolete">替换</translation>
    </message>
    <message>
        <source>Find</source>
        <translation type="obsolete">查找</translation>
    </message>
    <message>
        <source>Replace All</source>
        <translation type="obsolete">替换全部</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">选项</translation>
    </message>
    <message>
        <source>Whole words only</source>
        <translation type="obsolete">全字匹配</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation type="obsolete">区分大小写</translation>
    </message>
    <message>
        <source>Start at Beginning</source>
        <translation type="obsolete">从头开始</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="obsolete">方向</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">向下</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation type="obsolete">向上</translation>
    </message>
</context>
<context>
    <name>ReplaceWidget</name>
    <message>
        <location filename="../replacewidget.ui" line="20"/>
        <source>Form</source>
        <translation>从</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="34"/>
        <location filename="../replacewidget.ui" line="60"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="70"/>
        <source>Forward</source>
        <translation>向下</translation>
    </message>
    <message>
        <source>+</source>
        <translation type="vanished">+</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="222"/>
        <location filename="../replacewidget.cpp" line="251"/>
        <source>Text must be selected before checking this option.</source>
        <translation>检查该选项之前必须选择文本。</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="225"/>
        <source>In selection only</source>
        <translation>只在选中的部分中查找</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="130"/>
        <source>Backward</source>
        <translation>向上</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="97"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="123"/>
        <source>Replace All</source>
        <translation>全部替换</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="158"/>
        <source>Whole words only</source>
        <translation>全字匹配</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="165"/>
        <source>Case sensitive</source>
        <translation>区分大小写</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="172"/>
        <source>Start at Beginning</source>
        <translation>从头开始</translation>
    </message>
    <message>
        <location filename="../replacewidget.ui" line="215"/>
        <source>Regular Expression</source>
        <translation>正则表达式</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="82"/>
        <location filename="../replacewidget.cpp" line="153"/>
        <source>Invalid regular expression.</source>
        <translation>无效的正则表达式。</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Replace this occurrence ? </source>
        <translation>是否替换?</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../replacewidget.cpp" line="97"/>
        <location filename="../replacewidget.cpp" line="120"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../scandialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>查找文件夹</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="20"/>
        <source>In directory :</source>
        <translation>文件夹:</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="57"/>
        <source>Text :</source>
        <translation>内容:</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="67"/>
        <location filename="../scandialog.cpp" line="43"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="74"/>
        <source>Include subdirectories</source>
        <translation>包含子文件夹</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="81"/>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <location filename="../scandialog.ui" line="100"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>SourceView</name>
    <message>
        <location filename="../sourceview.cpp" line="50"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="61"/>
        <source>Check differences</source>
        <translation>检查差异</translation>
    </message>
    <message>
        <location filename="../sourceview.cpp" line="167"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
</context>
<context>
    <name>SpellerDialog</name>
    <message>
        <source>Error : Can&apos;t open the dictionary</source>
        <translation type="obsolete">错误 : 不能打开字典</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="121"/>
        <source>Check spelling selection...</source>
        <translation>检查选中部分的拼写</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="131"/>
        <source>Check spelling from cursor...</source>
        <translation>从光标处开始检查拼写</translation>
    </message>
    <message>
        <location filename="../spellerdialog.cpp" line="152"/>
        <location filename="../spellerdialog.cpp" line="169"/>
        <location filename="../spellerdialog.cpp" line="193"/>
        <location filename="../spellerdialog.cpp" line="319"/>
        <source>No more misspelled words</source>
        <translation>没有其他拼写错误</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="13"/>
        <source>Check Spelling</source>
        <translation>检查拼写</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="19"/>
        <source>Unknown word:</source>
        <translation>未知单词:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="33"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="43"/>
        <source>Replace with:</source>
        <translation>替换为:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="53"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="73"/>
        <source>Always ignore</source>
        <translation>总是忽略</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="80"/>
        <source>Suggested words :</source>
        <translation>建议单词:</translation>
    </message>
    <message>
        <location filename="../spellerdialog.ui" line="130"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>StructDialog</name>
    <message>
        <location filename="../structdialog.ui" line="14"/>
        <source>Structure</source>
        <translation>文档结构</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="37"/>
        <source>Numeration</source>
        <translation>计数</translation>
    </message>
    <message>
        <location filename="../structdialog.ui" line="53"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>SymbolListWidget</name>
    <message>
        <location filename="../symbollistwidget.cpp" line="52"/>
        <source>Add to favorites</source>
        <translation>加入收藏</translation>
    </message>
    <message>
        <location filename="../symbollistwidget.cpp" line="53"/>
        <source>Remove from favorites</source>
        <translation>从收藏中删除</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <location filename="../tabdialog.cpp" line="108"/>
        <source>Quick Tabular</source>
        <translation>表格向导</translation>
    </message>
    <message>
        <source>Horizontal Separator</source>
        <translation type="obsolete">水平分隔符</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="37"/>
        <source>Num of Columns</source>
        <translation>列数</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="78"/>
        <source>Columns</source>
        <translation>列</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="86"/>
        <source>Column :</source>
        <translation>列:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="113"/>
        <source>Alignment :</source>
        <translation>对齐方式:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="127"/>
        <source>Left Border :</source>
        <translation>左边界:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="167"/>
        <source>Apply to all columns</source>
        <translation>应用到所有列</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="194"/>
        <source>Right Border (last column) :</source>
        <translation>右边界:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="263"/>
        <source>Rows</source>
        <translation>行</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="271"/>
        <source>Row :</source>
        <translation>行:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="296"/>
        <source>Top Border</source>
        <translation>上边界</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="305"/>
        <source>Merge columns :</source>
        <translation>合并列:</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="321"/>
        <source>-&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="364"/>
        <source>Apply to all rows</source>
        <translation>应用到所有行</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="389"/>
        <source>Bottom Border (last row)</source>
        <translation>下边界</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="396"/>
        <source>Add vertical margin for each row</source>
        <translation>每行下方增加空白高度</translation>
    </message>
    <message>
        <source>Columns Alignment</source>
        <translation type="obsolete">列对齐</translation>
    </message>
    <message>
        <source>Vertical Separator</source>
        <translation type="obsolete">垂直分隔符</translation>
    </message>
    <message>
        <location filename="../tabdialog.ui" line="232"/>
        <source>Num of Rows</source>
        <translation>行数</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>TabbingDialog</name>
    <message>
        <location filename="../tabbingdialog.cpp" line="27"/>
        <source>Quick Tabbing</source>
        <translation>制表向导</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="67"/>
        <source>Spacing</source>
        <translation>间隔</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="80"/>
        <source>Num of Rows</source>
        <translation>行数</translation>
    </message>
    <message>
        <location filename="../tabbingdialog.ui" line="93"/>
        <source>Num of Columns</source>
        <translation>列数</translation>
    </message>
</context>
<context>
    <name>TexdocDialog</name>
    <message>
        <location filename="../texdocdialog.ui" line="20"/>
        <source>Search documentation about :</source>
        <translation>在说明文档中搜索:</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="59"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../texdocdialog.ui" line="116"/>
        <source>Texdoc command :</source>
        <translation>TeXdoc命令:</translation>
    </message>
    <message>
        <location filename="../texdocdialog.cpp" line="32"/>
        <source>Browse program</source>
        <translation>浏览程序</translation>
    </message>
</context>
<context>
    <name>Texmaker</name>
    <message>
        <location filename="../texmaker.cpp" line="667"/>
        <location filename="../texmaker.cpp" line="837"/>
        <location filename="../texmaker.cpp" line="2658"/>
        <location filename="../texmaker.cpp" line="5806"/>
        <source>Structure</source>
        <translation>文档结构</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="686"/>
        <location filename="../texmaker.cpp" line="5811"/>
        <source>Relation symbols</source>
        <translation>关系符号</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="696"/>
        <location filename="../texmaker.cpp" line="5816"/>
        <source>Arrow symbols</source>
        <translation>箭头符号</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="705"/>
        <location filename="../texmaker.cpp" line="5821"/>
        <source>Miscellaneous symbols</source>
        <translation>其他符号</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="714"/>
        <location filename="../texmaker.cpp" line="5826"/>
        <source>Delimiters</source>
        <translation>定界符</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="723"/>
        <location filename="../texmaker.cpp" line="5831"/>
        <source>Greek letters</source>
        <translation>希腊字母</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="732"/>
        <location filename="../texmaker.cpp" line="5836"/>
        <source>Most used symbols</source>
        <translation>常用符号</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="741"/>
        <location filename="../texmaker.cpp" line="5841"/>
        <source>Favorites symbols</source>
        <translation>收藏的符号</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="780"/>
        <location filename="../texmaker.cpp" line="813"/>
        <location filename="../texmaker.cpp" line="5846"/>
        <source>Pstricks Commands</source>
        <translation>PSTricks命令</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="788"/>
        <location filename="../texmaker.cpp" line="816"/>
        <location filename="../texmaker.cpp" line="5856"/>
        <source>MetaPost Commands</source>
        <translation>MetaPost命令</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="796"/>
        <location filename="../texmaker.cpp" line="819"/>
        <location filename="../texmaker.cpp" line="5861"/>
        <source>Tikz Commands</source>
        <translation>Tikz命令</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="804"/>
        <location filename="../texmaker.cpp" line="822"/>
        <location filename="../texmaker.cpp" line="5866"/>
        <source>Asymptote Commands</source>
        <translation>Asymptote命令</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2663"/>
        <source>Messages / Log File</source>
        <translation>消息和日志文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1215"/>
        <source>Toggle between the master document and the current document</source>
        <translation>在主文档和当前文档间切换</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1383"/>
        <location filename="../texmaker.cpp" line="11064"/>
        <source>Normal Mode</source>
        <translation>正常模式</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="obsolete">准备</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1407"/>
        <source>&amp;File</source>
        <translation>&amp;文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1408"/>
        <location filename="../texmaker.cpp" line="1409"/>
        <location filename="../texmaker.cpp" line="2914"/>
        <location filename="../texmaker.cpp" line="2915"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1414"/>
        <source>New by copying an existing file</source>
        <translation>通过复制已有文件新建</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1418"/>
        <location filename="../texmaker.cpp" line="1419"/>
        <location filename="../texmaker.cpp" line="2919"/>
        <location filename="../texmaker.cpp" line="2920"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1424"/>
        <source>Open Recent</source>
        <translation>最近打开的文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1438"/>
        <source>Restore previous session</source>
        <translation>恢复上一次会话</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1450"/>
        <location filename="../texmaker.cpp" line="1451"/>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="12260"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1458"/>
        <location filename="../texmaker.cpp" line="4020"/>
        <location filename="../texmaker.cpp" line="4093"/>
        <location filename="../texmaker.cpp" line="5732"/>
        <source>Save As</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1462"/>
        <source>Save All</source>
        <translation>全部保存</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1242"/>
        <location filename="../texmaker.cpp" line="1470"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="764"/>
        <location filename="../texmaker.cpp" line="5872"/>
        <source>User</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1437"/>
        <source>Session</source>
        <translation>会话</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1443"/>
        <source>Save session</source>
        <translation>保存会话</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1446"/>
        <source>Load session</source>
        <translation>加载会话</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1466"/>
        <source>Save A Copy</source>
        <translation>保存副本</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1476"/>
        <source>Close All</source>
        <translation>全部关闭</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1480"/>
        <source>Reload document from file</source>
        <translation>重新加载文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1484"/>
        <source>Reload all documents from file</source>
        <translation>重新加载所有文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1488"/>
        <source>Print</source>
        <translation>打印</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1494"/>
        <location filename="../texmaker.cpp" line="1495"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1501"/>
        <source>&amp;Edit</source>
        <translation>&amp;编辑</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1502"/>
        <location filename="../texmaker.cpp" line="1503"/>
        <source>Undo</source>
        <translation>撤消</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1508"/>
        <location filename="../texmaker.cpp" line="1509"/>
        <source>Redo</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1515"/>
        <location filename="../texmaker.cpp" line="1516"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1521"/>
        <location filename="../texmaker.cpp" line="1522"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1527"/>
        <location filename="../texmaker.cpp" line="1528"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1533"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1539"/>
        <source>Comment</source>
        <translation>注释</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1545"/>
        <source>Uncomment</source>
        <translation>取消注释</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1551"/>
        <source>Indent</source>
        <translation>缩进</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1557"/>
        <source>Unindent</source>
        <translation>取消缩进</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1564"/>
        <source>Fold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1568"/>
        <source>Unfold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1573"/>
        <source>&amp;Fold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1616"/>
        <source>&amp;Unfold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1663"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1669"/>
        <source>FindNext</source>
        <translation>查找下一个</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1675"/>
        <source>Find In Directory</source>
        <translation>在文件夹中查找</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1680"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1686"/>
        <source>Goto Line</source>
        <translation>跳转到行</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1693"/>
        <source>Check Spelling</source>
        <translation>检查拼写</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1700"/>
        <source>Refresh Structure</source>
        <translation>更新文档结构</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1706"/>
        <source>Refresh Bibliography</source>
        <translation>更新参考文献</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1711"/>
        <source>&amp;Tools</source>
        <translation>&amp;工具</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1712"/>
        <location filename="../texmaker.cpp" line="2969"/>
        <location filename="../texmaker.cpp" line="9757"/>
        <source>Quick Build</source>
        <translation>快速构建</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1723"/>
        <location filename="../texmaker.cpp" line="2995"/>
        <source>View Dvi</source>
        <translation>查看DVI</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1733"/>
        <location filename="../texmaker.cpp" line="2996"/>
        <source>View PS</source>
        <translation>查看PS</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1743"/>
        <location filename="../texmaker.cpp" line="2997"/>
        <source>View PDF</source>
        <translation>查看PDF</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1758"/>
        <location filename="../texmaker.cpp" line="3010"/>
        <source>View Log</source>
        <translation>查看日志</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1433"/>
        <location filename="../texmaker.cpp" line="1799"/>
        <location filename="../texmaker.cpp" line="2547"/>
        <source>Clean</source>
        <translation>清除历史记录</translation>
    </message>
    <message>
        <source>Convert to Html</source>
        <translation type="obsolete">转换为 HTML</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1818"/>
        <location filename="../texmaker.cpp" line="3020"/>
        <source>Previous LaTeX Error</source>
        <translation>上一个LaTeX错误</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1820"/>
        <location filename="../texmaker.cpp" line="3014"/>
        <source>Next LaTeX Error</source>
        <translation>下一个LaTeX错误</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1823"/>
        <source>&amp;LaTeX</source>
        <translation>&amp;LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1857"/>
        <source>&amp;Sectioning</source>
        <translation>&amp;分节</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1887"/>
        <source>&amp;Environment</source>
        <translation>&amp;环境</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1933"/>
        <source>&amp;List Environment</source>
        <translation>&amp;罗列环境</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1957"/>
        <source>Font St&amp;yles</source>
        <translation>字体&amp;风格</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1994"/>
        <source>&amp;Tabular Environment</source>
        <translation>&amp;表格环境</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2020"/>
        <source>S&amp;pacing</source>
        <translation>&amp;间隔</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2047"/>
        <source>International &amp;Accents</source>
        <translation>&amp;重音符号</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2089"/>
        <source>International &amp;Quotes</source>
        <translation>非英文&amp;引用</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2138"/>
        <source>&amp;Math</source>
        <translation>&amp;数学</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2139"/>
        <source>Inline math mode $...$</source>
        <translation>行内数学模式$…$</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2144"/>
        <source>Display math mode \[...\]</source>
        <translation>行间数学模式\[…\]</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2149"/>
        <source>Numbered equations \begin{equation}</source>
        <translation>带编号的公式\begin{equation}</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2202"/>
        <source>Math &amp;Functions</source>
        <translation>数学&amp;函数</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2324"/>
        <source>Math Font St&amp;yles</source>
        <translation>数学字体&amp;风格</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2358"/>
        <source>Math &amp;Accents</source>
        <translation>数学&amp;重音符号</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2400"/>
        <source>Math S&amp;paces</source>
        <translation>数学&amp;空格</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2422"/>
        <source>&amp;Wizard</source>
        <translation>&amp;向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2423"/>
        <source>Quick Start</source>
        <translation>文档向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2426"/>
        <source>Quick Beamer Presentation</source>
        <translation>Beamer演示文稿向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2429"/>
        <source>Quick Xelatex Document</source>
        <translation>XeLaTeX文档向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2432"/>
        <source>Quick Letter</source>
        <translation>信件向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2436"/>
        <source>Quick Tabular</source>
        <translation>表格向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2439"/>
        <source>Quick Tabbing</source>
        <translation>制表向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2442"/>
        <source>Quick Array</source>
        <translation>数组向导</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2446"/>
        <source>&amp;Bibliography</source>
        <translation>&amp;参考文献</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2552"/>
        <source>&amp;User</source>
        <translation>&amp;自定义</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2553"/>
        <source>User &amp;Tags</source>
        <translation>自定义&amp;标签</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2595"/>
        <location filename="../texmaker.cpp" line="8156"/>
        <location filename="../texmaker.cpp" line="8212"/>
        <source>Edit User &amp;Tags</source>
        <translation>编辑自定义&amp;标签</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2598"/>
        <source>User &amp;Commands</source>
        <translation>自定义&amp;命令</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2620"/>
        <location filename="../texmaker.cpp" line="9717"/>
        <location filename="../texmaker.cpp" line="9753"/>
        <source>Edit User &amp;Commands</source>
        <translation>编辑自定义&amp;命令</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2624"/>
        <source>Customize Completion</source>
        <translation>自动补全</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2629"/>
        <source>Run script</source>
        <translation>运行脚本</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2640"/>
        <source>Other script</source>
        <translation>其他脚本</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2645"/>
        <source>&amp;View</source>
        <translation>&amp;查看</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1224"/>
        <location filename="../texmaker.cpp" line="2646"/>
        <source>Next Document</source>
        <translation>下一个文档</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1220"/>
        <location filename="../texmaker.cpp" line="2651"/>
        <source>Previous Document</source>
        <translation>上一个文档</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2701"/>
        <source>&amp;Options</source>
        <translation>&amp;选项</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2702"/>
        <source>Configure Texmaker</source>
        <translation>配置Texmaker</translation>
    </message>
    <message>
        <source>Interface Appearance</source>
        <translation type="vanished">界面外观</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2727"/>
        <source>Change Interface Font</source>
        <translation>改变界面字体</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3026"/>
        <source>Stop Process</source>
        <translation>中止</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3751"/>
        <location filename="../texmaker.cpp" line="3900"/>
        <location filename="../texmaker.cpp" line="3959"/>
        <source>The document has been changed outside Texmaker.Do you want to reload it (and discard your changes) or save it (and overwrite the file)?</source>
        <translation>文档已经改变。你想重新加载(放弃修改)还是保存(覆盖文件)?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4539"/>
        <source>The document contains unsaved work.You will lose changes by reloading the document.</source>
        <oldsource>The document contains unsaved work. you will lose changes by reloading the file</oldsource>
        <translation>文档包含尚未保存的内容。重新加载文档会丢失改动的内容。</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <source>Reload the file</source>
        <translation>重新加载文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8441"/>
        <source>A document must be saved with an extension (and without spaces or accents in the name) before being used by a command.</source>
        <translation>文档在被命令使用前必须先保存(文件名不能包含空格或重音符号且要有扩展名)。</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Make a copy of the %1.pdf/ps document in the &quot;build&quot; subdirectory and delete all the others %1.* files?</source>
        <translation>在build文件夹中建立%1.pdf/ps的副本并删除其他%1.*文件吗?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11623"/>
        <source>The language setting will take effect after restarting the application.</source>
        <translation>语言设置在重启程序后生效。</translation>
    </message>
    <message>
        <source>The appearance setting will take effect after restarting the application.</source>
        <translation type="vanished">外观设置在重启程序后生效。</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2707"/>
        <location filename="../texmaker.cpp" line="11055"/>
        <source>Define Current Document as &apos;Master Document&apos;</source>
        <translation>设置当前文档为主文档</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2763"/>
        <source>&amp;Help</source>
        <translation>&amp;帮助</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2764"/>
        <location filename="../texmaker.cpp" line="2765"/>
        <source>LaTeX Reference</source>
        <translation>LaTeX参考</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2768"/>
        <location filename="../texmaker.cpp" line="2769"/>
        <source>User Manual</source>
        <translation>用户手册</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2801"/>
        <source>About Texmaker</source>
        <translation>关于Texmaker</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1103"/>
        <source>Bold</source>
        <translation>加粗</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1108"/>
        <source>Italic</source>
        <translation>斜体</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="obsolete">下划线</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1118"/>
        <source>Left</source>
        <translation>左对齐</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1123"/>
        <source>Center</source>
        <translation>居中</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1128"/>
        <source>Right</source>
        <translation>右对齐</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1134"/>
        <location filename="../texmaker.cpp" line="1174"/>
        <source>New line</source>
        <translation>新建行</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1804"/>
        <source>Open Terminal</source>
        <translation>打开控制台</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1808"/>
        <source>Export via TeX4ht</source>
        <translation>通过TeX4ht导出</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1813"/>
        <source>Convert to unicode</source>
        <translation>转换为Unicode</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2669"/>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Pdf Viewer</source>
        <translation>PDF查看器</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2686"/>
        <source>List of opened files</source>
        <translation>已打开文件列表</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2693"/>
        <source>Full Screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2732"/>
        <source>Interface Language</source>
        <translation>界面语言</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2749"/>
        <source>Manage Settings File</source>
        <translation>管理设置文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2751"/>
        <source>Settings File</source>
        <translation>设置文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2753"/>
        <source>Reset Settings</source>
        <translation>重置设置</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2756"/>
        <source>Save a copy of the settings file</source>
        <translation>保存设置文件副本</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2759"/>
        <source>Replace the settings file by a new one</source>
        <translation>替换配置文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2796"/>
        <source>Check for Update</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="2991"/>
        <source>Run</source>
        <translation>运行</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3004"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="1261"/>
        <location filename="../texmaker.cpp" line="1264"/>
        <location filename="../texmaker.cpp" line="1267"/>
        <source>Click to jump to the bookmark</source>
        <translation>点击跳转到书签</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="4753"/>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="8441"/>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10020"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3246"/>
        <location filename="../texmaker.cpp" line="3600"/>
        <location filename="../texmaker.cpp" line="12322"/>
        <location filename="../texmaker.cpp" line="12450"/>
        <source>You do not have read permission to this file.</source>
        <translation>你没有读这个文件的权限。</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3595"/>
        <location filename="../texmaker.cpp" line="3655"/>
        <location filename="../texmaker.cpp" line="12312"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3929"/>
        <location filename="../texmaker.cpp" line="3990"/>
        <location filename="../texmaker.cpp" line="12267"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>文件无法保存，请检查你是否拥有写权限。</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4130"/>
        <location filename="../texmaker.cpp" line="4194"/>
        <location filename="../texmaker.cpp" line="4264"/>
        <location filename="../texmaker.cpp" line="4370"/>
        <source>The document contains unsaved work. Do you want to save it before closing?</source>
        <translation>文档包含尚未保存的内容，你想在退出前保存吗?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Save and Close</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <source>Don&apos;t Save and Close</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="3753"/>
        <location filename="../texmaker.cpp" line="3902"/>
        <location filename="../texmaker.cpp" line="3961"/>
        <location filename="../texmaker.cpp" line="4132"/>
        <location filename="../texmaker.cpp" line="4196"/>
        <location filename="../texmaker.cpp" line="4266"/>
        <location filename="../texmaker.cpp" line="4372"/>
        <location filename="../texmaker.cpp" line="4541"/>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>The document contains unsaved work. Do you want to save it before exiting?</source>
        <translation type="obsolete">文档包含尚未保存的内容，你想在退出前保存它吗?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4753"/>
        <source>Error : Can&apos;t open the dictionary</source>
        <translation>错误:无法打开字典</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4774"/>
        <source>Browse script</source>
        <translation>浏览脚本</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5710"/>
        <source>Delete settings file?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>删除配置文件?(Texmaker将关闭)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5711"/>
        <location filename="../texmaker.cpp" line="5754"/>
        <location filename="../texmaker.cpp" line="9305"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5753"/>
        <source>Replace settings file by a new one?
(Texmaker will be closed and you will have to restart it)</source>
        <translation>替换配置文件?(Texmaker将关闭)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="5778"/>
        <source>Opened Files</source>
        <translation>已打开的文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="6667"/>
        <source>Select an image File</source>
        <translation>选择图片文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4311"/>
        <location filename="../texmaker.cpp" line="6698"/>
        <location filename="../texmaker.cpp" line="6723"/>
        <source>Select a File</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8436"/>
        <location filename="../texmaker.cpp" line="9297"/>
        <location filename="../texmaker.cpp" line="9690"/>
        <location filename="../texmaker.cpp" line="9887"/>
        <source>Can&apos;t detect the file name</source>
        <translation>无法检测该文件名</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="8751"/>
        <location filename="../texmaker.cpp" line="8773"/>
        <location filename="../texmaker.cpp" line="8792"/>
        <location filename="../texmaker.cpp" line="8811"/>
        <location filename="../texmaker.cpp" line="8833"/>
        <location filename="../texmaker.cpp" line="8876"/>
        <location filename="../texmaker.cpp" line="8893"/>
        <location filename="../texmaker.cpp" line="8904"/>
        <location filename="../texmaker.cpp" line="8955"/>
        <location filename="../texmaker.cpp" line="8972"/>
        <location filename="../texmaker.cpp" line="8983"/>
        <location filename="../texmaker.cpp" line="9028"/>
        <location filename="../texmaker.cpp" line="9047"/>
        <location filename="../texmaker.cpp" line="9066"/>
        <location filename="../texmaker.cpp" line="9085"/>
        <location filename="../texmaker.cpp" line="9102"/>
        <location filename="../texmaker.cpp" line="9113"/>
        <location filename="../texmaker.cpp" line="9138"/>
        <location filename="../texmaker.cpp" line="9155"/>
        <location filename="../texmaker.cpp" line="9166"/>
        <location filename="../texmaker.cpp" line="9197"/>
        <location filename="../texmaker.cpp" line="9982"/>
        <location filename="../texmaker.cpp" line="10608"/>
        <location filename="../texmaker.cpp" line="11073"/>
        <location filename="../texmaker.cpp" line="12620"/>
        <source>Could not start the command.</source>
        <translation>无法启动该命令。</translation>
    </message>
    <message>
        <source>View Dvi file</source>
        <translation type="obsolete">查看 Dvi 文件</translation>
    </message>
    <message>
        <source>View PS file</source>
        <translation type="obsolete">查看 PS 文件</translation>
    </message>
    <message>
        <source>View Pdf file</source>
        <translation type="obsolete">查看 PDF 文件</translation>
    </message>
    <message>
        <source>Delete the output files generated by LaTeX ?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind)</source>
        <translation type="obsolete">删除 LaTeX 产生的输出文件?
(.log,.aux,.dvi,.lof,.lot,.bit,.idx,.glo,.bbl,.ilg,.toc,.ind)</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete Files</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9334"/>
        <source>Delete the output files generated by LaTeX ?</source>
        <translation>删除LaTeX生成的文件?</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PdfLaTeX</source>
        <translation>PdfLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>dvips</source>
        <translation>dvips</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvi Viewer</source>
        <translation>DVI查看器</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>PS Viewer</source>
        <translation>PS查看器</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Dvipdfm</source>
        <translation>Dvipdfm</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ps2pdf</source>
        <translation>ps2pdf</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Bibtex</source>
        <translation>BibTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Makeindex</source>
        <translation>Makeindex</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>metapost</source>
        <translation>MetaPost</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>ghostscript</source>
        <translation>Ghostscript</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Asymptote</source>
        <translation>Asymptote</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>Latexmk</source>
        <translation>LaTeX-Mk</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>R Sweave</source>
        <translation>R Sweave</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>XeLaTex</source>
        <translation>XeLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="9714"/>
        <source>LuaLaTex</source>
        <translation>LuaLaTeX</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10020"/>
        <source>Log File not found !</source>
        <translation>未找到日志文件!</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10360"/>
        <location filename="../texmaker.cpp" line="10400"/>
        <source>Click to jump to the line</source>
        <translation>点击跳转到行</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="10474"/>
        <location filename="../texmaker.cpp" line="10509"/>
        <source>No LaTeX errors detected !</source>
        <translation>未检测到LaTeX错误!</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="4809"/>
        <location filename="../texmaker.cpp" line="10548"/>
        <location filename="../texmaker.cpp" line="10586"/>
        <source>File not found</source>
        <translation>未找到文件</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11079"/>
        <location filename="../texmaker.cpp" line="12629"/>
        <source>Normal Mode (current master document :</source>
        <translation>正常模式(当前主文档):</translation>
    </message>
    <message>
        <location filename="../texmaker.cpp" line="11081"/>
        <location filename="../texmaker.cpp" line="12631"/>
        <source>Master Document :</source>
        <translation>主文档:</translation>
    </message>
</context>
<context>
    <name>UnicodeDialog</name>
    <message>
        <location filename="../unicodedialog.ui" line="14"/>
        <location filename="../unicodedialog.ui" line="98"/>
        <source>Convert to unicode</source>
        <translation>转换为Unicode</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="28"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="75"/>
        <source>Original Encoding</source>
        <translation>原始编码</translation>
    </message>
    <message>
        <location filename="../unicodedialog.ui" line="145"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="56"/>
        <source>Select a File</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../unicodedialog.cpp" line="70"/>
        <source>You do not have read permission to this file.</source>
        <translation>你没有读这个文件的权限。</translation>
    </message>
</context>
<context>
    <name>UnicodeView</name>
    <message>
        <location filename="../unicodeview.cpp" line="56"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="152"/>
        <source>Save As</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../unicodeview.cpp" line="159"/>
        <source>The file could not be saved. Please check if you have write permission.</source>
        <translation>文件不能保存，请检查你是否拥有写权限。</translation>
    </message>
</context>
<context>
    <name>UserCompletionDialog</name>
    <message>
        <location filename="../usercompletiondialog.ui" line="14"/>
        <source>Completion</source>
        <translation>自动补全</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="25"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="48"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="55"/>
        <source>( @ : placeholder )</source>
        <translation>(@:占位符)</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="62"/>
        <source>( #bib# : bibliography item )</source>
        <translation>(#bib#:参考文献项)</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="69"/>
        <source>( #label# : label item )</source>
        <translation>(#label#:标签项)</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="76"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="../usercompletiondialog.ui" line="85"/>
        <source>Items already supplied by Texmaker</source>
        <translation>Texmaker已有的项</translation>
    </message>
</context>
<context>
    <name>UserMenuDialog</name>
    <message>
        <location filename="../usermenudialog.ui" line="14"/>
        <source>Edit User Tags</source>
        <translation>编辑自定义标签</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="45"/>
        <source>Menu Item</source>
        <translation>菜单项</translation>
    </message>
    <message>
        <location filename="../usermenudialog.ui" line="55"/>
        <source>LaTeX Content</source>
        <translation>LaTeX内容</translation>
    </message>
</context>
<context>
    <name>UserQuickDialog</name>
    <message>
        <location filename="../userquickdialog.ui" line="14"/>
        <source>Quick Build Command</source>
        <translation>快速构建命令</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="42"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="70"/>
        <source>Ordered list of commands :</source>
        <translation>命令列表:</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="80"/>
        <source>Up</source>
        <translation>上移</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="87"/>
        <source>Down</source>
        <translation>下移</translation>
    </message>
    <message>
        <location filename="../userquickdialog.ui" line="94"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>UserTagsListWidget</name>
    <message>
        <location filename="../usertagslistwidget.cpp" line="30"/>
        <source>Add tag</source>
        <translation>添加标签</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="32"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../usertagslistwidget.cpp" line="34"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
</context>
<context>
    <name>UserToolDialog</name>
    <message>
        <location filename="../usertooldialog.ui" line="14"/>
        <source>Edit User Commands</source>
        <translation>编辑自定义命令</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="51"/>
        <source>(the commands must be separated by &apos;|&apos;)</source>
        <translation>(命令必须用&apos;|&apos;分隔)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="70"/>
        <source>Menu Item</source>
        <translation>菜单项</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="77"/>
        <source>Command (% : filename without extension)</source>
        <translation>命令(%:不带扩展名的文件名)</translation>
    </message>
    <message>
        <location filename="../usertooldialog.ui" line="103"/>
        <source>wizard</source>
        <translation>向导</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../versiondialog.ui" line="14"/>
        <source>Texmaker</source>
        <translation>Texmaker</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="20"/>
        <source>Check for available version</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="34"/>
        <source>Available version</source>
        <translation>可用版本</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="48"/>
        <source>Go to the download page</source>
        <translation>进入下载页</translation>
    </message>
    <message>
        <location filename="../versiondialog.ui" line="68"/>
        <source>You&apos;re using the version</source>
        <translation>当前版本</translation>
    </message>
    <message>
        <location filename="../versiondialog.cpp" line="53"/>
        <location filename="../versiondialog.cpp" line="60"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>WebPublishDialog</name>
    <message>
        <source>Left</source>
        <translation type="obsolete">左对齐</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">居中</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="obsolete">右对齐</translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="obsolete">图标</translation>
    </message>
    <message>
        <source>Page numbers</source>
        <translation type="obsolete">页数</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">否</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="obsolete">打开文件</translation>
    </message>
    <message>
        <source>Convert to Html</source>
        <translation type="obsolete">转换为 HTML</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Bitstream Vera Sans&apos;;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#000000;&quot;&gt;LaTeX to Html conversion tool&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Bitstream Vera Sans&apos;; color:#000000;&quot;&gt;Copyright 2004-2006 P.Brachet &amp;amp; J.Amblard&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Bitstream Vera Sans&apos;;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#000000;&quot;&gt;LaTeX to Html conversion tool&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Bitstream Vera Sans&apos;; color:#000000;&quot;&gt;Copyright 2004-2006 P.Brachet &amp;amp; J.Amblard&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>LaTeX options</source>
        <translation type="obsolete">LaTeX 选项</translation>
    </message>
    <message>
        <source>Number of latex compilations for the input file</source>
        <translation type="obsolete">输入文件的LaTeX编译数</translation>
    </message>
    <message>
        <source>Content name :</source>
        <translation type="obsolete">内容名 :</translation>
    </message>
    <message>
        <source>Start Index :</source>
        <translation type="obsolete">开始索引 :</translation>
    </message>
    <message>
        <source>Tocdepth :</source>
        <translation type="obsolete">目录深度 :</translation>
    </message>
    <message>
        <source>Latex code for the title of the table of contents</source>
        <translation type="obsolete">目录名的Latex代码</translation>
    </message>
    <message>
        <source>The number of the ps page corresponding to the first numberline indexed in the toc file</source>
        <translation type="obsolete">对应于目录文件中第一个数字索引的ps页面数</translation>
    </message>
    <message>
        <source>Set the value of the LaTeX tocdepth counter</source>
        <translation type="obsolete">设置LaTeX目录深度计数器值</translation>
    </message>
    <message>
        <source>Number of compilations :</source>
        <translation type="obsolete">编译数 :</translation>
    </message>
    <message>
        <source>Html options</source>
        <translation type="obsolete">HTML 选项</translation>
    </message>
    <message>
        <source>Title :</source>
        <translation type="obsolete">标题 :</translation>
    </message>
    <message>
        <source>Title of the html files</source>
        <translation type="obsolete">HTML文件的标题</translation>
    </message>
    <message>
        <source>Navigation :</source>
        <translation type="obsolete">导航 :</translation>
    </message>
    <message>
        <source>Footnote :</source>
        <translation type="obsolete">脚注 :</translation>
    </message>
    <message>
        <source>Navigation mode</source>
        <translation type="obsolete">导航模式</translation>
    </message>
    <message>
        <source>Alignment :</source>
        <translation type="obsolete">对齐方式 :</translation>
    </message>
    <message>
        <source>Create an index page ?</source>
        <translation type="obsolete">创建一个索引(index)页 ?</translation>
    </message>
    <message>
        <source>Text displayed at the bottom of each html files </source>
        <translation type="obsolete">显示在每个HTML文件底部的文本 </translation>
    </message>
    <message>
        <source>Alignment in the html files</source>
        <translation type="obsolete">HTML文件对齐方式</translation>
    </message>
    <message>
        <source>Create index :</source>
        <translation type="obsolete">创建索引 :</translation>
    </message>
    <message>
        <source>Browser command. Let&apos;s empty to not run the browser at the end of the conversion</source>
        <translation type="obsolete">浏览器命令. 为空则在转换完后不启动浏览器</translation>
    </message>
    <message>
        <source>Launch</source>
        <translation type="obsolete">启动</translation>
    </message>
    <message>
        <source>Input File :</source>
        <translation type="obsolete">输入文件 :</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <source>Browser :</source>
        <translation type="obsolete">浏览器 :</translation>
    </message>
    <message>
        <source>Images options</source>
        <translation type="obsolete">图片选项</translation>
    </message>
    <message>
        <source>Images Width :</source>
        <translation type="obsolete">图片宽度 :</translation>
    </message>
    <message>
        <source>Width of the largest image</source>
        <translation type="obsolete">最大图片宽度</translation>
    </message>
</context>
<context>
    <name>X11FontDialog</name>
    <message>
        <location filename="../x11fontdialog.ui" line="14"/>
        <source>Select a Font</source>
        <translation>选择字体</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="34"/>
        <source>Font Family</source>
        <translation>字体族</translation>
    </message>
    <message>
        <location filename="../x11fontdialog.ui" line="44"/>
        <source>Font Size</source>
        <translation>字体大小</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
</TS>
