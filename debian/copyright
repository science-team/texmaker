Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Texmaker
Upstream-Contact: Pascal Brachet <pbrachet@xm1math.net>
Source: http://www.xm1math.net/texmaker/download.html
Files-Excluded: synctex_parser*
                .obj

Files: *
Copyright: 2003-2022 Pascal Brachet
License: GPL-2+

Files: pageitem.* documentview.* presentationview.*
Copyright: 2012 Pascal Brachet
 2012 Adam Reichold
License: GPL-2+

Files: debian/*
Copyright: 2006-2007, Joseph Smidt <jsmidt@byu.edu>
 2008-2012, Andreas Tille <tille@debian.org>
 2008-2011, Ruben Molina <rmolina@udea.edu.co>
 2013-2022, Julián Moreno Patiño <julian@debian.org>
License: GPL-2+

Files: pdfviewer.* pdfviewerwidget.cpp
Copyright: 2007-2010 Jonathan Kew
           2008 Nokia Corporation and/or its subsidiary(-ies).
License: GPL-2 or GPL-3
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2',
 and the full text of the GNU General Public License version 3 can be
 found in the file `/usr/share/common-licenses/GPL-3'.

Files: dictionaries/de_DE.*
Copyright: 1998-2009 Bjoern Jacke <bjoern@j3e.de> and Franz Michael Baumann <frami.baumann@web.de>
License: GPL-2 or GPL-3
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2',
 and the full text of the GNU General Public License version 3 can be
 found in the file `/usr/share/common-licenses/GPL-3'.

Files: dictionaries/it_IT.*
Copyright: 2001-2003 Gianluca Turconi
           2004-2007 Davide Prina
License: GPL-3
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

Files: singleapp/*
Copyright: 2012 Digia Plc and/or its subsidiary(-ies)
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
     of its contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: diff/*
Copyright: 2007-2011 Chas Emerick <cemerick@snowtide.com>
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
   1. Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY Chas Emerick ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL Chas Emerick OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: minisplitter.*
Copyright: 2009 Nokia Corporation and/or its subsidiary(-ies).
 2012 Digia Plc and/or its subsidiary(-ies)
License: LGPL-2.1
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License version 2.1
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

Files: dictionaries/en_GB.*
Copyright: 2005 David Bartlett and Andrew Brown
License: LGPL-2
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

Files: dictionaries/fr_FR.*
Copyright: Christophe Pythoud
License: LGPL-2 or GPL-2 or MPL-1.1
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'. And the full text of the
  GNU General Public License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: dictionaries/es_ES.*
Copyright: Santiago Bosio
License: LGPL-2
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

Files: hunspell/*
Copyright: 2002-2005 Kevin Hendricks (MySpell) and Laszlo Nemeth (Hunspell)
License: LGPL-2.1 or GPL-2 or MPL-1.1
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2',
 and the full text of the GNU Lesser General Public License version 2
 can be found in the file `/usr/share/common-licenses/LGPL-2'.

Files: hunspell/phonet.*
Copyright: 2000, 2007 Bjoern Jacke <bjoern at j3e.de>
           2007 Laszlo Nemeth <nemeth at OOo>
License: LGPL-2.1 or GPL-2 or MPL-1.1
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2',
 and the full text of the GNU Lesser General Public License version 2
 can be found in the file `/usr/share/common-licenses/LGPL-2'.

Files: encodingprober/*
Copyright:  1998 <developer@mozilla.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

