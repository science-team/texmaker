/***************************************************************************
 *   copyright       : (C) 2003-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/texmaker/                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "x11fontdialog.h"

#include <QFontDatabase>

X11FontDialog::X11FontDialog(QWidget *parent)
    :QDialog( parent)
{
setModal(true);
ui.setupUi(this);
QFontDatabase fdb;
ui.comboBoxFont->addItems( fdb.families() );
}

X11FontDialog::~X11FontDialog(){
}
