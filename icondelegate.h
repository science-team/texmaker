/***************************************************************************
 *   copyright       : (C) 2003-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/texmaker/                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef ICONDELEGATE_H
#define ICONLDELEGATE_H

#include <QAbstractItemDelegate>
#include <QFontMetrics>
#include <QModelIndex>
#include <QSize>

class QAbstractItemModel;
class QObject;
class QPainter;

class IconDelegate : public QAbstractItemDelegate
{
    Q_OBJECT

public:
    IconDelegate(QObject *parent = 0)
        : QAbstractItemDelegate(parent) {}

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index ) const;
protected:
    virtual void drawDecoration(QPainter *painter, const QStyleOptionViewItem &option,
                                const QRect &rect, const QPixmap &pixmap) const;
    virtual void drawFocus(QPainter *painter, const QStyleOptionViewItem &option,
                           const QRect &rect) const;
    virtual void drawCheck(QPainter *painter, const QStyleOptionViewItem &option,
                           const QRect &rect, Qt::CheckState state) const;

    void doLayout(const QStyleOptionViewItem &option,
                  QRect *checkRect, QRect *iconRect, QRect *textRect, bool hint) const;
    QPixmap decoration(const QStyleOptionViewItem &option, const QVariant &variant) const;
    #if (QT_VERSION >= QT_VERSION_CHECK(5, 13, 0))
    static QPixmap selectedPixmap(const QPixmap &pixmap, const QPalette &palette, bool enabled);
    #else
    QPixmap *selected(const QPixmap &pixmap, const QPalette &palette, bool enabled) const;
    #endif
    QRect check(const QStyleOptionViewItem &option, const QRect &bounding,
                const QVariant &variant) const;
};

#endif
